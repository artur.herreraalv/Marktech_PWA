-- Adminer 4.8.1 PostgreSQL 9.6.24 dump

\connect "homestead";

DROP TABLE IF EXISTS "products";
DROP SEQUENCE IF EXISTS products_id_seq;
CREATE SEQUENCE products_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."products" (
    "id" integer DEFAULT nextval('products_id_seq') NOT NULL,
    "name" character varying(255) NOT NULL,
    "description" text NOT NULL,
    "image" character varying(255) NOT NULL,
    "price" integer NOT NULL,
    "discounted_price" integer NOT NULL,
    "created_at" timestamp NOT NULL,
    "updated_at" timestamp NOT NULL,
    "category" character varying(255) NOT NULL,
    "subcategory" character varying(255) NOT NULL,
    "featured" integer NOT NULL,
    "trademark" character varying(255) NOT NULL,
    "stock" integer NOT NULL,
    "sales" smallint NOT NULL,
    CONSTRAINT "products_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "products" ("id", "name", "description", "image", "price", "discounted_price", "created_at", "updated_at", "category", "subcategory", "featured", "trademark", "stock", "sales") VALUES
(1,	'LAPTOP LENOVO IDEAPAD 3 15ITL6',	'Procesador Intel Core i5 1135G7 Hasta 4.2 GHz, Memoria de 8GB DDR4, SSD de 512GB, Pantalla de 15.6" LED, Video Iris Xe Graphics, S.O. Windows 10 Home (64 Bits)',	'test1.png',	16999,	0,	'2022-06-24 09:47:06',	'2022-11-09 03:09:34',	'Computadoras',	'Laptop',	1,	'Lenovo',	91,	19),
(2,	'CONSOLA XBOX SERIES S DE 512GB',	'512GB, Color Blanco.',	'test3.jpg',	5699,	0,	'2022-07-25 23:10:00',	'2022-07-25 23:10:00',	'Electronica',	'Consolas',	0,	'XBOX',	0,	1),
(3,	'APPLE IMAC RETINA 24"',	'Apple M1, 8GB, 256GB SSD, Azul (Abril 2021)',	'13.webp',	36059,	0,	'2022-07-16 12:01:03',	'2022-11-06 22:03:14',	'Computadoras',	'Escritorio',	1,	'Apple',	90,	11),
(4,	'SSD ADATA ULTIMATE SU630',	'480GB, SATA, 2.5", 7mm',	'16.webp',	819,	0,	'2022-07-16 12:16:15',	'2022-07-29 05:02:18',	'Hardware',	'SSD',	0,	'Adata',	108,	1),
(5,	'Gabinete Acteck Kioto GC460',	'RGB, Micro ATX, Incluye Fuente de 500W. Color Negro',	'45.jpg',	749,	0,	'2022-07-25 20:08:54',	'2022-07-25 20:08:54',	'Hardware',	'Gabinetes',	0,	'Evotec',	30,	0),
(6,	'Procesador Intel Core i9 12900K de Doceava Generación',	'3.2 GHz (hasta 5.2 GHz) con Intel UHD Graphics 770, Socket 1700, Caché 30MB, 16 Núcleos, 14nm.',	'24.jpg',	14500,	0,	'2022-07-25 02:19:26',	'2022-10-29 16:13:46',	'Hardware',	'Procesadores',	1,	'Intel',	2,	8),
(7,	'AMD Athlon 3000G',	'Gráficos Radeon Vega 3, S-AM4, 3.50GHz, Dual-Core, 4MB L3 Cache',	'28.jpg',	1050,	0,	'2022-07-25 02:49:14',	'2022-11-09 03:05:27',	'Hardware',	'Procesadores',	0,	'AMD',	29,	1),
(8,	'TARJETA DE VIDEO EVGA NVIDIA GEFORCE RTX 3090 FTW3 ULTRA GAMING',	'24GB 384 bit GDDR6X, PCI Express x16 4.0',	'14.jpg',	42979,	0,	'2022-07-16 12:03:08',	'2022-10-29 16:13:46',	'Hardware',	'Tarjetas de Video',	0,	'Nvidia',	97,	4);

-- 2022-11-15 01:32:13.918027+00
