@extends('layouts.app')
@section('content')
    <figure class="text-center">
        <blockquote class="blockquote">
            <p class="h1 titles"><strong>{{ __('Categories') }}</strong></p>
        </blockquote>
    </figure>

    <div class="accordion accordion-flush" id="accordionFlushExample">
        <div class="accordion-item">
            <h2 class="accordion-header" id="flush-headingOne">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                    data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                    <strong>{{ __('Hardware') }}
                    </strong> <span class="iconify" data-icon="simple-icons:pcgamingwiki" style="font-size: 32px;"></span>
                </button>
            </h2>
            <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne"
                data-bs-parent="#accordionFlushExample">
                <div class="accordion-body">

                    <a class="h3 text-center" href="/hardware"><strong>{{ __('Build your PC') }}
                        </strong></a><br>
                    <hr class="hr" />
                    <a class="btn btn-dark d-block" href="/hardware/procesadores"><span class="iconify"
                            data-icon="arcticons:cpuinfo" style="font-size: 32px;"></span>
                        {{ __('CPUs') }}</a><br>
                    <a class="btn btn-dark d-block" href="/hardware\motherboards"><span class="iconify"
                            data-icon="bi:motherboard" style="font-size: 32px;"></span>
                        {{ __('Motherboards') }}</a><br>
                    <a class="btn btn-dark d-block" href="/hardware\gabinetes"><span class="iconify"
                            data-icon="arcticons:pc-creator" style="font-size: 32px;"></span>
                        {{ __('Cases') }}</a><br>
                    <a class="btn btn-dark d-block" href="/hardware\graficas"><span class="iconify" data-icon="bi:gpu-card"
                            style="font-size: 32px;"></span>
                        {{ __('Graphics cards') }}</a><br>
                    <a class="btn btn-dark d-block" href="/hardware\ram"><span class="iconify"
                            data-icon="fluent:ram-20-regular" style="font-size: 32px;"></span>
                        {{ __('RAM') }}</a><br>
                    <a class="btn btn-dark d-block" href="/hardware\disipadores"><span class="iconify"
                            data-icon="grommet-icons:fan-option" style="font-size: 32px;"></span>
                        {{ __('Coolers') }}</a><br>
                    <a class="btn btn-dark d-block" href="/hardware\fuentes"><span class="iconify"
                            data-icon="icon-park-solid:power-supply" style="font-size: 32px;"></span>
                        {{ __('Power supplies') }}</a>
                    <br><br>
                    <a class="h3 text-center" href="/hardware"><strong>
                            {{ __('Storage') }}</strong></a><br>
                    <hr class="hr" />
                    <a class="btn btn-dark d-block" href="/hardware\ssd"><span class="iconify" data-icon="clarity:ssd-line"
                            style="font-size: 32px;"></span>
                        {{ __('SSDs') }}</a><br>
                    <a class="btn btn-dark d-block" href="/hardware\hdd"><span class="iconify" data-icon="bi:device-hdd"
                            style="font-size: 32px;"></span> {{ __('HDDs') }}</a><br>
                    <a class="btn btn-dark d-block" href="/hardware\ram"><span class="iconify"
                            data-icon="fluent:ram-20-regular" style="font-size: 32px;"></span>{{ __('RAM') }}</a><br>
                    <a class="btn btn-dark d-block" href="/hardware\usb"><span class="iconify" data-icon="bi:usb-drive"
                            style="font-size: 32px;"></span>
                        {{ __('USB/SD') }}</a><br>

                </div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="flush-headingTwo">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                    data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                    <strong>{{ __('Accessories') }}
                        </strong> <span class="iconify" data-icon="bi:headphones"
                        style="font-size: 32px;"></span>
                </button>
            </h2>
            <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo"
                data-bs-parent="#accordionFlushExample">
                <div class="accordion-body">
                    <a class="h3 text-center" href="/accesorios"><strong>{{ __('Accessories') }}
                        </strong></a><br>
                    <hr class="hr" />
                    <a class="btn btn-dark d-block" href="/accesorios\audifonos"><span class="iconify"
                            data-icon="el:headphones" style="font-size: 32px;"></span>
                        {{ __('Headphones') }}</a><br>
                    <a class="btn btn-dark d-block" href="/accesorios\alfombrillas"><span class="iconify"
                            data-icon="system-uicons:paper" style="font-size: 32px;"></span>
                        {{ __('Mousepads') }}</a><br>
                    <a class="btn btn-dark d-block" href="/accesorios\mouse"><span class="iconify"
                            data-icon="emojione-monotone:computer-mouse" style="font-size: 32px;"></span>
                        {{ __('Mouse') }}</a><br>
                    <a class="btn btn-dark d-block" href="/accesorios\teclados"><span class="iconify"
                            data-icon="bi:keyboard" style="font-size: 32px;"></span>
                        {{ __('Keyboards') }}</a><br>
                </div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="flush-headingThree">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                    data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                    <strong>{{ __('Computers') }}
                        </strong> <span class="iconify" data-icon="icon-park-outline:laptop"
                        style="font-size: 32px;"></span>
                </button>
            </h2>
            <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree"
                data-bs-parent="#accordionFlushExample">
                <div class="accordion-body">
                    <a class="h3 text-center" href="/computadoras"><strong>{{ __('Computers') }}
                        </strong></a><br>
                    <hr class="hr" />
                    <a class="btn btn-dark d-block" href="/computadoras\laptop"><span class="iconify"
                            data-icon="fa:laptop" style="font-size: 32px;"></span>
                        {{ __('Laptops') }}</a><br>
                    <a class="btn btn-dark d-block" href="/computadoras\escritorio"><span class="iconify"
                            data-icon="game-icons:pc" style="font-size: 32px;"></span>
                        {{ __('Desktops') }}</a>
                </div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="flush-headingFour">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                    data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseThree">
                    <strong>{{ __('Electronics') }}
                        </strong> <span class="iconify" data-icon="heroicons:tv"
                        style="font-size: 32px;"></span>
                </button>
            </h2>
            <div id="flush-collapseFour" class="accordion-collapse collapse" aria-labelledby="flush-headingFour"
                data-bs-parent="#accordionFlushExample">
                <div class="accordion-body">
                    <a class="h3 text-center" href="/electronica"><strong>{{ __('Electronics') }}
                        </strong></a><br>
                    <hr class="hr" />
                    <a class="btn btn-dark d-block" href="/electronica\consolas"><span class="iconify"
                            data-icon="fluent:xbox-console-20-regular" style="font-size: 32px;"></span>
                        {{ __('Consoles') }}</a><br>
                    <a class="btn btn-dark d-block" href="/electronica\tv"><span class="iconify" data-icon="bx:tv"
                            style="font-size: 32px;"></span>
                        {{ __('TVs') }}</a><br>
                    <a class="btn btn-dark d-block" href="/electronica\monitores"><span class="iconify"
                            data-icon="ep:monitor" style="font-size: 32px;"></span>
                        {{ __('Monitors') }}</a><br>
                    <a class="btn btn-dark d-block" href="/electronica\bocinas"><span class="iconify"
                            data-icon="mdi:soundbar" style="font-size: 32px;"></span>
                        {{ __('Speakers') }}</a><br>
                    <a class="btn btn-dark d-block" href="/electronica\camaras"><span class="iconify"
                            data-icon="ant-design:camera-outlined" style="font-size: 32px;"></span>
                        {{ __('Cameras') }}</a><br>
                    <a class="btn btn-dark d-block" href="/electronica\telefonos"><span class="iconify"
                            data-icon="pepicons:smartphone-notch" style="font-size: 32px;"></span>
                        {{ __('Phones') }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
