@extends('layouts.app')
@section('content')
    <figure class="text-center">
        <blockquote class="blockquote">
            <p class="h1 titles" id="language"><strong>{{ __('Language') }}</strong></p>
        </blockquote>
    </figure>
    {{-- <a href="{{ url('change-language/en') }}">{{ __('English') }}</a>
    <a href="{{ url('change-language/es') }}">{{ __('Spanish') }}</a> --}}

    {{-- // a list group with all the languages --}}
    <div class="list-group">
        <a href="{{ url('change-language/en') }}" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1"><span class="iconify" data-icon="emojione-v1:flag-for-united-states"  style="font-size: 64px;"></span>

                </h5>
            </div>
            <p class="language1 mb-1 text-center">{{ __('English') }}</p>

            {{-- if the language is spanish, then check icon --}}
            @if (App::getLocale() == 'en')
            <span class="iconify check" data-icon="material-symbols:check-circle" style="font-size: 46px; color: #007fff;"></span>
            @else
            <span class="iconify check" data-icon="ph:circle-thin" style="font-size: 48px;"></span>
            @endif
        </a>
        <a href="{{ url('change-language/es') }}" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1"><span class="iconify" data-icon="emojione-v1:flag-for-mexico" style="font-size: 64px;"></span>

                </h5>
            </div>
            <p class="language2 mb-1 text-center p-1">{{ __('Spanish') }}</p>

            {{-- if the language is spanish, then check icon --}}
            @if (App::getLocale() == 'es')
            <span class="iconify check" data-icon="material-symbols:check-circle" style="font-size: 46px; color: #007fff;"></span>
            @else
            <span class="iconify check" data-icon="ph:circle-thin" style="font-size: 48px;"></span>
            @endif
        </a>
    </div>
@endsection
