@extends('layouts.app')
@section('content')


    <figure class="text-center">
        <blockquote class="blockquote">
            <p class="h1"><strong>{{ __('Themes') }}</strong></p>
        </blockquote>
    </figure>
    <div class="form-check form-switch">
        <input type="checkbox" class="form-check-input" id="darkSwitchm" />

        <label class="custom-control-label" for="darkSwitchm">{{ __('Dark mode') }}</label>
    </div>

@endsection
