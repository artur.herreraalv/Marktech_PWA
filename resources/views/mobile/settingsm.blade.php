@extends('layouts.app')
@section('content')
    <figure class="text-center">
        <blockquote class="blockquote">
            <p class="h1 titles"><strong>{{ __('Settings') }}</strong></p>
        </blockquote>
    </figure>
    <div class="list-group">
        <a href="/theme" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1"><span class="iconify" data-icon="arcticons:galaxy-themes" style="font-size: 32px;"></span>
                </h5>
                <small class="text-muted"><span class="iconify" data-icon="bi:arrow-right-circle"
                        style="font-size: 32px;"></span></small>
            </div>
            <p class="mb-1">{{ __('Themes') }}</p>
            <small class="text-muted">{{ __('Change the appearance of the application.') }}</small>
        </a>
        <a href="/languagem" class="list-group-item list-group-item-action" aria-current="true">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1"><span class="iconify" data-icon="clarity:language-line" style="font-size: 32px;"></span>
                </h5>
                <small class="text-muted"><span class="iconify" data-icon="bi:arrow-right-circle"
                        style="font-size: 32px;"></span></small>
            </div>
            <p class="mb-1">{{ __('Language') }}</p>
            <small class="text-muted"></small>
        </a>
        <a href="/aboutm" class="list-group-item list-group-item-action" aria-current="true">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1"><span class="iconify" data-icon="fluent:info-20-regular" style="font-size: 32px;"></span>
                </h5>
                <small class="text-muted"><span class="iconify" data-icon="bi:arrow-right-circle"
                        style="font-size: 32px;"></span></small>
            </div>
            <p class="mb-1">{{ __('About Marktech') }}</p>
            <small class="text-muted"></small>
        </a>
        <a href="/helpm" class="list-group-item list-group-item-action" aria-current="true">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1"><span class="iconify" data-icon="ei:question" style="font-size: 32px;"></span></h5>
                <small class="text-muted"><span class="iconify" data-icon="bi:arrow-right-circle"
                        style="font-size: 32px;"></span></small>
            </div>
            <p class="mb-1">{{ __('Help') }}</p>
            <small class="text-muted"></small>
        </a>
    </div>
@endsection
