@extends('layouts.app')
@section('content')
    {{-- // show loading-animation when loading products from database --}}
    <script>
        $(document).ready(function() {
            if ($(window).width() < 992) {
                // hide all hide-desktop elements
                $('.products-loaded').hide();
                $('.loading-animation').ajaxStart(function() {
                    $(this).show();
                })
                // check if all ajax requests are finished
                var checkAjax = setInterval(function() {
                    if ($.active == 0) {
                        $('.loading-animation').hide();
                        $('.products-loaded').show();
                        clearInterval(checkAjax);
                    } else { // if not finished, check again in 100ms
                        $('.loading-animation').show();
                        $('.products-loaded').hide();
                    }
                }, 1000);
            }
        });
    </script>
    {{-- when the page resizes 992 reload the page --}}
    <script>
        $(window).resize(function() {
            if ($(window).width() < 992) {
                location.reload();
            }
        });
    </script>
    {{-- when the page resizes 992 reload the page --}}
    {{-- <script>
        $(window).resize(function() {
            if ($(window).width() > 992) {
                location.reload();
            }
        });
    </script> --}}


    <div class="hide-mobile">
        <br>
    </div>

    <div class="container products-loaded">
        <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel" data-bs-pause="false">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active"
                    aria-current="true" aria-label="Slide 1"><span></span></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"
                    aria-label="Slide 2"><span></span></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"
                    aria-label="Slide 3"><span></span></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="3"
                    aria-label="Slide 4"><span></span></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="4"
                    aria-label="Slide 5"><span></span></button>
            </div>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <a href="{{ route('product.banner.nvidia') }}">
                        <img src="{!! asset('img/banner-nvidia.jpg') !!}" class="d-block w-100">
                    </a>
                </div>
                <div class="carousel-item">
                    <a href="{{ route('product.banner.adata') }}">
                        <img src="{!! asset('img/banner-adata.jpg') !!}" href="/card" class="d-block w-100">
                    </a>
                </div>
                <div class="carousel-item">
                    <a href="{{ route('product.banner.mac') }}">
                        <img src="{!! asset('img/banner-mac.jpg') !!}" href="/card" class="d-block w-100">
                    </a>
                </div>
                <div class="carousel-item">
                    <a href="{{ route('product.banner.toshiba') }}">
                        <img src="{!! asset('img/banner-toshiba.jpg') !!}" href="/card" class="d-block w-100">
                    </a>
                </div>
                <div class="carousel-item">
                    <a href="{{ route('product.banner.lg') }}">
                        <img src="{!! asset('img/banner-lg.jpg') !!}" href="/card" class="d-block w-100">
                    </a>
                </div>
            </div>
            <div class="hide-mobile">
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators"
                    data-bs-slide="prev">
                    <span class="iconify" data-icon="material-symbols:arrow-circle-left-rounded" style="font-size: 38px; color: #007fff;"></span>

                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators"
                    data-bs-slide="next">
                    <span class="iconify" data-icon="material-symbols:arrow-circle-right-rounded" style="font-size: 38px; color: #007fff;"></span>
                </button>
            </div>
        </div>

    </div>

    <!--Carrusel-->

    <div class="container loading-animation hide-desktop">
        <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel" data-bs-pause="false">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active"
                    aria-current="true" aria-label="Slide 1"><span></span></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"
                    aria-label="Slide 2"><span></span></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"
                    aria-label="Slide 3"><span></span></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="3"
                    aria-label="Slide 4"><span></span></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="4"
                    aria-label="Slide 5"><span></span></button>
            </div>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <a href="">
                        <img src="{!! asset('img/banner3.webp') !!}" class="card__header header__img skeleton d-block w-100">
                    </a>
                </div>
                <div class="carousel-item">
                    <a href="">
                        <img src="{!! asset('img/banner3.webp') !!}" href="/card"
                            class="card__header header__img skeleton d-block w-100">
                    </a>
                </div>
                <div class="carousel-item">
                    <a href="">
                        <img src="{!! asset('img/banner3.webp') !!}" href="/card"
                            class="card__header header__img skeleton d-block w-100">
                    </a>
                </div>
                <div class="carousel-item">
                    <a href="">
                        <img src="{!! asset('img/banner3.webp') !!}" href="/card"
                            class="card__header header__img skeleton d-block w-100">
                    </a>
                </div>
                <div class="carousel-item">
                    <a href="">
                        <img src="{!! asset('img/banner3.webp') !!}" href="/card"
                            class="card__header header__img skeleton d-block w-100">
                    </a>
                </div>
            </div>
            <div class="hide-mobile">
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators"
                    data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>

                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators"
                    data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                </button>
            </div>
        </div>

    </div>

    <br>

    {{-- // loading animation database products
    <script>
        $(document).ready(function() {
            $('#loading').hide();
            $('#loading').ajaxStart(function() {
                $(this).show();
            })
        });
    </script>

    <div id="loading" class="text-center">
        <img src="{!! asset('img/UI/load.gif') !!}">
    </div> --}}

    <div class="hide-mobile">
        <!--Products featured-->

        <div class="container-sm">
            <div class="p-3 border bg-light text-center fs-4"><strong>{{ __('Featured Products') }}</strong></div>
        </div>
        <br>
        <div class="row">
            @foreach ($viewData['products_featured'] as $product)
                <div class="col-lg-3 mb-3 d-flex align-items-stretch">
                    <div class="card">
                        <a class="hove btn stretched-link"></a>
                        <a href="{{ route('product.show', ['id' => $product->getId()]) }}">
                            <div class="img-card">
                                <img src="{{ asset('/img/products/' . $product->getImage()) }}"
                                    class="card-img-top img-card d-inline mx-auto d-block" style="height:20em;">
                            </div>
                        </a>
                        {{-- <div class="card-body text-center"> --}}
                        <div class="card-body">
                            <a href="{{ route('product.show', ['id' => $product->getId()]) }}"><strong>{{ $product->getName() }}</strong>
                            </a>
                            <p></p>
                            <a>
                                @if ($product->getPrice() == 0)
                                    <span><strong class="text-primary fs-5">{{ __('Free') }}</strong></span>
                                @elseif ($product->getDiscountedprice() > 0)
                                    <strong class="text-secondary text-decoration-line-through fs-5">
                                        <x-money class="text-decoration-line-through" amount="{{ $product->getPrice() }}"
                                            currency="MXN" convert />
                                    </strong>
                                    <strong class="text-primary fs-5">
                                        <x-money class="text-decoration-line-through"
                                            amount="{{ $product->getPrice() - $product->getDiscountedprice() }}"
                                            currency="MXN" convert />
                                    </strong>
                                @else
                                    <strong class="text-primary fs-5">
                                        <x-money class="text-decoration-line-through" amount="{{ $product->getPrice() }}"
                                            currency="MXN" convert />
                                    </strong>
                                @endif
                                <br>
                                <br>
                                @if ($product->getStock() > 0)
                                    <span class="badge bg-primary text-white fs-6"><span class="iconify"
                                            data-icon="akar-icons:check"></span>
                                        {{ __('In stock') }}</span>
                                    {{-- // add to cart --}}
                                    <form action="{{ route('cart.add', ['id' => $product->getId()]) }}" method="POST">
                                        @csrf
                                        <br>
                                        <input type="hidden" name="quantity" value="1">
                                        <button type="submit" class="hov btn btn-primary btn-sm fs-6">
                                            <span class="iconify" data-icon="mi:shopping-cart-add"></span>
                                            <strong class="titledark">{{ __('Add to cart') }}</strong>
                                        </button>
                                    </form>
                                @else
                                    <span class="badge bg-secondary text-white fs-6"><span class="iconify"
                                            data-icon="bi:x-lg"></span>

                                        {{ __('Out of stock') }}</span>
                                @endif
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="p-3 bg-light text-center fs-4"><a href="{{ route('product.more.featured') }}"
                style="color: #007fff;">{{ __('See more featured products') }}
                ></a></div>
        <br>

    </div>

    <div class="loading-animation hide-desktop">
        <div class="row">
            <div class="card mb-4">
                <div class="card-header h5 text-center">
                    <div class="skeleton skeleton-title"></div>
                </div>
                <div class="col-lg-3 mb-3 d-flex align-items-stretch">
                    <div class="card cardproduct shadow-none">
                        <a class="hove btn stretched-link"></a>
                        <a href="#">
                            <div class="img-card">
                                <img src="{{ asset('/img/products/transparent.png') }}"
                                    class="skeleton card-img-top img-card d-inline mx-auto d-block imgproduct"
                                    style="height:20em;">
                            </div>
                        </a>

                        <div class="card-body">
                            <a href="#">
                                <div class="skeleton skeleton-text"></div>
                                <div class="skeleton skeleton-text"></div>
                            </a>
                            <a>
                                <strong class="text-primary fs-5">
                                    <div class="skeleton skeleton-text skeleton-footer"></div>
                                </strong>
                                <br>
                                <div class="skeleton skeleton-text"></div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-3 d-flex align-items-stretch">
                    <div class="card cardproduct shadow-none">
                        <a class="hove btn stretched-link"></a>
                        <a href="#">
                            <div class="img-card">
                                <img src="{{ asset('/img/products/transparent.png') }}"
                                    class="skeleton card-img-top img-card d-inline mx-auto d-block imgproduct"
                                    style="height:20em;">
                            </div>
                        </a>
                        <div class="card-body">
                            <a href="#">
                                <div class="skeleton skeleton-text"></div>
                                <div class="skeleton skeleton-text"></div>
                            </a>
                            <a>
                                <strong class="text-primary fs-5">
                                    <div class="skeleton skeleton-text skeleton-footer"></div>
                                </strong>
                                <br>
                                <div class="skeleton skeleton-text"></div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-3 d-flex align-items-stretch">
                    <div class="card cardproduct shadow-none">
                        <a class="hove btn stretched-link"></a>
                        <a href="#">
                            <div class="img-card">
                                <img src="{{ asset('/img/products/transparent.png') }}"
                                    class="skeleton card-img-top img-card d-inline mx-auto d-block imgproduct"
                                    style="height:20em;">
                            </div>
                        </a>
                        <div class="card-body">
                            <a href="#">
                                <div class="skeleton skeleton-text"></div>
                                <div class="skeleton skeleton-text"></div>
                            </a>
                            <a>
                                <strong class="text-primary fs-5">
                                    <div class="skeleton skeleton-text skeleton-footer"></div>
                                </strong>
                                <br>
                                <div class="skeleton skeleton-text"></div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-3 d-flex align-items-stretch">
                    <div class="card cardproduct shadow-none">
                        <a class="hove btn stretched-link"></a>
                        <a href="#">
                            <div class="img-card">
                                <img src="{{ asset('/img/products/transparent.png') }}"
                                    class="skeleton card-img-top img-card d-inline mx-auto d-block imgproduct"
                                    style="height:20em;">
                            </div>
                        </a>
                        <div class="card-body">
                            <a href="#">
                                <div class="skeleton skeleton-text"></div>
                                <div class="skeleton skeleton-text"></div>
                            </a>
                            <a>
                                <strong class="text-primary fs-5">
                                    <div class="skeleton skeleton-text skeleton-footer"></div>
                                </strong>
                                <br>
                                <div class="skeleton skeleton-text"></div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="loading-animation hide-desktop">

        <div class="row">
            <div class="card mb-4">
                <div class="card-header h5 text-center">
                    <div class="skeleton skeleton-title"></div>
                </div>
                <div class="col-lg-3 mb-3 d-flex align-items-stretch">
                    <div class="card cardproduct shadow-none">
                        <a class="hove btn stretched-link"></a>
                        <a href="#">
                            <div class="img-card">
                                <img src="{{ asset('/img/products/transparent.png') }}"
                                    class="skeleton card-img-top img-card d-inline mx-auto d-block imgproduct"
                                    style="height:20em;">
                            </div>
                        </a>
                        <div class="card-body">
                            <a href="#">
                                <div class="skeleton skeleton-text"></div>
                                <div class="skeleton skeleton-text"></div>
                            </a>
                            <a>
                                <strong class="text-primary fs-5">
                                    <div class="skeleton skeleton-text skeleton-footer"></div>
                                </strong>
                                <br>
                                <div class="skeleton skeleton-text"></div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-3 d-flex align-items-stretch">
                    <div class="card cardproduct shadow-none">
                        <a class="hove btn stretched-link"></a>
                        <a href="#">
                            <div class="img-card">
                                <img src="{{ asset('/img/products/transparent.png') }}"
                                    class="skeleton card-img-top img-card d-inline mx-auto d-block imgproduct"
                                    style="height:20em;">
                            </div>
                        </a>
                        <div class="card-body">
                            <a href="#">
                                <div class="skeleton skeleton-text"></div>
                                <div class="skeleton skeleton-text"></div>
                            </a>
                            <a>
                                <strong class="text-primary fs-5">
                                    <div class="skeleton skeleton-text skeleton-footer"></div>
                                </strong>
                                <br>
                                <div class="skeleton skeleton-text"></div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-3 d-flex align-items-stretch">
                    <div class="card cardproduct shadow-none">
                        <a class="hove btn stretched-link"></a>
                        <a href="#">
                            <div class="img-card">
                                <img src="{{ asset('/img/products/transparent.png') }}"
                                    class="skeleton card-img-top img-card d-inline mx-auto d-block imgproduct"
                                    style="height:20em;">
                            </div>
                        </a>
                        <div class="card-body">
                            <a href="#">
                                <div class="skeleton skeleton-text"></div>
                                <div class="skeleton skeleton-text"></div>
                            </a>
                            <a>
                                <strong class="text-primary fs-5">
                                    <div class="skeleton skeleton-text skeleton-footer"></div>
                                </strong>
                                <br>
                                <div class="skeleton skeleton-text"></div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-3 d-flex align-items-stretch">
                    <div class="card cardproduct shadow-none">
                        <a class="hove btn stretched-link"></a>
                        <a href="#">
                            <div class="img-card">
                                <img src="{{ asset('/img/products/transparent.png') }}"
                                    class="skeleton card-img-top img-card d-inline mx-auto d-block imgproduct"
                                    style="height:20em;">
                            </div>
                        </a>
                        <div class="card-body">
                            <a href="#">
                                <div class="skeleton skeleton-text"></div>
                                <div class="skeleton skeleton-text"></div>
                            </a>
                            <a>
                                <strong class="text-primary fs-5">
                                    <div class="skeleton skeleton-text skeleton-footer"></div>
                                </strong>
                                <br>
                                <div class="skeleton skeleton-text"></div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="loading-animation hide-desktop">

        <div class="row">
            <div class="card mb-4">
                <div class="card-header h5 text-center">
                    <div class="skeleton skeleton-title"></div>
                </div>
                <div class="col-lg-3 mb-3 d-flex align-items-stretch">
                    <div class="card cardproduct shadow-none">
                        <a class="hove btn stretched-link"></a>
                        <a href="#">
                            <div class="img-card">
                                <img src="{{ asset('/img/products/transparent.png') }}"
                                    class="skeleton card-img-top img-card d-inline mx-auto d-block imgproduct"
                                    style="height:20em;">
                            </div>
                        </a>
                        <div class="card-body">
                            <a href="#">
                                <div class="skeleton skeleton-text"></div>
                                <div class="skeleton skeleton-text"></div>
                            </a>
                            <a>
                                <strong class="text-primary fs-5">
                                    <div class="skeleton skeleton-text skeleton-footer"></div>
                                </strong>
                                <br>
                                <div class="skeleton skeleton-text"></div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-3 d-flex align-items-stretch">
                    <div class="card cardproduct shadow-none">
                        <a class="hove btn stretched-link"></a>
                        <a href="#">
                            <div class="img-card">
                                <img src="{{ asset('/img/products/transparent.png') }}"
                                    class="skeleton card-img-top img-card d-inline mx-auto d-block imgproduct"
                                    style="height:20em;">
                            </div>
                        </a>
                        <div class="card-body">
                            <a href="#">
                                <div class="skeleton skeleton-text"></div>
                                <div class="skeleton skeleton-text"></div>
                            </a>
                            <a>
                                <strong class="text-primary fs-5">
                                    <div class="skeleton skeleton-text skeleton-footer"></div>
                                </strong>
                                <br>
                                <div class="skeleton skeleton-text"></div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-3 d-flex align-items-stretch">
                    <div class="card cardproduct shadow-none">
                        <a class="hove btn stretched-link"></a>
                        <a href="#">
                            <div class="img-card">
                                <img src="{{ asset('/img/products/transparent.png') }}"
                                    class="skeleton card-img-top img-card d-inline mx-auto d-block imgproduct"
                                    style="height:20em;">
                            </div>
                        </a>
                        <div class="card-body">
                            <a href="#">
                                <div class="skeleton skeleton-text"></div>
                                <div class="skeleton skeleton-text"></div>
                            </a>
                            <a>
                                <strong class="text-primary fs-5">
                                    <div class="skeleton skeleton-text skeleton-footer"></div>
                                </strong>
                                <br>
                                <div class="skeleton skeleton-text"></div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-3 d-flex align-items-stretch">
                    <div class="card cardproduct shadow-none">
                        <a class="hove btn stretched-link"></a>
                        <a href="#">
                            <div class="img-card">
                                <img src="{{ asset('/img/products/transparent.png') }}"
                                    class="skeleton card-img-top img-card d-inline mx-auto d-block imgproduct"
                                    style="height:20em;">
                            </div>
                        </a>
                        <div class="card-body">
                            <a href="#">
                                <div class="skeleton skeleton-text"></div>
                                <div class="skeleton skeleton-text"></div>
                            </a>
                            <a>
                                <strong class="text-primary fs-5">
                                    <div class="skeleton skeleton-text skeleton-footer"></div>
                                </strong>
                                <br>
                                <div class="skeleton skeleton-text"></div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="hide-desktop products-loaded">

        <div class="row">
            <div class="card mb-4">
                <div class="card-header h5 text-center">
                    <strong>{{ __('Featured Products') }}</strong>
                </div>
                @foreach ($viewData['products_featured'] as $product)
                    <div class="col-lg-3 mb-3 d-flex align-items-stretch">
                        <div class="card cardproduct shadow-none">
                            <a class="hove btn stretched-link"></a>
                            <a href="{{ route('product.show', ['id' => $product->getId()]) }}">
                                <div class="img-card">
                                    <img src="{{ asset('/img/products/' . $product->getImage()) }}"
                                        class="card-img-top img-card d-inline mx-auto d-block imgproduct"
                                        style="height:20em;">
                                </div>
                            </a>

                            <div class="card-body">
                                <a href="{{ route('product.show', ['id' => $product->getId()]) }}"><strong>{{ $product->getName() }}</strong>
                                </a>
                                <a>
                                    @if ($product->getPrice() == 0)
                                        <span><strong class="text-primary fs-5">{{ __('Free') }}</strong></span>
                                    @elseif ($product->getDiscountedprice() > 0)
                                        <strong class="text-secondary text-decoration-line-through fs-5">
                                            <x-money class="text-decoration-line-through"
                                                amount="{{ $product->getPrice() }}" currency="MXN" convert />
                                        </strong>
                                        <strong class="text-primary fs-5">
                                            <x-money class="text-decoration-line-through"
                                                amount="{{ $product->getPrice() - $product->getDiscountedprice() }}"
                                                currency="MXN" convert />
                                        </strong>
                                    @else
                                        <strong class="text-primary fs-5">
                                            <x-money class="text-decoration-line-through"
                                                amount="{{ $product->getPrice() }}" currency="MXN" convert />
                                        </strong>
                                    @endif
                                    <br>
                                    @if ($product->getStock() > 0)
                                        <span class="badge bg-primary text-white fs-6"><span class="iconify"
                                                data-icon="akar-icons:check"></span>
                                            {{ __('In stock') }}</span>
                                    @else
                                        <span class="badge bg-secondary text-white fs-6"><span class="iconify"
                                                data-icon="bi:x-lg"></span>

                                            {{ __('Out of stock') }}</span>
                                    @endif
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="card-header h5 text-center header2">
                    <a href="{{ route('product.more.featured') }}" style="color: #007fff;">{{ __('See more featured products') }}
                        ></a>
                </div>
            </div>
        </div>

    </div>


    <!--Products Sales-->

    <div class="hide-mobile">
        <div class="container-sm">
            <div class="p-3 border bg-light text-center fs-4"><strong>{{ __('Best Sellers') }}</strong></div>
        </div>
        <br>
        <div class="row">
            @foreach ($viewData['products_sales'] as $product)
                <div class="col-lg-3 mb-3 d-flex align-items-stretch">
                    <div class="card">
                        <a href="{{ route('product.show', ['id' => $product->getId()]) }}">
                            <div class="img-card">
                                <img src="{{ asset('/img/products/' . $product->getImage()) }}"
                                    class="card-img-top img-card d-inline mx-auto d-block" style="height:20em;">
                            </div>
                        </a>
                        <div class="card-body">
                            <a href="{{ route('product.show', ['id' => $product->getId()]) }}"><strong>{{ $product->getName() }}</strong>
                            </a>
                            <p></p>
                            <a>
                                @if ($product->getPrice() == 0)
                                    <span><strong class="text-primary fs-5">{{ __('Free') }}</strong></span>
                                @elseif ($product->getDiscountedprice() > 0)
                                    <strong class="text-secondary text-decoration-line-through fs-5">
                                        <x-money class="text-decoration-line-through" amount="{{ $product->getPrice() }}"
                                            currency="MXN" convert />
                                    </strong>
                                    <strong class="text-primary fs-5">
                                        <x-money class="text-decoration-line-through"
                                            amount="{{ $product->getPrice() - $product->getDiscountedprice() }}"
                                            currency="MXN" convert />
                                    </strong>
                                @else
                                    <strong class="text-primary fs-5">
                                        <x-money class="text-decoration-line-through" amount="{{ $product->getPrice() }}"
                                            currency="MXN" convert />
                                    </strong>
                                @endif
                                <br>
                                <br>
                                @if ($product->getStock() > 0)
                                    <span class="badge bg-primary text-white fs-6"><span class="iconify"
                                            data-icon="akar-icons:check"></span>
                                        {{ __('In stock') }}</span>
                                    {{-- // add to cart --}}
                                    <form action="{{ route('cart.add', ['id' => $product->getId()]) }}" method="POST">
                                        @csrf
                                        <br>
                                        <input type="hidden" name="quantity" value="1">
                                        <button type="submit" class="hov btn btn-primary btn-sm fs-6">
                                            <span class="iconify" data-icon="mi:shopping-cart-add"></span>
                                            <strong class="titledark">{{ __('Add to cart') }}</strong>
                                        </button>
                                    </form>
                                @else
                                    <span class="badge bg-secondary text-white fs-6"><span class="iconify"
                                            data-icon="bi:x-lg"></span>

                                        {{ __('Out of stock') }}</span>
                                @endif
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="p-3 bg-light text-center fs-4"><a href="{{ route('product.more.sales') }}"
                style="color: #007fff;">{{ __('See more best sellers') }} ></a></div>
    </div>
    <br>

    <!--Products Sales Mobile-->


    <div class="hide-desktop products-loaded">
        <div class="row">
            <div class="card mb-4">
                <div class="card-header h5 text-center">
                    <strong>{{ __('Best Sellers') }}</strong>
                </div>
                @foreach ($viewData['products_sales'] as $product)
                    <div class="col-lg-3 mb-3 d-flex align-items-stretch">
                        <div class="card cardproduct shadow-none">
                            <a class="hove btn stretched-link"></a>
                            <a href="{{ route('product.show', ['id' => $product->getId()]) }}">
                                <div class="img-card">
                                    <img src="{{ asset('/img/products/' . $product->getImage()) }}"
                                        class="card-img-top img-card d-inline mx-auto d-block imgproduct"
                                        style="height:20em;">
                                </div>
                            </a>
                            {{-- <div class="card-body text-center"> --}}
                            <div class="card-body">
                                <a href="{{ route('product.show', ['id' => $product->getId()]) }}"><strong>{{ $product->getName() }}</strong>
                                </a>
                                <a>
                                    @if ($product->getPrice() == 0)
                                        <span><strong class="text-primary fs-5">{{ __('Free') }}</strong></span>
                                    @elseif ($product->getDiscountedprice() > 0)
                                        <strong class="text-secondary text-decoration-line-through fs-5">
                                            <x-money class="text-decoration-line-through"
                                                amount="{{ $product->getPrice() }}" currency="MXN" convert />
                                        </strong>
                                        <strong class="text-primary fs-5">
                                            <x-money class="text-decoration-line-through"
                                                amount="{{ $product->getPrice() - $product->getDiscountedprice() }}"
                                                currency="MXN" convert />
                                        </strong>
                                    @else
                                        <strong class="text-primary fs-5">
                                            <x-money class="text-decoration-line-through"
                                                amount="{{ $product->getPrice() }}" currency="MXN" convert />
                                        </strong>
                                    @endif
                                    <br>
                                    @if ($product->getStock() > 0)
                                        <span class="badge bg-primary text-white fs-6"><span class="iconify"
                                                data-icon="akar-icons:check"></span>
                                            {{ __('In stock') }}</span>
                                    @else
                                        <span class="badge bg-secondary text-white fs-6"><span class="iconify"
                                                data-icon="bi:x-lg"></span>

                                            {{ __('Out of stock') }}</span>
                                    @endif
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="card-header h5 text-center header2">
                    <a href="{{ route('product.more.sales') }}" style="color: #007fff;">{{ __('See more best sellers') }}
                        ></a>
                </div>
            </div>
        </div>
    </div>

    <!--trademarks-->

    <div class="hide-mobile">
        <div class="container-sm">
            <div class="p-3 border bg-light text-center fs-4"><strong>{{ __('The best trademarks') }}</strong></div>
        </div>
        <br>
        <div class="row">
            <div class="col" style="width: 10rem;">
                <a href="{{ route('trademark.dell') }}">
                    <img src="{!! asset('img/carrousel/dell.png') !!}" class="img-fluid" alt="...">
                </a>
            </div>
            <div class="col" style="width: 10rem;">
                <a href="{{ route('trademark.gigabyte') }}">
                    <img src="{!! asset('img/carrousel/gygabyte.png') !!}" class="img-fluid" alt="...">
                </a>
            </div>
            <div class="col" style="width: 10rem;">
                <a href="{{ route('trademark.nvidia') }}">
                    <img src="{!! asset('img/carrousel/nvidia.png') !!}" class="img-fluid" alt="...">
                </a>
            </div>
            <div class="col" style="width: 10rem;">
                <a href="{{ route('trademark.hp') }}">
                    <img src="{!! asset('img/carrousel/hp.png') !!}" class="img-fluid" alt="...">
                </a>
            </div>
        </div>
    </div>
    <br>
    <!--New Products-->

    <div class="hide-mobile">
        <div class="container-sm">
            <div class="p-3 border bg-light text-center fs-4"><strong>{{ __('New products') }}</strong></div>
        </div>
        <br>
        <div class="row">
            @foreach ($viewData['products_new'] as $product)
                <div class="col-lg-3 mb-3 d-flex align-items-stretch">
                    <div class="card">
                        <a href="{{ route('product.show', ['id' => $product->getId()]) }}">
                            <div class="img-card">
                                <img src="{{ asset('/img/products/' . $product->getImage()) }}"
                                    class="card-img-top img-card d-inline mx-auto d-block"
                                    style="height:20em; width:20em;">
                            </div>
                        </a>
                        <div class="card-body">
                            <a href="{{ route('product.show', ['id' => $product->getId()]) }}"><strong>{{ $product->getName() }}</strong>
                            </a>
                            <p></p>
                            <a>
                                @if ($product->getPrice() == 0)
                                    <span><strong class="text-primary fs-5">{{ __('Free') }}</strong></span>
                                @elseif ($product->getDiscountedprice() > 0)
                                    <strong class="text-secondary text-decoration-line-through fs-5">
                                        <x-money class="text-decoration-line-through" amount="{{ $product->getPrice() }}"
                                            currency="MXN" convert />
                                    </strong>
                                    <strong class="text-primary fs-5">
                                        <x-money class="text-decoration-line-through"
                                            amount="{{ $product->getPrice() - $product->getDiscountedprice() }}"
                                            currency="MXN" convert />
                                    </strong>
                                @else
                                    <strong class="text-primary fs-5">
                                        <x-money class="text-decoration-line-through" amount="{{ $product->getPrice() }}"
                                            currency="MXN" convert />
                                    </strong>
                                @endif
                                <br>
                                <br>
                                @if ($product->getStock() > 0)
                                    <span class="badge bg-primary text-white fs-6"><span class="iconify"
                                            data-icon="akar-icons:check"></span>
                                        {{ __('In stock') }}</span>
                                    {{-- // add to cart --}}
                                    <form action="{{ route('cart.add', ['id' => $product->getId()]) }}" method="POST">
                                        @csrf
                                        <br>
                                        <input type="hidden" name="quantity" value="1">
                                        <button type="submit" class="hov btn btn-primary btn-sm fs-6">
                                            <span class="iconify" data-icon="mi:shopping-cart-add"></span>
                                            <strong class="titledark">{{ __('Add to cart') }}</strong>
                                        </button>
                                    </form>
                                @else
                                    <span class="badge bg-secondary text-white fs-6"><span class="iconify"
                                            data-icon="bi:x-lg"></span>

                                        {{ __('Out of stock') }}</span>
                                @endif
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="p-3 bg-light text-center fs-4"><a href="{{ route('product.more.new') }}" style="color: #007fff;">{{ __('See more new products') }}
            ></a></div>
        <br>
    </div>

    <!--New Products-->

    <div class="hide-desktop products-loaded">
        <div class="row">
            <div class="card mb-4">
                <div class="card-header h5 text-center">
                    <strong>{{ __('New products') }}</strong>
                </div>
                @foreach ($viewData['products_new'] as $product)
                    <div class="col-lg-3 mb-3 d-flex align-items-stretch">
                        <div class="card cardproduct shadow-none">
                            <a class="hove btn stretched-link"></a>
                            <a href="{{ route('product.show', ['id' => $product->getId()]) }}">
                                <div class="img-card">
                                    <img src="{{ asset('/img/products/' . $product->getImage()) }}"
                                        class="card-img-top img-card d-inline mx-auto d-block imgproduct"
                                        style="height:20em;">
                                </div>
                            </a>
                            {{-- <div class="card-body text-center"> --}}
                            <div class="card-body">
                                <a href="{{ route('product.show', ['id' => $product->getId()]) }}"><strong>{{ $product->getName() }}</strong>
                                </a>
                                <a>
                                    @if ($product->getPrice() == 0)
                                        <span><strong class="text-primary fs-5">{{ __('Free') }}</strong></span>
                                    @elseif ($product->getDiscountedprice() > 0)
                                        <strong class="text-secondary text-decoration-line-through fs-5">
                                            <x-money class="text-decoration-line-through"
                                                amount="{{ $product->getPrice() }}" currency="MXN" convert />
                                        </strong>
                                        <strong class="text-primary fs-5">
                                            <x-money class="text-decoration-line-through"
                                                amount="{{ $product->getPrice() - $product->getDiscountedprice() }}"
                                                currency="MXN" convert />
                                        </strong>
                                    @else
                                        <strong class="text-primary fs-5">
                                            <x-money class="text-decoration-line-through"
                                                amount="{{ $product->getPrice() }}" currency="MXN" convert />
                                        </strong>
                                    @endif
                                    <br>
                                    @if ($product->getStock() > 0)
                                        <span class="badge bg-primary text-white fs-6"><span class="iconify"
                                                data-icon="akar-icons:check"></span>
                                            {{ __('In stock') }}</span>
                                    @else
                                        <span class="badge bg-secondary text-white fs-6"><span class="iconify"
                                                data-icon="bi:x-lg"></span>

                                            {{ __('Out of stock') }}</span>
                                    @endif
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="card-header h5 text-center header2">
                    <a href="{{ route('product.more.new') }}" style="color: #007fff;">{{ __('See more new products') }}
                         ></a>
                </div>
            </div>
        </div>
    </div>
    <div class="hide-desktop">
        <!--space for bottom navbar-->
        {{-- <div class="container-sm">
            <div class="p-3 border bg-light text-center fs-4"><strong>bottom navbar</strong></div>
        </div> --}}
        <!--space for bottom navbar-->
    </div>
@endsection
