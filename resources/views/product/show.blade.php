@extends('layouts.app')
@section('content')



    <div class="card mb-3">
        <div class="row g-0">
            <div class="col-md-4">
                <img src="{{ asset('/img/products/' . $viewData['product']->getImage()) }}" class="img-fluid rounded-start">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h3 class="card-title">
                        <strong>{{ $viewData['product']->getName() }}</strong> <br><br>
                        <strong>
                            @if ($viewData['product']->getPrice() == 0)
                                <span class="fs-1 text-primary">{{ __('Free') }}</span>
                            @elseif ($viewData['product']->getDiscountedprice() > 0)
                                <span class="fs-1 text-primary"><strong class="text-decoration-line-through">
                                        <x-money class="text-decoration-line-through"
                                            amount="{{ $viewData['product']->getPrice() }}" currency="MXN" convert /></span>
                        </strong>
                        <span class="fs-1 text-primary">
                            <x-money
                                amount="{{ $viewData['product']->getPrice() - $viewData['product']->getDiscountedprice() }}"
                                currency="MXN" convert />
                        </span>
                    @else
                        <span class="fs-1 text-primary">
                            <x-money amount="{{ $viewData['product']->getPrice() }}" currency="MXN" convert />
                        </span>
                        @endif
                        </strong>
                        </h5>
                        <br>
                        <h5><strong>{{ __('Description') }}:</strong></h5>
                        <p class="card-text">{{ $viewData['product']->getDescription() }}</p>
                        <p class="card-text">{{ __('Category') }}:
                            <strong>{{ $viewData['product']->getCategory() }}</strong>
                        </p>
                        <br>
                        @if ($viewData['product']->getStock() > 0)
                            <h5><strong>{{ __('Stock') }}:
                                </strong> {{ $viewData['product']->getStock() }}</h5>
                        @else
                            <h5><strong class="text-danger">{{ __('Out of stock') }}</strong></h5>
                            </h5>
                        @endif
                        <br>

                        {{-- obtain the average number of stars --}}
                        @php
                            $average = 0;
                            $count = 0;
                            foreach ($viewData['comments'] as $comments) {
                                $average += $comments->stars;
                                $count++;
                            }
                            if ($count > 0) {
                                $average = $average / $count;
                            }
                        @endphp

                        <div class="my-rating" data-rating="{{ $average }}">
                            @switch($average)
                                @case(0)
                                    <span class="iconify" data-icon="fluent-mdl2:favorite-star" style="font-size: 24px;"></span>
                                    <span class="iconify" data-icon="fluent-mdl2:favorite-star" style="font-size: 24px;"></span>
                                    <span class="iconify" data-icon="fluent-mdl2:favorite-star" style="font-size: 24px;"></span>
                                    <span class="iconify" data-icon="fluent-mdl2:favorite-star" style="font-size: 24px;"></span>
                                    <span class="iconify" data-icon="fluent-mdl2:favorite-star" style="font-size: 24px;"></span>
                                @break

                                @case($average >= 1 && $average < 2)
                                    <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                    <span class="iconify" data-icon="fluent-mdl2:favorite-star" style="font-size: 24px;"></span>
                                    <span class="iconify" data-icon="fluent-mdl2:favorite-star" style="font-size: 24px;"></span>
                                    <span class="iconify" data-icon="fluent-mdl2:favorite-star" style="font-size: 24px;"></span>
                                    <span class="iconify" data-icon="fluent-mdl2:favorite-star" style="font-size: 24px;"></span>
                                @break

                                @case($average >= 2 && $average < 3)
                                    <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                    <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                    <span class="iconify" data-icon="fluent-mdl2:favorite-star" style="font-size: 24px;"></span>
                                    <span class="iconify" data-icon="fluent-mdl2:favorite-star" style="font-size: 24px;"></span>
                                    <span class="iconify" data-icon="fluent-mdl2:favorite-star" style="font-size: 24px;"></span>
                                @break

                                @case($average >= 3 && $average < 4)
                                    <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                    <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                    <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                    <span class="iconify" data-icon="fluent-mdl2:favorite-star" style="font-size: 24px;"></span>
                                    <span class="iconify" data-icon="fluent-mdl2:favorite-star" style="font-size: 24px;"></span>
                                @break

                                @case($average >= 4 && $average < 5)
                                    <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                    <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                    <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                    <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                    <span class="iconify" data-icon="fluent-mdl2:favorite-star" style="font-size: 24px;"></span>
                                @break

                                @case($average == 5)
                                    <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                    <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                    <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                    <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                    <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                @break

                                @default
                            @endswitch
                        </div>

                        <p class="card-text">
                        <form method="POST" action="{{ route('cart.add', ['id' => $viewData['product']->getId()]) }}">
                            <div class="row">
                                @csrf
                                <div class="col-auto">
                                    <div class="input-group col-auto">
                                        <div class="input-group-text">{{ __('Quantity') }}:</div>

                                        @if ($viewData['product']->getStock() > 0)
                                            <input type="number" class="form-control" name="quantity" value="1"
                                                min="1" max="{{ $viewData['product']->getStock() }}">
                                        @else
                                            <input type="number" class="form-control" name="quantity" value="0"
                                                min="0" max="{{ $viewData['product']->getStock() }}" disabled>
                                        @endif
                                    </div>
                                </div>

                                @if ($viewData['product']->getStock() > 0)
                                    <div class="col-auto">
                                        <button class="btn btn-black text-white" type="submit">
                                            {{ __('Add to cart') }} </button>
                                    </div>
                                @else
                                    <div class="col-auto">
                                        <button type="submit" class="btn btn-black text-white" disabled>
                                            {{ __('Add to cart') }} </button>
                                @endif

                            </div>
                        </form>

                        </p>
                </div>
            </div>
        </div>



        <table class="table table-borderless table-striped text-center mt-3">
            <thead>
                <tr>
                    <th scope="col" colspan="2" class="text-center">{{ __('Details') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><strong>{{ __('Trademark') }}</strong></td>
                    <td>{{ $viewData['product']->getTrademark() }}</td>
                </tr>
                <tr>
                    <td><strong>{{ __('Type') }}</strong></td>
                    <td>{{ $viewData['product']->getSubcategory() }}</td>
                </tr>
            </tbody>
        </table>
    </div>


    <div class="hide-desktop">
        <div class="card">
            <div class="card-header h5 text-center headercom">
                <strong>{{ __('User reviews') }}</strong>
            </div>
            {{-- comentarios --}}
            @if (count($viewData['comments']) > 0)
                @foreach ($viewData['comments'] as $comments)
                    <div class="row">
                        <div class="card-body comments">
                            <div class="row">
                                <div class="card-body">
                                    <h5 class="card-title"><strong>{{ $comments->user_name }}</strong></h5>
                                    <p class="card-text">{{ $comments->comment }}</p>
                                    <p class="card-text"><small class="text-muted">{{ __('Date') }}:
                                            {{ Carbon\Carbon::parse($comments->created_at)->subHours(3)->subMinutes(2) }}</small>
                                    </p>
                                    @switch($comments->stars)
                                        @case(1)
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                style="font-size: 24px;"></span>
                                        @break

                                        @case(2)
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                style="font-size: 24px;"></span>
                                        @break

                                        @case(3)
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                style="font-size: 24px;"></span>
                                        @break

                                        @case(4)
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                style="font-size: 24px;"></span>
                                        @break

                                        @case(5)
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                        @break

                                        @default
                                            <p>{{ __('Without rating') }}</p>
                                    @endswitch
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="my-4">
                @endforeach
            @else
                <div class="row">
                    <div class="card-body nocomments">
                        <div class="row">
                            <div class="card-body">
                                <hr class="my-4">
                                <h5 class="card-title text-center">{{ __('No one has commented on this product yet.') }}
                                </h5>
                                <hr class="my-4">
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>

    <div class="hide-mobile">
        <div class="card">
            <div class="card-header h5 text-center headercom">
                <strong>{{ __('User reviews') }}</strong>
            </div>
            {{-- comentarios --}}
            {{-- si el producto tiene comentarios --}}
            @if (count($viewData['comments']) > 0)
                @foreach ($viewData['comments'] as $comments)
                    <div class="row">
                        <div class="card-body comments">
                            <div class="row">
                                <div class="card-body">
                                    <h5 class="card-title"><strong>{{ $comments->user_name }}</strong></h5>
                                    <p class="card-text">{{ $comments->comment }}</p>
                                    <p class="card-text"><small class="text-muted">{{ __('Date') }}:
                                            {{ Carbon\Carbon::parse($comments->created_at)->subHours(3)->subMinutes(2) }}</small>
                                    </p>
                                    @switch($comments->stars)
                                        @case(1)
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                style="font-size: 24px;"></span>
                                        @break

                                        @case(2)
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                style="font-size: 24px;"></span>
                                        @break

                                        @case(3)
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                style="font-size: 24px;"></span>
                                        @break

                                        @case(4)
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                style="font-size: 24px;"></span>
                                        @break

                                        @case(5)
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                            <span class="iconify" data-icon="emojione:star" style="font-size: 24px;"></span>
                                        @break

                                        @default
                                            <p>{{ __('Without rating') }}</p>
                                    @endswitch
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="my-4">
                @endforeach
            @else
                <div class="row">
                    <div class="card-body nocomments">
                        <div class="row">
                            <div class="card-body">
                                <hr class="my-4">
                                <h5 class="card-title text-center">{{ __('No one has commented on this product yet.') }}
                                </h5>
                                <hr class="my-4">
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>


    <div class="hide-mobile">
        {!! $viewData['comments']->withQueryString()->links('layouts.pagination') !!}
    </div>

    <div class="hide-desktop mobile-pagination">
        {!! $viewData['comments']->withQueryString()->links('layouts.paginationm') !!}
    </div>
@endsection
