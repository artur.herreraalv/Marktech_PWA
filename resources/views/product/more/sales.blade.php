@extends('layouts.navbar')
@section('content')

    {{-- submit search with checkbox --}}
    <script>
        $(document).ready(function() {
            $("#search-form").on("change", "input:checkbox", function() {
                $("#search-form").submit();
            });
        });
    </script>

    {{-- submit search with select --}}
    <script>
        $(document).ready(function() {
            $("#search-form").on("change", "select", function() {
                $("#search-form").submit();
            });
        });
    </script>


    {{-- // show loading-animation when loading products from database --}}
    <script>
        $(document).ready(function() {
            if ($(window).width() < 992) {
                // hide all hide-desktop elements
                $('.products-loaded').hide();
                $('.loading-animation').ajaxStart(function() {
                    $(this).show();
                })
                // check if all ajax requests are finished
                var checkAjax = setInterval(function() {
                    if ($.active == 0) {
                        $('.loading-animation').hide();
                        $('.products-loaded').show();
                        clearInterval(checkAjax);
                    } else { // if not finished, check again in 100ms
                        $('.loading-animation').show();
                        $('.products-loaded').hide();
                    }
                }, 1000);
            }
        });
    </script>
    {{-- when the page resizes 992 reload the page --}}
    <script>
        $(window).resize(function() {
            if ($(window).width() < 992) {
                location.reload();
            }
        });
    </script>

    <div class="products-loaded">

        <section id="filters">
            <div class="container position-relative">
                <form id="search-form" method="GET">


                    {{-- save search in cache and get it in input script --}}
                    {{-- <script>
                        $(document).ready(function() {
                            var url = window.location.href;
                            // get the value of search from the current url before the first &
                            var barra = url.substring(url.indexOf('=') + 1, url.indexOf('&'));
                            // var barra = url.split("barra=")[1];
                            if (barra) {
                                $('input[name="barra"]').val(barra);
                            }
                        });
                    </script> --}}


                    <div class="hide-mobile desktop-pagination">
                        {!! $viewData['products_sales']->withQueryString()->links('layouts.pagination') !!}
                    </div>

                    <div class="hide-desktop mobile-pagination">
                        {!! $viewData['products_sales']->withQueryString()->links('layouts.paginationm') !!}
                    </div>

                </form>
            </div>
        </section>

        <script>
            $(document).ready(function() {
                $("#search-form-mobile").on("change", "input:checkbox", function() {
                    $("#search-form-mobile").submit();
                });
            });
        </script>

        {{-- submit search with select --}}
        <script>
            $(document).ready(function() {
                $("#search-form-mobile").on("change", "select", function() {
                    $("#search-form-mobile").submit();
                });
            });
        </script>

        <section id="filters-mobile">
            <div class="container position-relative">
                <form id="search-form-mobile" method="GET">


                    <div class="hide-mobile desktop-pagination">
                        {!! $viewData['products_sales']->withQueryString()->links('layouts.pagination') !!}
                    </div>

                    <div class="hide-desktop mobile-pagination">
                        {!! $viewData['products_sales']->withQueryString()->links('layouts.paginationm') !!}
                    </div>

                </form>
            </div>
            <br>
        </section>

        <div class="hide-mobile">
            @if (isset($viewData['products_sales']))
                @foreach ($viewData['products_sales'] as $product)
                    <div class="container ">
                        <div class="card mb-3 mx-auto" style="max-width: 840px;">
                            <div class="row no-gutters">

                                <div class="col-md-4">

                                    <a href="{{ route('product.show', ['id' => $product->id]) }}">
                                        <div class="img-card">
                                            <img src="{{ asset('/img/products/' . $product->image) }}" width="200px"
                                                height="200px" alt="imagen"
                                                class="card-img-top img-card d-inline mx-auto d-block">
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body">
                                        <a href="{{ route('product.show', ['id' => $product->id]) }}">
                                            <h5 class="card-title"><b>{{ $product->name }}</b></h5>
                                        </a>
                                        @if ($product->getPrice() == 0)
                                            <span><strong class="text-primary fs-5">{{ __('Free') }}</strong></span>
                                        @elseif ($product->getDiscountedprice() > 0)
                                            <strong class="text-secondary text-decoration-line-through fs-5">
                                                <x-money class="text-decoration-line-through"
                                                    amount="{{ $product->getPrice() }}" currency="MXN" convert />
                                            </strong>
                                            <strong class="text-primary fs-5">
                                                <x-money class="text-decoration-line-through"
                                                    amount="{{ $product->getPrice() - $product->getDiscountedprice() }}"
                                                    currency="MXN" convert />
                                            </strong>
                                        @else
                                            <strong class="text-primary fs-5">
                                                <x-money class="text-decoration-line-through"
                                                    amount="{{ $product->getPrice() }}" currency="MXN" convert />
                                            </strong>
                                        @endif
                                        <br>
                                        <br>
                                        @if ($product->getStock() > 0)
                                            <span class="badge bg-primary text-white fs-6"><span class="iconify"
                                                    data-icon="akar-icons:check"></span>
                                                {{ __('In stock') }}</span>
                                            {{-- // add to cart --}}
                                            <form action="{{ route('cart.add', ['id' => $product->getId()]) }}"
                                                method="POST">
                                                @csrf
                                                <br>
                                                <input type="hidden" name="quantity" value="1">
                                                <button type="submit" class="hov btn btn-primary btn-sm fs-6">
                                                    <span class="iconify" data-icon="mi:shopping-cart-add"></span>
                                                    <strong class="titledark">{{ __('Add to cart') }}</strong>
                                                </button>
                                            </form>
                                       @else
                                            <span class="badge bg-secondary text-white fs-6"><span class="iconify"
                                                    data-icon="bi:x-lg"></span>

                                                {{ __('Out of stock') }}</span>
                                        @endif

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>

        <div class="hide-desktop">
            @if (isset($viewData['products_sales']))
                <div class="card mb-4">
                    @foreach ($viewData['products_sales'] as $product)
                        <div class="col-lg-3 mb-3 d-flex align-items-stretch">
                            <div class="card cardproduct shadow-none">
                                <a class="hove btn stretched-link"></a>
                                <a href="{{ route('product.show', ['id' => $product->getId()]) }}">
                                    <div class="img-card">
                                        <img src="{{ asset('/img/products/' . $product->getImage()) }}"
                                            class="card-img-top img-card d-inline mx-auto d-block imgproduct"
                                            style="height:20em;">
                                    </div>
                                </a>
                                {{-- <div class="card-body text-center"> --}}
                                <div class="card-body">
                                    <a href="{{ route('product.show', ['id' => $product->getId()]) }}"><strong>{{ $product->getName() }}</strong>
                                    </a>
                                    <a>
                                        @if ($product->getPrice() == 0)
                                            <span><strong class="text-primary fs-5">{{ __('Free') }}</strong></span>
                                        @elseif ($product->getDiscountedprice() > 0)
                                            <strong class="text-secondary text-decoration-line-through fs-5">
                                                <x-money class="text-decoration-line-through"
                                                    amount="{{ $product->getPrice() }}" currency="MXN" convert />
                                            </strong>
                                            <strong class="text-primary fs-5">
                                                <x-money class="text-decoration-line-through"
                                                    amount="{{ $product->getPrice() - $product->getDiscountedprice() }}"
                                                    currency="MXN" convert />
                                            </strong>
                                        @else
                                            <strong class="text-primary fs-5">
                                                <x-money class="text-decoration-line-through"
                                                    amount="{{ $product->getPrice() }}" currency="MXN" convert />
                                            </strong>
                                        @endif
                                        <br>
                                        @if ($product->getStock() > 0)
                                            <span class="badge bg-primary text-white fs-6"><span class="iconify"
                                                    data-icon="akar-icons:check"></span>
                                                {{ __('In stock') }}</span>
                                       @else
                                            <span class="badge bg-secondary text-white fs-6"><span class="iconify"
                                                    data-icon="bi:x-lg"></span>

                                                {{ __('Out of stock') }}</span>
                                        @endif
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>

        <div class="hide-mobile">
            {!! $viewData['products_sales']->withQueryString()->links('layouts.pagination') !!}
        </div>

        <div class="hide-desktop mobile-pagination">
            {!! $viewData['products_sales']->withQueryString()->links('layouts.paginationm') !!}
        </div>
    </div>

    <div class="container-sm">
        <div class="p-3 borderless bg-transparent text-center fs-4"><strong><span class="iconify"
                    data-icon="codicon:blank" style="font-size: 32px;"></span></strong></div>
    </div>
    {{-- loading animation  --}}

    <div class="loading-animation hide-desktop">
        <div class="container position-relative" id="container">
            <form id="search-form" method="GET">
                <input type="text" class="form-control mx-auto" name="barra" placeholder="{{ __('Search products...') }}"
                    style="width: 300px; height: 50px"><br>

                <div class="hide-desktop">
                    <div class="card mb-4">
                        <div class="col-lg-3 mb-3 d-flex align-items-stretch">
                            <div class="card cardproduct shadow-none">
                                <a class="hove btn stretched-link"></a>
                                <a href="#">
                                    <div class="img-card">
                                        <img src="{{ asset('/img/products/transparent.png') }}"
                                            class="skeleton card-img-top img-card d-inline mx-auto d-block imgproduct"
                                            style="height:20em;">
                                    </div>
                                </a>
                                {{-- <div class="card-body text-center"> --}}
                                <div class="card-body">
                                    <a href="#">
                                        <div class="skeleton skeleton-text"></div>
                                        <div class="skeleton skeleton-text"></div>
                                    </a>
                                    <a>
                                        <strong class="text-primary fs-5">
                                            <div class="skeleton skeleton-text skeleton-footer"></div>
                                        </strong>
                                        <br>
                                        <div class="skeleton skeleton-text"></div>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 mb-3 d-flex align-items-stretch">
                            <div class="card cardproduct shadow-none">
                                <a class="hove btn stretched-link"></a>
                                <a href="#">
                                    <div class="img-card">
                                        <img src="{{ asset('/img/products/transparent.png') }}"
                                            class="skeleton card-img-top img-card d-inline mx-auto d-block imgproduct"
                                            style="height:20em;">
                                    </div>
                                </a>
                                {{-- <div class="card-body text-center"> --}}
                                <div class="card-body">
                                    <a href="#">
                                        <div class="skeleton skeleton-text"></div>
                                        <div class="skeleton skeleton-text"></div>
                                    </a>
                                    <a>
                                        <strong class="text-primary fs-5">
                                            <div class="skeleton skeleton-text skeleton-footer"></div>
                                        </strong>
                                        <br>
                                        <div class="skeleton skeleton-text"></div>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 mb-3 d-flex align-items-stretch">
                            <div class="card cardproduct shadow-none">
                                <a class="hove btn stretched-link"></a>
                                <a href="#">
                                    <div class="img-card">
                                        <img src="{{ asset('/img/products/transparent.png') }}"
                                            class="skeleton card-img-top img-card d-inline mx-auto d-block imgproduct"
                                            style="height:20em;">
                                    </div>
                                </a>
                                {{-- <div class="card-body text-center"> --}}
                                <div class="card-body">
                                    <a href="#">
                                        <div class="skeleton skeleton-text"></div>
                                        <div class="skeleton skeleton-text"></div>
                                    </a>
                                    <a>
                                        <strong class="text-primary fs-5">
                                            <div class="skeleton skeleton-text skeleton-footer"></div>
                                        </strong>
                                        <br>
                                        <div class="skeleton skeleton-text"></div>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 mb-3 d-flex align-items-stretch">
                            <div class="card cardproduct shadow-none">
                                <a class="hove btn stretched-link"></a>
                                <a href="#">
                                    <div class="img-card">
                                        <img src="{{ asset('/img/products/transparent.png') }}"
                                            class="skeleton card-img-top img-card d-inline mx-auto d-block imgproduct"
                                            style="height:20em;">
                                    </div>
                                </a>
                                {{-- <div class="card-body text-center"> --}}
                                <div class="card-body">
                                    <a href="#">
                                        <div class="skeleton skeleton-text"></div>
                                        <div class="skeleton skeleton-text"></div>
                                    </a>
                                    <a>
                                        <strong class="text-primary fs-5">
                                            <div class="skeleton skeleton-text skeleton-footer"></div>
                                        </strong>
                                        <br>
                                        <div class="skeleton skeleton-text"></div>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

    @endsection
