@extends('layouts.admin')
@section('title', $viewData['title'])
@section('content')

    <div class="card">
        <div class="card-header">
            {{ __('Manage Items') }}
        </div>
        <div class="card-body">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">{{ __('Quantity') }}</th>
                        <th scope="col">{{ __('Price') }}</th>
                        <th scope="col">{{ __('Order') }}</th>
                        <th scope="col">{{ __('Product Code') }}</th>
                        {{-- <th scope="col">Modificar</th> --}}
                        {{-- <th scope="col">Eliminar</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @foreach ($viewData['products'] as $product)
                        <tr>
                            <td>{{ $product->getId() }}</td>
                            <td>{{ $product->getQuantity() }}</td>
                            <td>{{ $product->getPrice() }}</td>
                            <td>{{ $product->getOrderId() }}</td>
                            <td>{{ $product->getProductId() }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
