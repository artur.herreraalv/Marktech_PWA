@extends('layouts.admin')
@section('title', $viewData['title'])
@section('content')
    <div class="card mb-4">
        <div class="card-header">
            {{ __('Add User') }}
        </div>
        <div class="card-body">
            @if ($errors->any())
                <ul class="alert alert-danger list-unstyled">
                    @foreach ($errors->all() as $error)
                        <li> - {{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            <form method="POST" action="{{ route('admin.user.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col">
                        <div class="mb-3 row">
                            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">{{ __('Name') }}</label>
                            <div class="col-lg-10 col-md-6 col-sm-12">
                                <input name="name" value="{{ old('name') }}" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="mb-3 row">
                            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">{{ __('Email') }}</label>
                            <div class="col-lg-10 col-md-6 col-sm-12">
                                <input name="email" value="{{ old('email') }}" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label class="form-label">{{ __('Password') }}</label>
                    <input name="password" value="{{ old('password') }}" type="text" class="form-control">
                </div>
                <select class="form-select mb-4" name="role" value="{{ old('role') }}" aria-label="Default select example" required>
                    <option selected>{{ __('Select Role') }}</option>
                    <option value="client">{{ __('Client') }}</option>
                    <option value="admin">{{ __('Admin') }}</option>
                </select>
                <div class="mb-3">
                    <label class="form-label">{{ __('Spent') }}</label>
                    <input name="balance" value="{{ old('balance') }}" type="text" class="form-control">
                </div>
                <button type="submit" class="btn btn-black">{{ __('Save') }}</button>
        </div>
        </form>
    </div>

    <div class="card">
        <div class="card-header">
            {{ __('Manage Users') }}
        </div>
        <div class="card-body">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">{{ __('Name') }}</th>
                        <th scope="col">{{ __('Email') }}</th>
                        <th scope="col">{{ __('Password Hash') }}</th>
                        <th scope="col">{{ __('Role') }}</th>
                        <th scope="col">{{ __('Edit') }}</th>
                        <th scope="col">{{ __('Delete') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($viewData['products'] as $product)
                        <tr>
                            <td>{{ $product->getId() }}</td>
                            <td>{{ $product->getName() }}</td>
                            <td>{{ $product->getEmail() }}</td>
                            <td>{{ $product->getPassword() }}</td>
                            <td>{{ $product->getRole() }}</td>
                            <td>
                                <a class="btn btn-black"
                                    href="{{ route('admin.user.edit', ['id' => $product->getId()]) }}">
                                    <i class="bi-pencil"></i>
                                </a>
                            </td>
                            <td>
                                <form action="{{ route('admin.user.delete', $product->getId()) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger">
                                        <i class="bi-trash"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
