@extends('layouts.admin')
@section('title', $viewData['title'])
@section('content')
    <div class="card mb-4">
        <div class="card-header">
            {{ __('Modify User') }}
        </div>
        <div class="card-body">
            @if ($errors->any())
                <ul class="alert alert-danger list-unstyled">
                    @foreach ($errors->all() as $error)
                        <li>- {{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('admin.user.update', ['id' => $viewData['product']->getId()]) }}"
                enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col">
                        <div class="mb-3 row">
                            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">
                                {{ __('Name') }} </label>
                            <div class="col-lg-10 col-md-6 col-sm-12">
                                <input name="name" value="{{ $viewData['product']->getName() }}" type="text"
                                    class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="mb-3 row">
                            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">
                                {{ __('Email') }} </label>
                            <div class="col-lg-10 col-md-6 col-sm-12">
                                <input name="email" value="{{ $viewData['product']->getEmail() }}" type="text"
                                    class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label class="form-label">{{ __('Password') }}</label>
                    <input name="password" value="{{ $viewData['product']->getPassword() }}" type="text" class="form-control" disabled>
                </div>
                <select class="form-select mb-4" name="role" value="{{ $viewData['product']->getRole() }}" aria-label="Default select example" required>
                    <option selected>{{ __('Select Role') }}</option>
                    <option value="client">{{ __('Client') }}</option>
                    <option value="admin">{{ __('Admin') }}</option>
                </select>
                <div class="mb-3">
                    <label class="form-label">{{ __('Spent') }}</label>
                    <input name="balance" value="{{ $viewData['product']->getBalance() }}" type="text" class="form-control">
                </div>
                <button type="submit" class="btn btn-black">{{ __('Save') }}</button>
            </form>
        </div>
    </div>
@endsection
