@extends('layouts.admin')
@section('title', $viewData['title'])
@section('content')
    <div class="card text-center">
        <div class="card-body">
            {{ __('Welcome to Marktech Admin Panel') }}
        </div>
    </div>
@endsection
