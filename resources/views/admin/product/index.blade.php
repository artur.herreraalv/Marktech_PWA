@extends('layouts.admin')
@section('title', $viewData['title'])
@section('content')
    <div class="card mb-4">
        <div class="card-header">
            {{ __('Add Product') }}
        </div>
        <div class="card-body">
            @if ($errors->any())
                <ul class="alert alert-danger list-unstyled">
                    @foreach ($errors->all() as $error)
                        <li> - {{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            <form method="POST" action="{{ route('admin.product.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col">
                        <div class="mb-3 row">
                            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">{{ __('Name') }}</label>
                            <div class="col-lg-10 col-md-6 col-sm-12">
                                <input name="name" value="{{ old('name') }}" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="mb-3 row">
                            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">{{ __('Price') }}</label>
                            <div class="col-lg-10 col-md-6 col-sm-12">
                                <input name="price" value="{{ old('price') }}" type="number" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="mb-3 row">
                            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">{{ __('Image') }}</label>
                            <div class="col-lg-10 col-md-6 col-sm-12">
                                <input class="form-control" type="file" name="image">
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        &nbsp;
                    </div>
                </div>
                <div class="mb-3">
                    <label class="form-label">{{ __('Discount Quantity') }}</label>
                    <input name="discounted_price" value="{{ old('discounted_price') }}" type="number"
                        class="form-control">
                </div>
                <div class="mb-3">
                    <label class="form-label">{{ __('Description') }}</label>
                    <textarea class="form-control" name="description" rows="3">{{ old('description') }}</textarea>
                </div>
                <div class="mb-3">
                    <label class="form-label">{{ __('Stock') }}</label>
                    <input name="stock" value="{{ old('stock') }}" type="number" class="form-control">
                </div>
                <h5>{{ __('Brand') }}</h5>
                <select name="trademark" class="form-control">
                    <option value="">{{ __('Select Brand') }}</option>
                    @foreach ($viewData['trademarks'] as $menus)
                        <option value="{{ $menus->trademarks }}">{{ $menus->trademarks }}</option>
                    @endforeach
                </select>
                <br>
                <h5>{{ __('Category') }}</h5>
                <select name="category" class="form-control">
                    <option value="">{{ __('Select Category') }}</option>
                    @foreach ($viewData['categories'] as $menus)
                        <option value="{{ $menus->categories }}">{{ $menus->categories }}</option>
                    @endforeach
                </select>
                <br>
                <h5>{{ __('Sub Category') }}</h5>
                <select name="subcategory" class="form-control">
                    <option value="">{{ __('Select Sub Category') }}</option>
                    @foreach ($viewData['subcategories'] as $menus)
                        <option value="{{ $menus->subcategories }}">{{ $menus->subcategories }}</option>
                    @endforeach
                </select>
                <br>
                <h5>{{ __('Featured?') }}</h5>
                <select class="form-select mb-4" name="featured" value="{{ old('featured') }}"
                    aria-label="Default select example" required>
                    <option value="0" selected>{{ __('No') }}</option>
                    <option value="1">{{ __('Yes') }}</option>
                </select>
                <button type="submit" class="btn btn-black">{{ __('Save') }}</button>
        </div>
        </form>
    </div>

    <div class="card">
        <div class="card-header">
            {{ __('Manage Products') }} </div>
        <div class="card-body">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">{{ __('Name') }}</th>
                        <th scope="col">{{ __('Flag') }}</th>
                        <th scope="col">{{ __('Price') }}</th>
                        <th scope="col">{{ __('Category') }}</th>
                        <th scope="col">{{ __('Sub Category') }}</th>
                        <th scope="col">{{ __('Featured?') }}</th>
                        <th scope="col">{{ __('Edit') }}</th>
                        <th scope="col">{{ __('Delete') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($viewData['products'] as $product)
                        <tr>
                            <td>{{ $product->getId() }}</td>
                            <td>{{ $product->getName() }}</td>
                            <td>{{ $product->getTrademark() }}</td>
                            <td>{{ $product->getPrice() }}</td>
                            <td>{{ $product->getCategory() }}</td>
                            <td>{{ $product->getSubcategory() }}</td>
                            <td>{{ $product->getFeatured() }}</td>
                            <td>
                                <a class="btn btn-black"
                                    href="{{ route('admin.product.edit', ['id' => $product->getId()]) }}">
                                    <i class="bi-pencil"></i>
                                </a>
                            </td>
                            <td>
                                <form action="{{ route('admin.product.delete', $product->getId()) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger">
                                        <i class="bi-trash"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
