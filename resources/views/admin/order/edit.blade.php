@extends('layouts.admin')
@section('title', $viewData['title'])
@section('content')
    <div class="card mb-4">
        <div class="card-header">
            {{ __('Edit Order') }}
        </div>
        <div class="card-body">
            @if ($errors->any())
                <ul class="alert alert-danger list-unstyled">
                    @foreach ($errors->all() as $error)
                        <li>- {{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('admin.order.update', ['id' => $viewData['product']->getId()]) }}"
                enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col">
                        <div class="mb-3 row">
                            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">{{ __('Total') }}</label>
                            <div class="col-lg-10 col-md-6 col-sm-12">
                                <input name="total" value="{{ $viewData['product']->getTotal() }}" type="number"
                                    class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="mb-3 row">
                            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">{{ __('User') }}</label>
                            <div class="col-lg-10 col-md-6 col-sm-12">
                                <input name="user_id" value="{{ $viewData['product']->getUserId() }}" type="number"
                                    class="form-control" required>
                            </div>
                        </div>
                    </div>
                </div>
                <select class="form-select mb-4" name="paid" value="{{ $viewData['product']->getState() }}"
                    aria-label="Default select example" required>
                    <option value="" selected disabled>{{ __('Payment Status') }}</option>
                    <option value="No Pagado">{{ __('Not Paid') }}</option>
                    <option value="Pagado">{{ __('Paid') }}</option>
                </select>
                <div class="mb-3">
                    <label class="form-label">{{ __('Address') }}</label>
                    <input name="address" value="{{ $viewData['product']->getAddress() }}" type="text"
                        class="form-control" required>
                </div>
                <select class="form-select mb-4" name="status" value="{{ $viewData['product']->getEstado() }}"
                    aria-label="Default select example" required>
                    <option value="" selected disabled>{{ __('Shipping Status') }}</option>
                    <option value="Preparando Pedido">{{ __('Preparing Order') }}</option>
                    <option value="Enviado">{{ __('Sent') }}</option>
                    <option value="Entregado">{{ __('Delivered') }}</option>
                </select>

                <select class="form-select mb-4" name="paqueteria" value="{{ $viewData['product']->getPaqueteria() }}"
                    aria-label="Default select example" required>
                    <option value="" selected disabled>{{ __('Courier') }}</option>
                    <option value="DHL">DHL</option>
                    <option value="IMILE">IMILE</option>
                    <option value="Fedex">Fedex</option>
                    <option value="Estafeta">Estafeta</option>
                </select>

                <div class="mb-3">
                    <label class="form-label">{{ __('Tracking Number') }}</label>
                    <input name="tracking_number" value="{{ $viewData['product']->getTrackingNumber() }}" type="number"
                        class="form-control" placeholder="1234567890" required>
                </div>

                <button type="submit" class="btn btn-black">{{ __('Save') }}</button>
            </form>
        </div>
    </div>
@endsection
