@extends('layouts.admin')
@section('title', $viewData['title'])
@section('content')

    <div class="card">
        <div class="card-header">
            {{ __('Payed Orders') }}
        </div>
        <div class="card-body">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">{{ __('Total') }}</th>
                        <th scope="col">{{ __('User') }}</th>
                        <th scope="col">{{ __('Status') }}</th>
                        <th scope="col">{{ __('Address') }}</th>
                        <th scope="col">{{ __('Sent') }}</th>
                        <th scope="col">{{ __('Tracking Number') }}</th>
                        <th scope="col">{{ __('Package') }}</th>
                        <th scope="col">{{ __('Edit') }}</th>
                        {{-- <th scope="col">Eliminar</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @foreach ($viewData['pagados'] as $product)
                        <tr>
                            <td>{{ $product->getId() }}</td>
                            <td>{{ $product->getTotal() }}</td>
                            <td>{{ $product->getUserId() }}</td>
                            <td>{{ $product->getState() }}</td>
                            <td>{{ $product->getAddress() }}</td>
                            <td>{{ $product->getEstado() }}</td>
                            <td>{{ $product->getTrackingNumber() }}</td>
                            <td>{{ $product->getPaqueteria() }}</td>
                            <td>
                                <a class="btn btn-black"
                                    href="{{ route('admin.order.edit', ['id' => $product->getId()]) }}">
                                    <i class="bi-pencil"></i>
                                </a>
                            </td>
                            <td>
                                {{-- <form action="{{ route('admin.order.delete', $product->getId()) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger">
                                        <i class="bi-trash"></i>
                                    </button>
                                </form> --}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <br>
    <div class="card">
        <div class="card-header">

            {{ __('Canceled Orders') }}
        </div>
        <div class="card-body">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">{{ __('Total') }}</th>
                        <th scope="col">{{ __('User') }}</th>
                        <th scope="col">{{ __('Status') }}</th>
                        <th scope="col">{{ __('Address') }}</th>
                        <th scope="col">{{ __('Sent') }}</th>
                        <th scope="col">{{ __('Edit') }}</th>
                        {{-- <th scope="col">Eliminar</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @foreach ($viewData['cancelado'] as $product)
                        <tr>
                            <td>{{ $product->getId() }}</td>
                            <td>{{ $product->getTotal() }}</td>
                            <td>{{ $product->getUserId() }}</td>
                            <td>{{ $product->getState() }}</td>
                            <td>{{ $product->getAddress() }}</td>
                            <td>{{ $product->getEstado() }}</td>
                            <td>
                                <a class="btn btn-black"
                                    href="{{ route('admin.order.edit', ['id' => $product->getId()]) }}">
                                    <i class="bi-pencil"></i>
                                </a>
                            </td>
                            <td>
                                {{-- <form action="{{ route('admin.order.delete', $product->getId()) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger">
                                        <i class="bi-trash"></i>
                                    </button>
                                </form> --}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <br>
    <div class="card">
        <div class="card-header">

                {{ __('Pending Orders') }}
        </div>
        <div class="card-body">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">{{ __('Total') }}</th>
                        <th scope="col">{{ __('User') }}</th>
                        <th scope="col">{{ __('Status') }}</th>
                        <th scope="col">{{ __('Address') }}</th>
                        <th scope="col">{{ __('Sent') }}</th>
                        <th scope="col">{{ __('Edit') }}</th>
                        {{-- <th scope="col">Eliminar</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @foreach ($viewData['pendiente'] as $product)
                        <tr>
                            <td>{{ $product->getId() }}</td>
                            <td>{{ $product->getTotal() }}</td>
                            <td>{{ $product->getUserId() }}</td>
                            <td>{{ $product->getState() }}</td>
                            <td>{{ $product->getAddress() }}</td>
                            <td>{{ $product->getEstado() }}</td>
                            <td>
                                <a class="btn btn-black"
                                    href="{{ route('admin.order.edit', ['id' => $product->getId()]) }}">
                                    <i class="bi-pencil"></i>
                                </a>
                            </td>
                            <td>
                                {{-- <form action="{{ route('admin.order.delete', $product->getId()) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger">
                                        <i class="bi-trash"></i>
                                    </button>
                                </form> --}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <br>
    <div class="card">
        <div class="card-header">

                {{ __('All Orders') }}
        </div>
        <div class="card-body">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">{{ __('Total') }}</th>
                        <th scope="col">{{ __('User') }}</th>
                        <th scope="col">{{ __('Status') }}</th>
                        <th scope="col">{{ __('Address') }}</th>
                        <th scope="col">{{ __('Sent') }}</th>
                        <th scope="col">{{ __('Edit') }}</th>
                        {{-- <th scope="col">Eliminar</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @foreach ($viewData['products'] as $product)
                        <tr>
                            <td>{{ $product->getId() }}</td>
                            <td>{{ $product->getTotal() }}</td>
                            <td>{{ $product->getUserId() }}</td>
                            <td>{{ $product->getState() }}</td>
                            <td>{{ $product->getAddress() }}</td>
                            <td>{{ $product->getEstado() }}</td>
                            <td>
                                <a class="btn btn-black"
                                    href="{{ route('admin.order.edit', ['id' => $product->getId()]) }}">
                                    <i class="bi-pencil"></i>
                                </a>
                            </td>
                            <td>
                                {{-- <form action="{{ route('admin.order.delete', $product->getId()) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger">
                                        <i class="bi-trash"></i>
                                    </button>
                                </form> --}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>


@endsection
