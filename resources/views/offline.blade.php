{{-- when open the app if the user is offline go to public/offline.blade.php --}}
<script>
    window.addEventListener('load', function() {
        if (!navigator.onLine) {
            window.location = '/offline';
        }
    });
</script>

{{-- if the user is online go to public/index.blade.php --}}
<script>
    window.addEventListener('load', function() {
        if (navigator.onLine) {
            window.location = '/';
        }
    });
</script>
