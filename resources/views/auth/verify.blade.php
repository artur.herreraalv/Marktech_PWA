@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">{{ __('Email verification is required') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A new verification link has been sent to your e-mail address.') }}
                        </div>
                    @endif

                    {{ __('To continue you must verify your email, check your inbox for a verification link.') }}
                    {{ __('If you have not received the link') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-black p-0 m-0 align-baseline">{{ __('click here to request another one') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
