<!doctype html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
    <script src="{{ asset('js/dark-mode-switch.min.js') }}" defer></script>
    <link href="{{ asset('css/dark-mode-admin.css') }}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <title>@yield('title', 'Marktech')</title>

</head>

<body>

    <div class="row g-0">
        <!-- sidebar -->
        <div class="p-3 col fixed text-white bg-black hide-mobile">
            <img src="{!! asset('img/mk2otlnwhite.png') !!}">
            <p class="fs-4 text-center">{{ __('Admin Panel') }}</p>
            <hr />
            <div class="form-check form-switch">
                <input type="checkbox" class="form-check-input" id="darkSwitch" />
                <label class="custom-control-label" for="darkSwitch">{{ __('Dark Mode') }}</label>
            </div>
            <hr />
            <ul class="nav flex-column">
                <li><a href="{{ route('admin.product.index') }}"
                        class="nav-link text-white text-center">{{ __('Products') }}</a>
                <li><a href="{{ route('admin.item.index') }}"
                        class="nav-link text-white text-center">{{ __('Items') }}</a>
                <li><a href="{{ route('admin.address.index') }}"
                        class="nav-link text-white text-center">{{ __('Addresses') }}</a>
                <li><a href="{{ route('admin.order.index') }}"
                        class="nav-link text-white text-center">{{ __('Orders') }}</a>
                <li><a href="{{ route('admin.user.index') }}"
                        class="nav-link text-white text-center">{{ __('Users') }}</a>
                <li><a href="{{ route('admin.menus.index') }}"
                        class="nav-link text-white text-center">{{ __('Menus') }}</a>
                </li>
                <li><a href="{{ route('push') }}"
                        class="nav-link text-white text-center">{{ __('Push Notifications') }}</a>
                    <div class="text-center">
                        <a href="{{ route('home.index') }}" class="mt-2 btn btn-dark text-white">
                            {{ __('Return to User') }}
                        </a>
                    </div>
                </li>
            </ul>
        </div>
        <!-- sidebar -->
        <div class="col content-grey">
            <div class="g-0 m-5">
                @yield('content')
            </div>
        </div>
    </div>

    <!-- Bottom fixed navbar bootstrap -->
    <div class="hide-desktop">
        <div class="mobile-nav">
            <nav class="navbar navbar-black bg-black fixed-bottom">
                <div class="container-fluid">
                    <div class="navbar-nav flex-row mx-auto">
                        <a class="nav-link" href="{{ route('admin.product.index') }}"><span class="iconify"
                                data-icon="tabler:home" style="font-size: 32px;"></span>
                            {{ __('Home') }}</a>
                        <a class="nav-link" href="{{ route('admin.item.index') }}"><span class="iconify"
                                data-icon="akar-icons:shipping-box-v1" style="font-size: 32px;"></span>
                            {{ __('Items') }}</a>
                        <a class="nav-link" href="{{ route('admin.address.index') }}"><span class="iconify"
                                data-icon="mdi:home-group" style="font-size: 32px;"></span>
                            {{ __('Addresses') }}</a>
                        <a class="nav-link" href="{{ route('admin.order.index') }}"><span class="iconify"
                                data-icon="icon-park-outline:transaction-order" style="font-size: 32px;"></span>
                            {{ __('Orders') }}</a>
                        <a class="nav-link" href="{{ route('admin.user.index') }}"><span class="iconify"
                                data-icon="icon-park-outline:every-user" style="font-size: 32px;"></span>
                            {{ __('Users') }}</a>
                        <a class="nav-link" href="{{ route('admin.menus.index') }}"><span class="iconify"
                                data-icon="ep:menu"></span>
                            {{ __('Menus') }}</a>
                        <a class="nav-link" href="{{ route('push') }}"><span class="iconify"
                                data-icon="foundation:burst-sale" style="font-size: 32px;"></span>
                            {{ __('Push Notifications') }}</a>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <!-- Bottom fixed navbar bootstrap -->
</body>

</html>
