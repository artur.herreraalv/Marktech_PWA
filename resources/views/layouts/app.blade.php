<!doctype html>
<html lang="en">

<head>
    <!-- Safari -->
    <meta name="apple-mobile-web-app-capable" content="yes">

    <!--  -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- PWA  -->
    <link rel="apple-touch-icon" href="{{ asset('/logo.jpg') }}">
    <link rel="manifest" href="{{ asset('/manifest.json') }}">
    <!-- Bootstrap CSS -->
    <script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>

    <!-- check if the dark mode is enabled -->
    <script>
        if (localStorage.getItem('darkSwitch') !== null) {
            if (localStorage.getItem('darkSwitch') === 'dark') {
                document.documentElement.setAttribute('data-theme', 'dark')
            } else {
                document.documentElement.setAttribute('data-theme', 'light')
            }
        }
        // check if the user has a preference
        var darkQuery = window.matchMedia('(prefers-color-scheme: dark)');
        darkQuery.addListener(function(e) {
            var newTheme = e.matches ? 'dark' : 'light';
            document.documentElement.setAttribute('data-theme', newTheme)
        })
    </script>

    <!-- check if the dark mode is enabled on mobile -->
    <script>
        if (localStorage.getItem('darkSwitchm') !== null) {
            if (localStorage.getItem('darkSwitchm') === 'dark') {
                document.documentElement.setAttribute('data-theme', 'dark')
            } else {
                document.documentElement.setAttribute('data-theme', 'light')
            }
        }
        // check if the user has a preference
        var darkQuery = window.matchMedia('(prefers-color-scheme: dark)');
        darkQuery.addListener(function(e) {
            var newTheme = e.matches ? 'dark' : 'light';
            document.documentElement.setAttribute('data-theme', newTheme)
        })
    </script>

    <script src="{{ asset('js/offline.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('css/offline-language-spanish.css') }}">
    <link rel="stylesheet" href="{{ asset('css/offline-theme-slide.css') }}">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/dark-mode.css') }}" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/favicon.ico') }}">
    <title>@yield('title', 'Marktech')</title>
    <!-- icons ios -->
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3) and (orientation: landscape)"
        href="{{ asset('img/img/splash_screens/iPhone_13_mini__iPhone_12_mini__iPhone_11_Pro__iPhone_XS__iPhone_X_landscape.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 834px) and (device-height: 1194px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)"
        href="{{ asset('img/splash_screens/11__iPad_Pro__10.5__iPad_Pro_landscape.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 1024px) and (device-height: 1366px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)"
        href="{{ asset('img/splash_screens/12.9__iPad_Pro_landscape.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)"
        href="{{ asset('img/splash_screens/iPhone_8__iPhone_7__iPhone_6s__iPhone_6__4.7__iPhone_SE_portrait.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)"
        href="{{ asset('img/splash_screens/iPhone_11__iPhone_XR_portrait.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)"
        href="{{ asset('img/splash_screens/iPhone_11__iPhone_XR_landscape.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 810px) and (device-height: 1080px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)"
        href="{{ asset('img/splash_screens/10.2__iPad_portrait.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3) and (orientation: landscape)"
        href="{{ asset('img/splash_screens/iPhone_11_Pro_Max__iPhone_XS_Max_landscape.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 393px) and (device-height: 852px) and (-webkit-device-pixel-ratio: 3) and (orientation: landscape)"
        href="{{ asset('img/splash_screens/iPhone_14_Pro_landscape.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 430px) and (device-height: 932px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)"
        href="{{ asset('img/splash_screens/iPhone_14_Pro_Max_portrait.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 834px) and (device-height: 1112px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)"
        href="{{ asset('img/splash_screens/10.5__iPad_Air_portrait.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 428px) and (device-height: 926px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)"
        href="{{ asset('img/splash_screens/iPhone_14_Plus__iPhone_13_Pro_Max__iPhone_12_Pro_Max_portrait.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 820px) and (device-height: 1180px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)"
        href="{{ asset('img/splash_screens/10.9__iPad_Air_landscape.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)"
        href="{{ asset('img/splash_screens/iPhone_13_mini__iPhone_12_mini__iPhone_11_Pro__iPhone_XS__iPhone_X_portrait.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 820px) and (device-height: 1180px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)"
        href="{{ asset('img/splash_screens/10.9__iPad_Air_portrait.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 834px) and (device-height: 1194px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)"
        href="{{ asset('img/splash_screens/11__iPad_Pro__10.5__iPad_Pro_portrait.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)"
        href="{{ asset('img/splash_screens/iPhone_8__iPhone_7__iPhone_6s__iPhone_6__4.7__iPhone_SE_landscape.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 430px) and (device-height: 932px) and (-webkit-device-pixel-ratio: 3) and (orientation: landscape)"
        href="{{ asset('img/splash_screens/iPhone_14_Pro_Max_landscape.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 393px) and (device-height: 852px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)"
        href="{{ asset('img/splash_screens/iPhone_14_Pro_portrait.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)"
        href="{{ asset('img/splash_screens/iPhone_11_Pro_Max__iPhone_XS_Max_portrait.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 428px) and (device-height: 926px) and (-webkit-device-pixel-ratio: 3) and (orientation: landscape)"
        href="{{ asset('img/splash_screens/iPhone_14_Plus__iPhone_13_Pro_Max__iPhone_12_Pro_Max_landscape.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)"
        href="{{ asset('img/splash_screens/9.7__iPad_Pro__7.9__iPad_mini__9.7__iPad_Air__9.7__iPad_portrait.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)"
        href="{{ asset('img/splash_screens/4__iPhone_SE__iPod_touch_5th_generation_and_later_landscape.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 390px) and (device-height: 844px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)"
        href="{{ asset('img/splash_screens/iPhone_14__iPhone_13_Pro__iPhone_13__iPhone_12_Pro__iPhone_12_portrait.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 810px) and (device-height: 1080px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)"
        href="{{ asset('img/splash_screens/10.2__iPad_landscape.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)"
        href="{{ asset('img/splash_screens/iPhone_8_Plus__iPhone_7_Plus__iPhone_6s_Plus__iPhone_6_Plus_portrait.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)"
        href="{{ asset('img/splash_screens/9.7__iPad_Pro__7.9__iPad_mini__9.7__iPad_Air__9.7__iPad_landscape.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)"
        href="{{ asset('img/splash_screens/4__iPhone_SE__iPod_touch_5th_generation_and_later_portrait.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 834px) and (device-height: 1112px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)"
        href="{{ asset('img/splash_screens/10.5__iPad_Air_landscape.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 1024px) and (device-height: 1366px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)"
        href="{{ asset('img/splash_screens/12.9__iPad_Pro_portrait.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3) and (orientation: landscape)"
        href="{{ asset('img/splash_screens/iPhone_8_Plus__iPhone_7_Plus__iPhone_6s_Plus__iPhone_6_Plus_landscape.png') }}">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 390px) and (device-height: 844px) and (-webkit-device-pixel-ratio: 3) and (orientation: landscape)"
        href="{{ asset('img/splash_screens/iPhone_14__iPhone_13_Pro__iPhone_13__iPhone_12_Pro__iPhone_12_landscape.png') }}">
    <!-- icons ios -->

    <!-- Chrome, Firefox OS and Opera -->
    <script>
        if (localStorage.getItem('darkSwitchm') !== null) {
            if (localStorage.getItem('theme') === 'dark') {
                document.write('<meta name="theme-color" content="#000000">');
            } else {
                document.write('<meta name="theme-color" content="#ffffff">');
            }
        }
    </script>
    <meta name="apple-mobile-web-app-status-bar-style" content="default">

    <style>
        body {
            background-color: rgb(255, 255, 255)
        }
    </style>

    <div class="hide-desktop hide-mobile">
        <script>
            var run = function() {
                if (Offline.state === 'up')
                    Offline.check();
            }
            setInterval(run, 5000);
        </script>
    </div>
</head>

<body>
    <!-- header -->
    @php
        //Device specific headers
        $iPod = stripos($_SERVER['HTTP_USER_AGENT'], 'iPod');
        $iPhone = stripos($_SERVER['HTTP_USER_AGENT'], 'iPhone');
        $iPad = stripos($_SERVER['HTTP_USER_AGENT'], 'iPad');
        $Android = stripos($_SERVER['HTTP_USER_AGENT'], 'Android');
        $webOS = stripos($_SERVER['HTTP_USER_AGENT'], 'webOS');

        if ($iPhone || $iPod || $iPad) {
            echo '<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />';
        } else {
            echo '<meta name="viewport" content="width=device-width, initial-scale=1.0" />';
        }
    @endphp



    {{-- <div id="div1"></div> --}}



    <div class="navbar1">
        <div class="hide-mobile">
            <div class="container-sm">
                <nav class="navbar navbar-expand-lg navbar-light bg-white">
                    <a class="mx-auto" href="/">
                        <div class="dark-hide"><img src="{!! asset('img/mk2otln.png') !!}" class="logo-mk"></div>
                        <div class="light-hide"><img src="{!! asset('img/mk2otlnwhite.png') !!}" class="logo-mk"></div>
                    </a>
                    <div class="container">
                        <a class="navbar-brand" href="{{ route('home.index') }}"></a>
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="mx-auto">
                            <div class="input-group">
                                <form action="/busqueda?barra={{ $barra }}" method="GET">
                                    <input type="text" class="form-control mx-auto search-desktop" name="barra"
                                        placeholder="{{ __('Search products...') }}" id="barra"
                                        style="width: 300px; height: 50px"><br>
                                    {{-- get the value of search from the current url after barra= --}}
                                    <script>
                                        $(document).ready(function() {
                                            var url = window.location.href;
                                            // get the value of search from the current url after barra= and before &
                                            var barra = url.split('barra=')[1] ? url.split('barra=')[1].split('&')[0] : '';
                                            if (barra) {
                                                $('input[name="barra"]').val(barra);
                                            }
                                        });
                                    </script>


                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-default">
                                        </button>
                                    </span>
                                </form>
                            </div>

                        </div>

                        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                            <div class="navbar-nav ms-auto">

                                @guest
                                    <a class="nav-link" href="{{ route('cart.index') }}" id="cart"><span
                                            class="iconify" data-icon="eva:shopping-cart-outline" data-width="24"></span>
                                        {{ __('Cart') }}</a>
                                    <a class="nav-link" href="/IniciarSesion" id="login">{{ __('Log in') }}</a>
                                    <a class="nav-link" href="/Registro" id="register">{{ __('Sign up') }}</a>
                                @else
                                    @if (Auth::user()->role == 'admin')
                                        <a class="nav-link fs-5" id="adminmode"
                                            href="{{ route('admin.product.index', ['id' => Auth::user()->id]) }}">{{ __('Admin Mode') }}</a>
                                    @endif

                                    <a class="nav-link position-relative" href="{{ route('cart.index') }} "
                                        id="cart"><span class="iconify" data-icon="eva:shopping-cart-outline"
                                            data-width="24"></span>
                                        {{ __('Cart') }}
                                        {{-- <span
                                            class="badge bg-danger rounded-pill position-absolute top-0 start-100 translate-middle"
                                            style="z-index: 1; font-size: 10px">
                                            {{ $viewData['products'] }} --}}
                                    </a>

                                    <a class="nav-link" href="{{ route('myaccount.orders') }}"
                                        id="myorders">{{ __('My Orders') }}</a>
                                    <a class="nav-link" href="/micuenta" id="myaccount">{{ __('My Account') }}</a>
                                    <form id="logout" action="{{ route('logout') }}" method="POST">
                                        <a role="button" class="nav-link" id="logout"
                                            onclick="document.getElementById('logout').submit();">{{ __('Log Out') }}</a>
                                        @csrf
                                    </form>

                                @endguest


                            </div>

                            {{-- // a drowpdown menu for the languages --}}
                            <div class="dropdown">
                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    {{ __('Language') }}
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li></li>
                                    <li><a class="dropdown-item" href="{{ url('change-language/en') }}"><span
                                                class="iconify" data-icon="emojione:flag-for-united-states"></span>
                                            {{ __('English') }}</a>
                                    </li>
                                    <li><a class="dropdown-item" href="{{ url('change-language/es') }}"><span
                                                class="iconify" data-icon="emojione:flag-for-mexico"></span>
                                            {{ __('Spanish') }}</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="form-check form-switch">
                                <input type="checkbox" class="form-check-input" id="darkSwitch" />

                                <label class="custom-control-label" for="darkSwitch"
                                    id="darkmode">{{ __('Dark Mode') }}</label>
                            </div>

                            {{-- <div class="nav-link">
                                <a href="{{ url('change-language/en') }}">{{ __('English') }}</a>
                                <a href="{{ url('change-language/es') }}">{{ __('Spanish') }}</a>
                            </div> --}}



                        </div>


                    </div>

                </nav>



                <!--Navbar-->
                </nav>
                <div class="hide-mobile">
                    <br>
                </div>
                <div class="navbar-black">
                    <nav class="navbar navbar-expand-lg navbar-dark bg-black rounded">

                        <!--Dropdown-->
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0 ">
                            <div class="collapse navbar-collapse " id="navbarSupportedContent">
                                <li class="nav-item dropdown s">
                                    <a class="btn btn-link btn-lg" href="/hardware" id="navbarDropdown"
                                        role="button" aria-haspopup="true" aria-expanded="false"
                                        id="hardware">{{ __('Hardware') }}<span class="iconify"
                                            data-icon="bx:down-arrow"></span></a>
                                    {{-- <a class="btn btn-link btn-lg dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    </a> --}}
                                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <ul>
                                            <li><a class="dropdown-item" href="/armatucomputadora"
                                                    id="buildyourpc">{{ __('Build your PC') }}</a></li>
                                            <li><a class="dropdown-item" href="/hardware/procesadores"
                                                    id="cpus">{{ __('CPUs') }}</a></li>
                                </li>
                                <li><a class="dropdown-item" href="/hardware\motherboards"
                                        id="motherboards">{{ __('Motherboards') }}</a></li>
                                </li>
                                <li><a class="dropdown-item" href="/hardware\gabinetes"
                                        id="case">{{ __('Cases') }}</a>
                                <li><a class="dropdown-item" href="/hardware\graficas"
                                        id="gpu">{{ __('GPUs') }}</a>
                                </li>
                                <li><a class="dropdown-item" href="/hardware\ram"
                                        id="ram">{{ __('RAM') }}</a></li>
                                <li><a class="dropdown-item" href="/hardware\disipadores"
                                        id="coolers">{{ __('Coolers') }}</a>
                                </li>
                                </li>
                                <li><a class="dropdown-item" href="/hardware\fuentes"
                                        id="psu">{{ __('PSUs') }}</a></li>
                                </li>
                        </ul>
                        <ul>
                            <li><a class="dropdown-item" href="/almacenamiento"
                                    id="storage">{{ __('Storage') }}</a></li>
                            </li>
                            <li><a class="dropdown-item" href="/hardware\ssd" id="ssd">{{ __('SSDs') }}</a>
                            </li>
                            </li>
                            <li><a class="dropdown-item" href="/hardware\hdd" id="hdd">{{ __('HDDs') }}</a>
                            </li>
                            <li><a class="dropdown-item" href="/hardware\ram" id="ram2">{{ __('RAM') }}</a>
                            </li>
                            <li><a class="dropdown-item" href="/hardware\usb"
                                    id="usbsd">{{ __('USBs & SDs') }}</a></li>
                        </ul>
                        </ul>
                        </li>
                        </ul>
                        <!--Dropdown-->
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item dropdown">
                                <a class="btn btn-link btn-lg" href="/accesorios" id="navbarDropdown" role="button"
                                    aria-haspopup="true" aria-expanded="false"
                                    id="accessories">{{ __('Accessories') }}
                                    <span class="iconify" data-icon="bx:down-arrow"></span></a>
                                {{-- <a class="btn btn-link btn-lg dropdown-toggle" href="/todo" id="navbarDropdown" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">

                    </a> --}}
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <ul>
                                        <li><a class="dropdown-item" href="/accesrios"
                                                id="accessories2">{{ __('Accessories') }}</a></li>
                                        <li><a class="dropdown-item" href="/accesorios\audifonos"
                                                id="headphones">{{ __('Headphones') }}</a></li>
                                        <li><a class="dropdown-item" href="/accesorios\alfombrillas"
                                                id="mousepads">{{ __('Mousepads') }}</a></li>
                            </li>
                            <li><a class="dropdown-item" href="/accesorios\mouse"
                                    id="mouse">{{ __('Mice') }}</a></li>
                            <li><a class="dropdown-item" href="/accesorios\teclados"
                                    id="keyboards">{{ __('Keyboards') }}</a>
                            </li>
                        </ul>
                        </ul>
                        </ul>
                        </li>
                        </ul>
                        <!--Dropdown-->
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item dropdown">
                                <a class="btn btn-link btn-lg" href="/computadoras" id="navbarDropdown"
                                    role="button" aria-haspopup="true" aria-expanded="false"
                                    id="pcs">{{ __('PCs') }}
                                    <span class="iconify" data-icon="bx:down-arrow"></span></a>
                                {{-- <a class="btn btn-link btn-lg dropdown-toggle" href="/todo" id="navbarDropdown" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">

                    </a> --}}
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <ul>
                                        <li><a class="dropdown-item" href="/computadras"
                                                id="pcs2">{{ __('PCs') }}</a></li>
                                        <li><a class="dropdown-item" href="/computadoras\laptop"
                                                id="laptop">{{ __('Laptops') }}</a></li>
                                        <li><a class="dropdown-item" href="/computadoras\escritorio"
                                                id="desktop">{{ __('Desktops') }}</a></li>
                            </li>
                        </ul>
                        </ul>
                        </ul>
                        </li>
                        </ul>
                        </ul>
                        </ul>
                        </li>
                        </ul>

                        <!--Dropdown-->
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item dropdown">
                                <a class="btn btn-link btn-lg" href="/electronica" id="navbarDropdown"
                                    role="button" aria-haspopup="true" aria-expanded="false"
                                    id="electronics">{{ __('Electronics') }}
                                    <span class="iconify" data-icon="bx:down-arrow"></span></a>
                                {{-- <a class="btn btn-link btn-lg dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">

                    </a> --}}
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <ul>
                                        <li><a class="dropdown-item" href="/electrnica"
                                                id="electronics2">{{ __('Electronics') }}</a></li>
                                        <li><a class="dropdown-item" href="/electronica\consolas"
                                                id="consoles">{{ __('Consoles') }}</a></li>
                                        <li><a class="dropdown-item" href="/electronica\tv" id="tv">TV</a>
                                        </li>
                                        <li><a class="dropdown-item" href="/electronica\monitores"
                                                id="monitors">{{ __('Monitors') }}</a></li>
                                        <li><a class="dropdown-item" href="/electronica\bocinas"
                                                id="speakers">{{ __('Speakers') }}</a></li>
                                        <li><a class="dropdown-item" href="/electronica\camaras"
                                                id="cameras">{{ __('Cameras') }}</a></li>
                                        <li><a class="dropdown-item" href="/electronica\telefonos"
                                                id="smartphones">{{ __('Smartphones') }}</a></li>
                                    </ul>
                                </ul>
                        </ul>
                        </li>
                        </ul>
                </div>
                </nav>
            </div>
        </div>

        <div class="hide-desktop">
            <nav class="navbar navbar-expand-lg navbar-light bg-white">
                <a class="mx-auto" href="/" onclick="window.location.reload();">
                    <div class="dark-hide"><img src="{!! asset('img/mk2otln.png') !!}" class="logo-mk"></div>
                    <div class="light-hide"><img src="{!! asset('img/mk2otlnwhite.png') !!}" class="logo-mk"></div>
                </a>

                <script>
                    $(document).scroll(function() {
                        var y = $(this).scrollTop();
                        if (y > 100) {
                            $('.scrollbtn').fadeIn();
                        } else {
                            $('.scrollbtn').fadeOut();
                        }
                    });

                    // when the user clicks on the button, scroll to the top of the document
                    function scrollToTop() {
                        document.body.scrollTop = 0;
                        document.documentElement.scrollTop = 0;
                    }
                </script>
                {{-- scroll to top button --}}

                {{-- show the scrollbtn if the user scrolls down --}}

                <div class="position-relative">
                    <a onclick="scrollToTop()" class="btn scrollbtn fixed-bottom"
                        style="margin-bottom: 100px; margin-left: 85%;">
                        <img src="{!! asset('img/UI/up-arrow-circular-button.png') !!}" width="36" height="36">
                    </a>
                </div>

                <div class="container">
                    <a class="navbar-brand" href="{{ route('home.index') }}"></a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class=" mx-auto">
                        <div class="input-group">
                            <form action="/busqueda?barra={{ $barra }}" method="GET">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="barra"
                                        placeholder="{{ __('Search products...') }}"
                                        style="width: 300px; height: 50px">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-default">
                                        </button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </nav>
            <!--Navbar-->
        </div>


        <!-- Bottom fixed navbar bootstrap -->
        <div class="hide-desktop dark-hide">
            <div class="mobile-nav">
                <nav class="navbar navbar-light bg-light fixed-bottom">
                    <div class="container-fluid">
                        <div class="navbar-nav flex-row mx-auto">
                            <a class="nav-link {{ request()->is('/') ? 'active' : '' }}" href="/"
                                id="home"><img src="{!! asset('img/UI/home-button.png') !!}" width="36"
                                    height="36">{{ __('Home2') }}</a>
                            <a class="nav-link {{ request()->is('categoriesm') ? 'active' : '' }}"
                                href="/categoriesm" id="categories"><img src="{!! asset('img/UI/menu.png') !!}"
                                    width="36" height="36">{{ __('Categories') }}</a>

                            <!-- account or login -->
                            @if (!Auth::guest())
                                <a class="nav-link {{ request()->is('micuenta') ? 'active' : '' }}" href="/micuenta"
                                    id="i"><img src="{!! asset('img/UI/user.png') !!}" width="36"
                                        height="36">{{ __('Account') }}</a>
                            @else
                                <a class="nav-link {{ request()->is('IniciarSesion') ? 'active' : '' }}"
                                    href="/IniciarSesion" id="i"><img src="{!! asset('img/UI/user.png') !!}"
                                        width="36" height="36">{{ __('Account') }}</a>
                            @endif
                            <!--account or login/-->

                            <a class="nav-link {{ request()->is('TuCarrito') ? 'active' : '' }}" href="/TuCarrito"
                                id="cartm"><img src="{!! asset('img/UI/shopping-cart.png') !!}" width="36"
                                    height="36">{{ __('Cart') }}</a>
                            <a class="nav-link {{ request()->is('settingsm') ? 'active' : '' }}" href="/settingsm"
                                id="settings"><img src="{!! asset('img/UI/settings.png') !!}" width="36"
                                    height="36">{{ __('Settings') }}</a>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <!-- Bottom fixed navbar bootstrap -->

        <!-- Bottom fixed navbar bootstrap -->
        <div class="hide-desktop light-hide">
            <div class="mobile-nav">
                <nav class="navbar navbar-light bg-light fixed-bottom">
                    <div class="container-fluid">
                        <div class="navbar-nav flex-row mx-auto">
                            <a class="nav-link {{ request()->is('/') ? 'active' : '' }}" href="/"
                                id="home"><img src="{!! asset('img/UI/home-buttonl.png') !!}" width="36"
                                    height="36">{{ __('Home2') }}</a>
                            <a class="nav-link {{ request()->is('categoriesm') ? 'active' : '' }}"
                                href="/categoriesm" id="categories"><img src="{!! asset('img/UI/menul.png') !!}"
                                    width="36" height="36">{{ __('Categories') }}</a>

                            <!-- account or login -->
                            @if (!Auth::guest())
                                <a class="nav-link {{ request()->is('micuenta') ? 'active' : '' }}" href="/micuenta"
                                    id="i"><img src="{!! asset('img/UI/userl.png') !!}" width="36"
                                        height="36">{{ __('Account') }}</a>
                            @else
                                <a class="nav-link {{ request()->is('IniciarSesion') ? 'active' : '' }}"
                                    href="/IniciarSesion" id="i"><img src="{!! asset('img/UI/userl.png') !!}"
                                        width="36" height="36">{{ __('Account') }}</a>
                            @endif
                            <!--account or login/-->

                            <a class="nav-link {{ request()->is('TuCarrito') ? 'active' : '' }}" href="/TuCarrito"
                                id="cartm"><img src="{!! asset('img/UI/shopping-cartl.png') !!}" width="36"
                                    height="36">{{ __('Cart') }}</a>
                            <a class="nav-link {{ request()->is('settingsm') ? 'active' : '' }}" href="/settingsm"
                                id="settings"><img src="{!! asset('img/UI/settingsl.png') !!}" width="36"
                                    height="36">{{ __('Settings') }}</a>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <!-- Bottom fixed navbar bootstrap -->

        {{-- Notifications/ --}}
        @php
            //Device specific headers
            $iPod = stripos($_SERVER['HTTP_USER_AGENT'], 'iPod');
            $iPhone = stripos($_SERVER['HTTP_USER_AGENT'], 'iPhone');
            $iPad = stripos($_SERVER['HTTP_USER_AGENT'], 'iPad');
            $Android = stripos($_SERVER['HTTP_USER_AGENT'], 'Android');
            $webOS = stripos($_SERVER['HTTP_USER_AGENT'], 'webOS');

            if ($iPhone || $iPod || $iPad) {
                // apply css for iphone
                echo '<link rel="stylesheet" href="/css/ios.css">';
            } else {
            }
        @endphp
        {{-- Notifications/ --}}

        {{-- // set blue color for active link if the button is selected --}}
        <script>
            $(document).ready(function() {
                $('.nav-link').click(function() {
                    $('.nav-link').removeClass("active");
                    $(this).addClass("active");
                });
            });
        </script>

        {{-- if the url is different from /, categoriesm, IniciarSesion, TuCarrito, settingsm show the button --}}
        @if (url()->current() != url('/'))
            @if (url()->current() != url('/categoriesm'))
                @if (url()->current() != url('/IniciarSesion'))
                    @if (url()->current() != url('/TuCarrito'))
                        @if (url()->current() != url('/settingsm'))
                            @if (url()->current() != url('/micuenta'))
                                {{-- back button --}}
                                <div class="back-button1">
                                    {{-- // go to the previous page and refresh it --}}
                                    <a href="javascript:history.back()" style="color: #007fff;" id="back">
                                        <img src="{!! asset('img/UI/left.png') !!}" class="logo-mk"
                                            style="width: 42px; height: 42px;">
                                        <strong style="margin-left: -12px;"
                                            class="back-btn">{{ __('Back') }}</strong>
                                    </a>
                                </div>
                            @endif
                        @endif
                    @endif
                @endif
            @endif
        @endif



        <!--content-->
        <div class="container my-4" id="pjax-container">
            @yield('content')
            <br>
            <div class="container-sm">
                <div class="p-3 borderless bg-transparent text-center fs-4"><strong><span class="iconify"
                            data-icon="codicon:blank" style="font-size: 32px;"></span></strong></div>
            </div>
        </div>



        <!--chatbot-->

        {{-- // if lang is spanish show botman --}}
        @if (app()->getLocale() == 'es')
            <script>
                var botmanWidget = {
                    frameEndpoint: '/chatbot',
                    title: 'MarkBot',
                    mainColor: '#000',
                    headerTextColor: '#ffff',
                    bubbleBackground: '#408591',
                    introMessage: 'Hola bienvenido a Marktech soy MarkBot, ¿en qué puedo ayudarte?🤖',
                    placeholderText: 'Escribe aquí...',
                    aboutText: '',
                    aboutLink: '',
                    bubbleAvatarUrl: src = '{!! asset('img/chat.jpg') !!}'
                };
            </script>
        @else
            <script>
                var botmanWidget = {
                    frameEndpoint: '/chatbot',
                    title: 'MarkBot',
                    mainColor: '#000',
                    headerTextColor: '#ffff',
                    bubbleBackground: '#408591',
                    introMessage: 'Hello welcome to Marktech Im MarkBot and Im here to help you.🤖​',
                    placeholderText: 'Type your message here...',
                    aboutText: '',
                    aboutLink: '',
                    bubbleAvatarUrl: src = '{!! asset('img/chat.jpg') !!}'
                };
            </script>
        @endif
        <script src={{ asset('js/widget.js') }}></script>

        <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <script src={{ asset('js/dark-mode-switch.js') }}></script>
        <script src={{ asset('js/dark-mode-switchM.js') }}></script>

        <div class="hide-mobile">
            <div class="container-sm">
                <section>
                    <!--footer-->
                    <footer class="mt-auto bg-black text-center text-white">

                        <div class="container p-4">

                            <!--Links-->
                            <section class="">

                                <div class="row">

                                    <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                                        <h5 class="text-uppercase fs-4" id="contact">
                                            <strong>{{ __('Contact') }}</strong>
                                        </h5>
                                        <br>
                                        <ul class="list-unstyled mb-0">
                                            <li>
                                                <a href="mailto:marktechof@gmail.com" class="text-white"
                                                    id="contactme">{{ __('Contact us') }}</a>
                                            </li>
                                            <li>
                                                <a href="/Sugerencias" class="text-white"
                                                    id="suggestions">{{ __('Suggestions') }}</a>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                                        <h5 class="text-uppercase fs-4" id="social">
                                            <strong>{{ __('Social') }}</strong>
                                        </h5>
                                        <br>
                                        <ul class="list-unstyled mb-0">
                                            <li>
                                                <a href="https://www.facebook.com/Mark-Tech-100458546063140"><i
                                                        class="icon iconify text-white"
                                                        data-icon="ion-social-facebook" data-width="24"></i></a>
                                            </li>
                                            <li>
                                                <a href="https://twitter.com/MarktechOficial"><i
                                                        class="icon iconify text-white" data-icon="ion-social-twitter"
                                                        data-width="24"></i></a>
                                            </li>
                                            <li>
                                                <a href="https://instagram.com/marktech2022"><i
                                                        class="icon iconify text-white"
                                                        data-icon="ion-social-instagram" data-width="24"></i></a>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                                        <h5 class="text-uppercase fs-4" id="marktech">
                                            <strong>{{ __('Marktech') }}</strong>
                                        </h5>
                                        <br>
                                        <ul class="list-unstyled mb-0">
                                            <li>
                                                <a href="/avisodeprivacidad" class="text-white"
                                                    id="privacypolicy">{{ __('Privacy policy') }}</a>
                                            </li>
                                            <li>
                                                <a href="/terminosycondiciones" class="text-white"
                                                    id="terms&conditions">{{ __('Terms & Conditions') }}</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                                        <img src="{{ asset('img/paypalcheckout.png') }}" alt="logo"
                                            class="img-fluid">
                                        <img src="{{ asset('img/stripe1.png') }}" alt="logo" class="img-fluid">
                                    </div>



                                </div>

                            </section>
                        </div>

                        <!--Derechos-->
                        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);" id="legal">
                            {{ __('All rights reserved.') }}
                            <a class="text-white" href="https://marktech.ml/"> https://marktech.ml/</a>
                        </div>
                    </footer>
                </section>
                <!--footer-->
            </div>
        </div>
        <script src="{{ asset('/sw.js') }}"></script>
        <script>
            if (!navigator.serviceWorker.controller) {
                navigator.serviceWorker.register("/sw.js").then(function(reg) {
                    console.log("Service worker has been registered for scope: " + reg.scope);
                });
            }
        </script>
        {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.pjax/2.0.1/jquery.pjax.min.js"></script>
        <script>
            $(document).pjax('a', '#pjax-container', {
                timeout: 10000,
                fragment: '#pjax-container',
            });
        </script> --}}

</body>


</html>
