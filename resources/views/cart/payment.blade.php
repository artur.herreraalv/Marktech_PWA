@extends('layouts.app')
@section('content')



@section('title', 'Marktech - Dirección')

<h5 class="text-center"><strong>{{ __('Products') }}</strong></h5>

<form action="/datos" method="POST">
    @csrf
    <div class="card">
        <div class="card-body">
            <div class="hide-mobile">
                <table class="table table-borderless table-striped text-center">
                    <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">{{ __('Name') }}</th>
                            <th scope="col">{{ __('Product Code') }}</th>
                            <th scope="col">{{ __('Price') }}</th>
                            <th scope="col">{{ __('Discount') }}</th>
                            <th scope="col">{{ __('Amount') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($viewData['products'] as $product)
                            <tr>
                                <td>
                                    <img src="{{ asset('/img/products/' . $product->image) }}"
                                        alt="{{ $product->getName() }}" class="img-fluid" width="100">
                                </td>
                                <td>{{ $product->getName() }}</td>
                                <td>{{ $product->getId() }}</td>
                                <td>
                                    <x-money amount="{{ $product->getPrice() - $product->getDiscountedprice() }}"
                                        currency="MXN" convert />
                                </td>
                                @if ([$product->getDiscountedprice()] > 0)
                                    <td class="text-decoration-line-through">-
                                        <x-money amount="{{ $product->getDiscountedprice() }}" currency="MXN" convert />
                                    </td>
                                @else
                                    <td></td>
                                @endif
                                <td>{{ session('products')[$product->getId()] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="hide-desktop">
                <table class="table text-center">
                    <thead>
                        {{-- <tr>
                        <th scope="col"></th>
                        <th scope="col">Name</th>
                        <th scope="col">Product Code</th>
                        <th scope="col">Price</th>
                        <th scope="col">Discount</th>
                        <th scope="col">Amount</th>
                    </tr> --}}
                    </thead>
                    <tbody>
                        @foreach ($viewData['products'] as $product)
                            {{-- <tr>
                            <td>
                                <img src="{{ asset('/img/products/' . $product->image) }}" alt="{{ $product->getName() }}"
                                    class="img-fluid" width="100">
                            </td>
                            <td>{{ $product->getName() }}</td>
                            <td>{{ $product->getId() }}</td>
                            <td>
                                <x-money amount="{{ $product->getPrice() - $product->getDiscountedprice() }}" currency="MXN"
                                    convert />
                            </td>
                            @if ([$product->getDiscountedprice()] > 0)
                                <td class="text-decoration-line-through">-
                                    <x-money amount="{{ $product->getDiscountedprice() }}" currency="MXN" convert />
                                </td>
                            @else
                                <td></td>
                            @endif
                            <td>{{ session('products')[$product->getId()] }}</td>
                        </tr> --}}
                            <tr>
                                <th scope="row"></th>
                                <td>
                                    <img src="{{ asset('/img/products/' . $product->image) }}"
                                        alt="{{ $product->getName() }}" class="img-fluid" width="100">
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">{{ __('Name') }}</th>
                                <td>{{ $product->getName() }}</td>
                            </tr>
                            <tr>
                                <th scope="row">{{ __('Product Code') }}</th>
                                <td>{{ $product->getId() }}</td>
                            </tr>
                            <tr>

<th scope="row">Price</th>
                                <td>
                                    <x-money amount="{{ $product->getPrice() - $product->getDiscountedprice() }}"
                                        currency="MXN" convert />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">{{ __('Discount') }}</th>
                                @if ([$product->getDiscountedprice()] > 0)
                                    <td class="text-decoration-line-through">-
                                        <x-money amount="{{ $product->getDiscountedprice() }}" currency="MXN"
                                            convert />
                                    </td>
                                @else
                                    <td></td>
                                @endif
                            </tr>
                            <tr>
                                <th scope="row">{{ __('Amount') }}</th>
                                <td>{{ session('products')[$product->getId()] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="text-end">

                </div>
            </div>
        </div>
    </div>

    <br>

    <select class="form-select mb-4" name="address" aria-label="Default select example" required>
        <option value="" selected disabled>{{ __('Select an address') }}</option>
        @foreach ($addresses as $dir)
            <option
                value="{{ $dir->calle }} {{ $dir->exterior }}, {{ $dir->colonia }}, {{ $dir->municipio }}, {{ $dir->estado }}">
                {{ $dir->calle }} {{ $dir->exterior }}, {{ $dir->colonia }}, {{ $dir->municipio }},
                {{ $dir->estado }}</option>
        @endforeach
    </select>
    <a href="/direcciones" class="btn btn-black mb-4">{{ __('Add address') }}</a>


    <div class="col-md-4 mb-4">
        <div class="card mb-4">
            <div class="card-header py-3">
                <h5 class="mb-0"><strong>{{ __('Order Summary') }}</strong></h5>
            </div>
            <div class="card-body cpayment">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
                        {{ __('Products') }}
                        <span>
                            <x-money amount="{{ $viewData['total'] }}" currency="MXN" convert />
                        </span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center px-0">
                        {{ __('Shipping') }}
                        <span>{{ __('Free') }}</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 mb-3">
                        <div>
                            <strong>{{ __('Total') }}</strong>

                            <p class="mb-0">({{ __('including VAT') }})</p>

                        </div>
                        <span><strong>
                                <x-money amount="{{ $viewData['total'] }}" currency="MXN" convert />
                            </strong></span>
                    </li>
                </ul>

                @if (count($viewData['products']) > 0)
                    <button type="submit" button class="btn btn-black mb-2">{{ __('Confirm order') }}</button>
                @endif

</form>

</div>
</div>
</div>

@endsection
