@extends('layouts.app')

@section('content')

@section('title', 'Marktech - Aviso de privacidad')
    {{-- test --}}
    @if (App::getLocale() == 'en')
    <div style="text-align: center;">
        <h1 class="titles">Privacy Policy</h1>
    </div>
        <div>
<p class="p">This Privacy Policy describes our policies regarding
the collection, use and disclosure of your information in connection with your
use of our web pages and emails.
The terms "we" and "our", when
used in this policy, will be understood as referring to our
parent company Marktech.com Group Inc. and the members of our
Marktech.com group, our controlled companies and subsidiaries. To the
purposes of data protection legislation, we are a parent of
 data of the information related to you with which we have by reason of
our Site.</p>

<p class="p">By visiting this site or by interacting in any other
form with Marktech.com, you consent to the practices described in this
Privacy Policy.</p>
<p class="p">When the term “personal data” is used in this Privacy Policy
personal”, it will be understood as such any Data referred to a
identifiable individual, including their name, address, identifier
online, payment information or email box. The data
The specific data we collect are described in this policy.</p>
<p class="p">We encourage you to read this entire Privacy Policy, which contains the following sections:</p>
<ul>
    <a id="index"></a><li><a onclick="Visibility.scrollAncla('#information-we-collect', 500);">Information we collect</a></li>
    <li><a onclick="Visibility.scrollAncla('#how-we-use-your-personal-data', 700);">How we use your Personal Data</a></li>
    <li><a onclick="Visibility.scrollAncla('#with-who-we-share-your-personal-data', 1100);">With whom do we share your Personal Data</a></li>
    <li><a onclick="Visibility.scrollAncla('#where-we-transfer-your-personal-data', 1300);">Where do we transfer your Personal Data</a></li>
    <li><a onclick="Visibility.scrollAncla('#how-we-protect-your-personal-data', 1500);">How we protect your Personal Data</a></li>
    <li><a onclick="Visibility.scrollAncla('#your-rights-in-relation-to-your-personal-data', 1700);">Your Rights in relation to your Personal Data</a></ li>
    <li><a onclick="Visibility.scrollAncla('#changes-to-this-policy', 2500);">Changes to this Policy</a></li>
    <li><a onclick="Visibility.scrollAnchor('#contact', 2700);">Contact</a></li>
    </ul>
    <h2>Information We Collect</h2>
    <p class="p"><strong>Contact information.</strong> We collect and store
    personal contact information that you voluntarily provide to us
    when subscribing to our products or services or to any of our
    our mailing lists. This includes your name, mailing address,
    web address, telephone number, fax number and mailing address
    electronic. Additionally, we collect contact information that
    has been provided to us during any exchange of correspondence
    related to customer service, as well as that related to
    the products and services offered on our Sites.</p>
    {{-- <p class="p"><strong>Financial information.</strong> We collect and store
    financial information you provide to us when you order our
    products or services. For example, when you request the registration of
    a domain name or request any other product or service from
    through Web.com, we ask that you provide a credit card number
    credit and billing address before processing the request.</p> --}}
    <p class="p"><strong>Information about your use of the Site.</strong> We collect
    information about the device with which you access the Sites and
    information relating to your use of the Site itself, such as
    domain name, Internet service provider, type and version of the
     browser, pages visited, information accessed, and the
    Internet Protocol (IP) you used to connect to the Internet.
    We also collect information related to cookies – for further
    information, please review our Cookies Policy.</p>
    <p class="p"><strong>Demographic information.</strong> We collect and store
    demographic information (your zip code and age). Also
    We may collect and store demographic information (such as
    example your zip code and age), and use this data to
    to tailor your experience on our Websites.</p>
    {{-- <p class="p"><strong>Telephone recordings and monitoring.</strong> To ensure
    that Web.com customers receive quality service, Web.com
    select phone calls to be recorded and/or monitored.
    These calls, between Web.com customers (or potential customers) and
     employees, are evaluated by supervisors and/or other representatives of
    Web.com. This is to ensure that a professional
    fast and adequate assistance and accurate information.</p> --}}
    <p class="p"><strong>To improve our services.</strong>
        <p class="p">In order to contract with you, we use this information to contract with you and fulfill your requests, such as:</p>
        <ul>
        <li>Send you the service information that is required</li>
        <li>Fulfill your requests</li>
        <li>Respond to customer requests for services, questions and inquiries</li>
        <li>Manage your account</li>
        </ul>
        <p class="p">Note that, generally, you will not be able to request to be excluded from
        these communications, which are transactional in nature and not
        promotional. If you do not wish to receive them, you have the alternative of
        cancel our services.</p>
        <p class="p">We also contract with other companies, individuals, and subsidiaries
        of Marktech.com to perform certain functions and services for you.
         These third parties:</p>
        <ul>
        <li>process credit card payments</li>
        </ul>
        <p class="p"><strong>To contact you.</strong> If you have provided your
        consent to receive emails, or marketing via
        telephone or SMS, or if you have provided us with your postal information,
        we may use the personal data provided to contact you
        occasionally. If you register on the Site, you may choose to
        provide your consent to receive marketing communications,
        communications by e-mail, landline and/or cell phone (including the use
        of automated dialing equipment and/or pre-recorded calls),
        text message (SMS), social networks and any other form of
        communication that your device is capable of receiving (e.g. video,
        {{-- etc.). If you do not wish to receive postal or electronic communications,
        You can withdraw your consent at any time by clicking
        the unsubscribe link at the bottom of the communication, or by calling
        any of the telephone numbers published in <a href="https://donweb.com/" target="_blank">our site</a>, <a onclick="Chat.openChat( event );">online chat< /a>.
                         You can also request it through the <a href="https://myaccount.donweb.com/support/help" target="_blank">Help Desk</a> and <a href="https://donweb .com/contact" target="_blank">contact form</a>.</p> --}}
                         <p class="p"><strong>Promotional offers and sweepstakes.</strong> If you provide your
                            name for a promotional offer or to enter a sweepstakes or
                            contest and you win a prize, we will publish a list of the winners
                            online where your name and city will be listed for promotional purposes and to
                             in order to demonstrate the transparency of the contest, and we will report your
                            name government agencies if required by law.</p>
                            {{-- <p class="p"><strong>*For services in the United States,</strong> when registering in
                             a Website and provide your telephone number, you consent that this
                            action constitutes a purchase, consultation and/or request for the purposes of the
                            telemarketing laws. Notwithstanding that your telephone number and/or
                            cell phone number may be included in the Federal Do Not Call Registry
                            ("Federal Do-Not-Call Registry") or your Local State Do Not Call Registry.
                            Call, you are giving your express written consent to
                            receive further information in the future (including tele-marketing)
                            relating to products and services from us and/or our
                            affiliates, and you hereby agree and consent to be contacted
                            using the information you have provided or provide to us in the
                            future. This means that we can contact you by email
                            electronic, telephone and/or cell phone (including the use of
                            automated calls and/or pre-recorded calls), text message
                            (SMS), social networks and any other form of communication that your
                            wireless device or any other type is capable of receiving (for
                            eg video, etc). We may also send you information or offers
                            occasionally to your postal or email address
                            we have in our files (as indicated in this <a href="https://donweb.com/es-mx/politica-de-privacidad">Privacy Policy</a>).</p> --} }

                            {{-- <p class="p"><strong>To protect our Site and our business.</strong>
                            We monitor use of our Sites and use the information you
                            we collect, including Personal Data, in order to pursue our
                            legitimate interest in protecting you, third parties and the site itself
                            by:</p>
                            <ul>
                            <li>the identification of fraudulent activities and transactions</li>
                            <li>the prevention of abuse of the Sites and the investigation and/or
                            seeking prosecution for any potential threat or misuse of the
                             Site.</li>
                            <li>assuring compliance with the terms of service and this Privacy Policy</li>
                            <li>investigation of violations of or enforcement of these agreements</li>
                            <li>protecting in any other way the rights and property of Web.com, its partners, and customers</li>
                            </ul> --}}
                            <p class="p"><strong>To improve our services.</strong> In order to pursue
                                our legitimate interest in improving our Sites, we may also
                                use Personal Data in order to conduct research and analysis and
                                 to identify what services you and other clients like you need:</p>
                                <ul>
                                <li>in order to better understand how people interact with our website in order to
                                 to provide product communications that we believe may
                                 be of interest to you</li>
                                <li>in order to determine the effectiveness of our advertising campaigns</li>
                                </ul>
                                <p class="p">When we process data for these purposes, we will ensure that
                                we will always protect to the fullest extent and take into account those
                                Rights. You have the right to object to this processing if you
                                want and if you want to do it, please contact us from the section called "contact us" at the bottom of the website
                                 in order to request removal from our lists. please keep in
                                Please note that if you object to this, it may affect our ability to
                                 develop tasks for your benefit.</p>
{{-- <p class="p"><strong>Para proveer información requerida por ICANN.</strong> Por
cualquier registro de nombres de dominio de titularidad de u operados
por Web.com, ICANN nos requiere difundir cierta información, recolectada
 con motivo del registro del nombre de dominio, y hacerla accesible al
público a través de una página web interactiva y un "puerto 43" del
servicio WHOIS. La información que publicamos consiste en su nombre
completo, dirección postal, número de teléfono, dirección de correo
electrónico, y, de ser suministrado, su número de fax, así como la fecha
 de alta y vencimiento del registro de su nombre de dominio y la
información del servidor de nombre asociado a su nombre de dominio. Esto
 no incluye la información demográfica, financiera o de uso de Internet
mencionada más arriba. A los fines de esta Política de Privacidad nos
referiremos a esta información en forma conjunta como "Información
WHOIS". Por favor advierta que no controlamos como el público pueda
utilizar la Información WHOIS. Para evitar cualquier tipo de duda, y a
fin de cumplir con ciertas leyes aplicables y requisitos de privacidad,
nos reservamos el derecho de enmascarar su información personalmente
identificable conforme fuera necesario, incluyendo sin limitar a,
enmarcar los datos publicados en Puerto 43 o la base de datos WHOIS.</p> --}}
{{-- <p class="p">Almacenamos la información por el tiempo que sea necesario a fin de
proveerle nuestros servicios, manejar nuestro negocio o según sea
requerido por ley o contrato. En caso de que Ud. haya celebrado un
contrato con nosotros, almacenaremos sus datos durante la duración del
contrato, y retendremos esa información por el período de tiempo
necesario de acuerdo a nuestras responsabilidades legales o
regulatorias, y luego de que Ud. dé de baja todos sus servicios con
nosotros a fin de resolver disputas, solicitar el cumplimiento de
nuestro Acuerdo de Servicios, y adherir a los requerimientos legales y
técnicos relacionados con la seguridad, integridad y operación de los
Sitios. Si Ud. tiene alguna inquietud sobre por cuánto tiempo
almacenamos algún dato específico, por favor contáctese a través de
nuestra <a href="https://donweb.com/contacto" target="_blank">página de contacto.</a></p> --}}
<h2>With whom we share your Personal Data</h2>
<p class="p">We provide your Personal Data to companies that help us with
our business activities (e.g. assisting us with
operations of the Sites, provide customer service, etc.) or that we
 assist in improving our products, content, services or
advertising. These companies are authorized to use your personal data
 only to the extent necessary to provide such services. When
we consult if you want to be included in directories, the information that we
 provide will be publicly disclosed.</p>
<p class="p">We will also share your personal data:</p>
<ul>
<li>As required by law, court order, or any other
government authority or supervisory authority
in order to enforce or apply our Services Agreement or other
agreements.</li>
<li>When we believe in good faith that disclosure is necessary in order to
to protect your rights, protect your safety or the safety of others,
 investigate fraud or respond to a government request. This includes the
exchange of information for law enforcement purposes, with
other companies and organizations for anti-fraud protection,
credit risk reduction or similar purposes.</li>
<li>In connection with a corporate change including a merger,
acquisition, transfer or sale of assets or in connection with
insolvency, bankruptcy or liquidation. Information regarding our
users, including customer information, should also be
disclosed to our legal and financial advisors, investors or
potential buyers in certain commercial transactions that have
for financing, investment or support.</li>
{{-- <li>A cualquier otro tercero cuando contemos con su consentimiento previo e informado a tales fines.</li>
<li>A otras entidades que pertenezcan al grupo Web.com (por ej.
cualquier sociedad controlada directa o indirectamente por Web.com). Una
 lista de nuestras sociedades está incluida <a href="https://www.sec.gov/Archives/edgar/data/1095291/000109529118000022/web201710k-ex211.htm" target="_blank">aquí</a>
 incluyendo sus vendedores, proveedores, contratistas y agentes, que
pueden estar involucrados en la provisión del Sitio o el contenido, a
fin de proporcionarle un mejor servicio o simplemente, proveer respuesta
 a sus requisitos (por ej. cuando utiliza planillas de contacto).</li>
</ul> --}}
{{-- <a id="adonde-transferimos-sus-datos-personales"></a><h2>A dónde transferimos sus Datos Personales</h2>
<p class="p">Los datos ingresados a través de sitios de DonWeb.com,
EnvialoSimple.com, SitioSimple.com y MiCuenta de DonWeb.com, se
encuentran y son procesados en la República Argentina. Así mismo,
ciertos datos son también transferidos y procesados en EE.UU.</p>
<p class="p">Escudo de Privacidad U.E.-EE.UU. y Suiza-EE.UU. Web.com y alguna de
sus subsidiarias (a saber, Franchise WebSitio Solutions, LP, Monster
Commerce, LLC, NameSecure, LLC, Network Solutions, LLC, Register.com,
Inc., RPI, Inc., SnapNames Web.com, LLC, Web.com Holding Company, Inc.,
Yodle Web.com, Inc.) participan de y cumplen con el Marco de Protección
de Privacidad U.E.-EE.UU. y el Marco de Protección de Privacidad
Suiza-EE.UU., de acuerdo con lo requerido por el Departamento de
Comercio de EE.UU. respecto de la recolección, uso y almacenamiento de
información personal que es transferida desde países miembros de la
Unión Europea (U.E.) y Suiza hacia los Estados Unidos (en forma
conjunta, el “Marco”). Para aprender más respecto del programa Escudo de
 Privacidad, y ver nuestra certificación, por favor visite
https://www.privacyshield.gov/. A fin de proporcionar una protección
adecuada para ciertos datos personales de la U.E. y datos personales de
Suiza, sobre clientes corporativos, clientes, proveedores, socios
comerciales recibidos en EE.UU., Web.com ha decidido auto-certificarse
dentro del Marco administrado por el Departamento de Comercio de EE.UU.
(“Escudo de Protección”). Web.com adhiere a los principios de
Notificación, Elección, Contabilidad para Posterior Transferencia,
Seguridad, Integridad y limitación por los fines de los datos, Acceso, y
 Recurso, Aplicabilidad, y Responsabilidad del Escudo de Protección.
Web.com es responsable por el procesamiento de los datos que recibe,
bajo este Marco de Escudo de Protección, y por cualquier transferencia
subsiguiente a un tercero que actúe como agente en su interés. Web.com
cumple con estos Principios del Escudo de Protección para toda
transferencia subsiguiente de datos personales desde la U.E y Suiza,
incluyendo la posterior transferencia de previsiones de responsabilidad.
 A los fines de asegurar el cumplimento del Escudo de Protección,
Web.com está sujeta a los poderes de investigación y control de
cumplimiento de la Comisión de Comercio Federal de los Estados Unidos
respecto de nuestro cumplimiento con el escudo de protección. Web.com
podrá ser requerida a develar información personal a agencias a cargo
del cumplimiento de la ley, regulatorias u otros entes gubernamentales, o
 a terceros, en cada caso a cumplir con las obligaciones o pedidos,
legales, regulatorios, o de seguridad nacional. Web.com se compromete a
resolver las disputas respecto de su privacidad y nuestra recolección o
use de su información personal. Los individuos de la Unión Europea o de
Suiza que tengan consultas o quejas respecto de esta Política de
Privacidad deberían en primer lugar contactarse con Web.com por correo
electrónico a privacyshield@web.com. Investigaremos e intentaremos
resolver cualquier queja o disputa dentro de los 45 días desde la
recepción de su reclamo. El mecanismo independiente de Web.com para
reclamos por Escudo de Privacidad para el uso de individuos de la U.E y
Suiza es JAMS. Si Ud. no se encuentra satisfecho con la resolución del
reclamo, puede contactarse mediante JAMS en
https://www.jamsadr.com/eu-us-privacy-shield para mayor información y
asistencia. Ud. tendrá la opción de seleccionar un arbitraje vinculante
para la resolución de su reclamo bajo ciertas circunstancias, siempre
que haya cumplido los siguientes pasos: (1) haya comunicado su reclamo
directamente a Web.com y nos haya dado la oportunidad de resolver el
problema; (2) haya utilizado el mecanismo de resolución de disputas
independiente identificado arriba; y (3) haya formulado el reclamo a
través de la autoridad competente de protección de datos personales y
dado la oportunidad al Departamento de Comercio de los EE.UU. a resolver
 la disputa sin costo para Ud. Si Ud. tiene alguna consulta respecto a
la participación de Web.com en el Marco de Escudo de Privacidad por
favor contáctenos en la siguiente dirección: privacyshield@web.com o por
 correo dirigido a Web.com Group, Inc., At.: Privacy Officer, 12808 Gran
 Bay Parkway West, Jacksonville, Florida, Estados Unidos 32258.</p> --}}

 <h2>How we protect your Personal Data</h2>
 <p class="p">We work to protect the security of your financial information
 during transmission using Secure encryption technology
 Sockets Layer ("SSL"), which encrypts the information you enter into the
 Site. However, no method of transmission over the Internet or
 electronic storage method is completely secure and does not
 we can guarantee your absolute safety. You can collaborate with your
 safety by taking a few simple precautions. For example, it is
 It is important to protect against unauthorized access to your computer or
  any username and password for Marktech.com services, and
 make sure you sign out when you're done using a
 shared computer.</p>

 <h2>Your Rights in relation to your Personal Data</h2>
 <p class="p">You will be empowered to require us to: </p>
 <ul>
 <li>Access a copy of your personal data.</li>
 <li>Correct, delete or restrict our processing of your personal data.</li>
 <li>Transfer the information you have provided to us under contract or consent to another organization.</li>
 <li>Stop processing your information, by withdrawing your consent or objecting to our legitimate interest.</li>
 </ul>
 <p class="p">Your rights regarding your personal data are limited
  in certain situations. For example, if we receive a request
 legal or for compelling legitimate reasons we can continue processing
 your information. We will provide you with information as to whether there are
 limits or exceptions that are applicable to any claim that
  formulate.</p>
 <p class="p">You can first view or modify the information you have provided to us upon request by clicking the "contact us" button at the bottom of the site,
  as applicable. Please note that you will not be required
 any type of financial information by telephone, and that in no
 case such information must be supplied without verification. You too
  You have the Right to make complaints if you feel that your Data
 personal have been treated inadequately. We recommend that in
 contact us first but, in the case of being
 applicable, you can make a claim directly to the authority of
 competent comptroller. This may be the controlling authority of the place
  of your residence or work, or the one located where you believe that the
 breach has occurred.</p>

 <h2>Changes to this Policy</h2>
 <p class="p">We may modify this Privacy Policy at any
 time, and we will communicate any changes to this Policy via email
 email to users who have provided an email address
 electronically or postally on our Site prior to the change
 come into force.</p>
     </div>
     @else
      {{-- test --}}
    <div style="text-align: center;">
        <h1 class="titles">Política de Privacidad</h1>
    </div>
        <div>
<p class="p">Esta Política de Privacidad describe nuestras políticas relativas a
la recolección, uso y revelación de su información en relación con su
uso de nuestras páginas web y correos electrónicos.
Los términos "nosotros" y "nuestro", cuando sean
utilizados en la presente política, se entenderán referidos a nuestra
sociedad controlante Marktech.com Group Inc. y a los integrantes de nuestro
grupo Marktech.com, nuestras sociedades controladas y subsidiarias. A los
fines de la legislación de protección de datos, somos una controlante de
 datos de la información relativa a Ud. con la que contamos en razón de
nuestro Sitio.</p>

<p class="p">Al visitar este sitio o por interactuar de cualquier otra
forma con Marktech.com, Ud. consiente las prácticas descritas en esta
Política de Privacidad.</p>
<p class="p">Cuando en esta Política de Privacidad se utilice el término “datos
personales”, se entenderá por tales a cualquier Dato referido a un
individuo identificable, incluyendo su nombre, domicilio, identificador
online, información de pago o casilla de correo electrónico. Los Datos
específicos que recolectamos se describen en esta política.</p>
<p class="p">Los alentamos a leer esta Política de Privacidad en forma completa, la que contiene las siguientes secciones:</p>
<ul>
<a id="indice"></a><li><a onclick="Visibilidad.scrollAncla('#informacion-que-recolectamos', 500);">Información que recolectamos</a></li>
<li><a onclick="Visibilidad.scrollAncla('#como-usamos-sus-datos-personales', 700);">Cómo usamos sus Datos Personales</a></li>
<li><a onclick="Visibilidad.scrollAncla('#con-quien-compartimos-sus-datos-personales', 1100);">Con quién compartimos sus Datos Personales</a></li>
<li><a onclick="Visibilidad.scrollAncla('#adonde-transferimos-sus-datos-personales', 1300);">Adónde transferimos sus Datos Personales</a></li>
<li><a onclick="Visibilidad.scrollAncla('#como-protegemos-sus-datos-personales', 1500);">Cómo protegemos sus Datos Personales</a></li>
<li><a onclick="Visibilidad.scrollAncla('#sus-derechos-en-relacion-con-sus-datos-personales', 1700);">Sus Derechos en relación con sus Datos Personales</a></li>
<li><a onclick="Visibilidad.scrollAncla('#cambios-a-esta-politica', 2500);">Cambios a esta Política</a></li>
<li><a onclick="Visibilidad.scrollAncla('#contacto', 2700);">Contacto</a></li>
</ul>
<h2>Información que recolectamos</h2>
<p class="p"><strong>Información de contacto.</strong> Recolectamos y almacenamos
la información de contacto personal que Ud. nos provee voluntariamente
al suscribirse a nuestros productos o servicios o a cualquiera de
nuestras listas de correo. Esto incluye su nombre, dirección postal,
dirección web, número de teléfono, número de fax y dirección de correo
electrónico. Adicionalmente, recolectamos información de contacto que
nos haya sido provista durante cualquier intercambio de correspondencia
relacionada con atención al cliente, así como aquella relacionada con
los productos y servicios ofrecidos en nuestros Sitios.</p>
{{-- <p class="p"><strong>Información financiera.</strong> Recolectamos y almacenamos
información financiera que Ud. nos provee cuando encarga nuestros
productos o servicios. Por ejemplo, cuando Ud. solicita el registro de
un nombre de dominio o solicita cualquier otro producto o servicio a
través de Web.com, solicitamos que Ud. provea el número de una tarjeta
de crédito y domicilio de facturación antes de procesar la solicitud.</p> --}}
<p class="p"><strong>Información sobre su uso del Sitio.</strong> Recolectamos
información sobre el dispositivo con el que Ud. accede a los Sitios e
información relativa a su uso del Sitio en sí mismo, como por ejemplo
nombre de dominio, proveedor de servicio de Internet, tipo y versión del
 explorador, páginas visitadas, información a la que accedió, y el
protocolo de Internet (IP) que utilizó para conectarse a Internet.
También recolectamos información relativa a cookies – para mayor
información, por favor revise nuestra Política de Cookies.</p>
<p class="p"><strong>Información demográfica.</strong> Recolectamos y almacenamos
información demográfica (su código postal y edad). También
podremos recolectar y almacenar información demográfica (como por
ejemplo su código postal y edad), y utilizar estos datos a fin
de adaptar su experiencia en nuestros Sitios Web.</p>
{{-- <p class="p"><strong>Grabaciones telefónicas y monitoreo.</strong> Para asegurar
que los clientes de Web.com reciban un servicio de calidad, Web.com
selecciona llamadas telefónicas para ser grabadas y/o monitoreadas.
Estas llamadas, entre los clientes de Web.com (o potenciales clientes) y
 empleados, son evaluadas por supervisores y/u otros representantes de
Web.com. Esto es para garantizar que se provea en forma profesional una
asistencia rápida y adecuada e información precisa.</p> --}}
<p class="p"><strong>Para mejorar nuestros servicios.</strong>
<p class="p">A fin de contratar con Ud. utilizamos esta información para contratar con Ud. y satisfacer sus pedidos, como por ejemplo:</p>
<ul>
<li>Enviarle la información del servicio que sea requerida</li>
<li>Dar cumplimento a sus pedidos</li>
<li>Responder a solicitudes de clientes de servicios, preguntas y consultas</li>
<li>Administrar su cuenta</li>
</ul>
<p class="p">Advierta que, generalmente, Ud. no podrá solicitar ser excluido de
estas comunicaciones, que son de naturaleza transaccional y no
promocional. Si Ud. no desea recibirlas, Ud. tiene la alternativa de
cancelar nuestros servicios.</p>
<p class="p">También contratamos con otras compañías, individuos, y subsidiarias
de Marktech.com para que lleven a cabo ciertas funciones y servicios para Ud.
 Estos terceros:</p>
<ul>
<li>procesan pagos de tarjetas de crédito</li>
</ul>
<p class="p"><strong>Para contactarlo.</strong> Si Ud. ha prestado su
consentimiento para recibir correos electrónicos, o marketing vía
telefónica o SMS, o si nos ha suministrado su información postal,
podremos utilizar los datos personales suministrados para contactarlo
ocasionalmente. Si Ud. se registra en el Sitio, Ud. puede optar por
proveer su consentimiento para recibir comunicaciones de marketing,
comunicaciones por e-mail, teléfono fijo y/o celular (incluyendo el uso
de equipos automatizados de marcación y/o llamadas pre-grabadas),
mensaje de texto (SMS), redes sociales y cualquier otra forma de
comunicación que su dispositivo sea capaz de recibir (por ej. video,
{{-- etc.). Si Ud. no desea recibir comunicaciones postales o electrónicas,
puede retirar su consentimiento en cualquier momento, haciendo clic en
el enlace de desuscripción al pie de la comunicación, o llamando a
cualquiera de los teléfonos publicados en <a href="https://donweb.com/" target="_blank">nuestro sitio</a>, <a onclick="Chat.abrirChat( event );">chat online</a>.
                 También puedes solicitarlo a través de la <a href="https://micuenta.donweb.com/soporte/ayuda" target="_blank">Mesa de Ayuda</a> y <a href="https://donweb.com/contacto" target="_blank">formulario de contacto</a>.</p> --}}
<p class="p"><strong>Ofertas promocionales y sorteos.</strong> Si Ud. provee su
nombre para una oferta promocional o para participar de un sorteo o
concurso y Ud. gana un premio, publicaremos una lista de los ganadores
online donde su nombre y su ciudad será listada a fines promocionales y a
 fin de demostrar la transparencia del concurso, y reportaremos su
nombre a las agencias gubernamentales de ser requerido por ley.</p>
{{-- <p class="p"><strong>*Para servicios en Estados Unidos,</strong> al registrarse en
 un Sitio Web y suministrar su número telefónico, Ud. consiente que esa
acción constituye una compra, consulta y/o solicitud a los fines de las
leyes de tele-marketing. Sin perjuicio de que su número telefónico y/o
número de celular pueda ser incluido en el Registro Federal No Llame
("Federal Do-Not-Call Registry") o en su Registro Local Estatal No
Llame, Ud. está prestando su consentimiento expreso y por escrito para
recibir mayor información en el futuro (incluyendo tele-marketing)
relativa a productos y servicios de nuestra parte y/o nuestras
afiliadas, y Ud. acuerda y consiente por el presente que lo contactemos
usando la información que nos ha suministrado o nos suministre en el
futuro. Esto implica que podremos contactarlo mediante correo
electrónico, teléfono y/o celular (incluyendo el uso de equipos de
llamadas automatizadas y/o llamadas pre-grabadas), mensaje de texto
(SMS), redes sociales y cualquier otra forma de comunicación que su
dispositivo wireless o de cualquier otro tipo sea capaz de recibir (por
ej. video, etc.). También podremos enviarle información u ofertas
ocasionalmente a su dirección postal o de correo electrónico que
tengamos en nuestros archivos (conforme lo señalado en esta <a href="https://donweb.com/es-mx/politica-de-privacidad">Política de Privacidad</a>).</p> --}}

{{-- <p class="p"><strong>Para proteger nuestro Sitio y nuestro negocio.</strong>
Vigilamos el uso de nuestros Sitios y utilizamos la información que
recolectamos, incluyendo Datos personales, a fin de perseguir nuestro
interés legítimo en protegerlo a Ud., a terceros y al propio sitio
mediante:</p>
<ul>
<li>la identificación de actividades y transacciones fraudulentas</li>
<li>la prevención del abuso de los Sitios y la investigación y/o
búsqueda de enjuiciamiento por cualquier amenaza potencial o mal uso del
 Sitio.</li>
<li>el aseguramiento del cumplimiento de los términos del servicio y de esta Política de Privacidad</li>
<li>la investigación de violaciones de o el cumplimiento de estos acuerdos</li>
<li>la protección de cualquier otra forma de los derechos y propiedad de Web.com, sus socios, y clientes</li>
</ul> --}}
<p class="p"><strong>Para mejorar nuestros servicios.</strong> A fin de perseguir
nuestro interés legítimo en mejorar nuestros Sitios, también podemos
utilizar Datos personales a fin de conducir investigaciones y análisis y
 para identificar qué servicios Ud. y otros clientes como Ud. necesitan:</p>
<ul>
<li>a fin de entender mejor cómo la gente interactúa con nuestro sitio web a fin
 de proveer comunicaciones de productos que creamos que pueden
 ser de su interés</li>
<li>a fin de determinar la efectividad de nuestras campañas publicitarias</li>
</ul>
<p class="p">Cuando procesamos datos para estos fines, nos aseguraremos de que
siempre protegeremos de la forma más amplia y tendremos en cuenta esos
derechos. Ud. tiene el Derecho de objetar este procesamiento si Ud.
desea y si Ud. desea hacerlo, por favor contáctese con nosotros desde la sección llamada "contactanos" en la parte inferior del sitio web
 a fin de solicitar la remoción de nuestras listas. Por favor tenga en
cuenta que si Ud. objeta esto, ello puede afectar nuestra capacidad para
 desarrollar tareas para su beneficio.</p>
{{-- <p class="p"><strong>Para proveer información requerida por ICANN.</strong> Por
cualquier registro de nombres de dominio de titularidad de u operados
por Web.com, ICANN nos requiere difundir cierta información, recolectada
 con motivo del registro del nombre de dominio, y hacerla accesible al
público a través de una página web interactiva y un "puerto 43" del
servicio WHOIS. La información que publicamos consiste en su nombre
completo, dirección postal, número de teléfono, dirección de correo
electrónico, y, de ser suministrado, su número de fax, así como la fecha
 de alta y vencimiento del registro de su nombre de dominio y la
información del servidor de nombre asociado a su nombre de dominio. Esto
 no incluye la información demográfica, financiera o de uso de Internet
mencionada más arriba. A los fines de esta Política de Privacidad nos
referiremos a esta información en forma conjunta como "Información
WHOIS". Por favor advierta que no controlamos como el público pueda
utilizar la Información WHOIS. Para evitar cualquier tipo de duda, y a
fin de cumplir con ciertas leyes aplicables y requisitos de privacidad,
nos reservamos el derecho de enmascarar su información personalmente
identificable conforme fuera necesario, incluyendo sin limitar a,
enmarcar los datos publicados en Puerto 43 o la base de datos WHOIS.</p> --}}
{{-- <p class="p">Almacenamos la información por el tiempo que sea necesario a fin de
proveerle nuestros servicios, manejar nuestro negocio o según sea
requerido por ley o contrato. En caso de que Ud. haya celebrado un
contrato con nosotros, almacenaremos sus datos durante la duración del
contrato, y retendremos esa información por el período de tiempo
necesario de acuerdo a nuestras responsabilidades legales o
regulatorias, y luego de que Ud. dé de baja todos sus servicios con
nosotros a fin de resolver disputas, solicitar el cumplimiento de
nuestro Acuerdo de Servicios, y adherir a los requerimientos legales y
técnicos relacionados con la seguridad, integridad y operación de los
Sitios. Si Ud. tiene alguna inquietud sobre por cuánto tiempo
almacenamos algún dato específico, por favor contáctese a través de
nuestra <a href="https://donweb.com/contacto" target="_blank">página de contacto.</a></p> --}}
<h2>Con quién compartimos sus Datos Personales</h2>
<p class="p">Suministramos sus Datos personales a empresas que nos ayudan con
nuestras actividades comerciales (por ej. que nos asisten con las
operaciones de los Sitios, proveen servicios al cliente, etc.) o que nos
 asisten en mejorar nuestros productos, contenidos, servicios o
publicidad. Estas empresas están autorizadas a usar sus datos personales
 sólo en cuanto sea necesario para proveer dichos servicios. Cuando le
consultamos si desea ser incluido en directorios, la información que nos
 provea será difundida públicamente.</p>
<p class="p">También compartiremos sus datos personales:</p>
<ul>
<li>Según sea requerido por ley, orden judicial, o por cualquier otra
autoridad gubernamental o autoridad de contralor a
fin de hacer cumplir o aplicar nuestro Acuerdo de Servicios u otros
acuerdos.</li>
<li>Cuando creamos de buena fe que dicha revelación es necesaria a fin
de proteger sus derechos, proteger su seguridad o la seguridad de otros,
 investigar fraude o responder un pedido gubernamental. Esto incluye el
intercambio de información a los fines de la aplicación de la ley, con
otras empresas y organizaciones para la protección anti-fraude,
reducción de riesgo crediticio o a fines análogos.</li>
<li>En relación con una modificación societaria incluyendo una fusión,
adquisición, transferencia o venta de activos o en relación con
insolvencia, quiebra o liquidación. Información respecto de nuestros
usuarios, incluyendo información de clientes, también deberá ser
develada a nuestros asesores legales y financieros, inversores o
potenciales compradores en ciertas transacciones comerciales que tengan
por objeto financiación, inversión o apoyo.</li>
{{-- <li>A cualquier otro tercero cuando contemos con su consentimiento previo e informado a tales fines.</li>
<li>A otras entidades que pertenezcan al grupo Web.com (por ej.
cualquier sociedad controlada directa o indirectamente por Web.com). Una
 lista de nuestras sociedades está incluida <a href="https://www.sec.gov/Archives/edgar/data/1095291/000109529118000022/web201710k-ex211.htm" target="_blank">aquí</a>
 incluyendo sus vendedores, proveedores, contratistas y agentes, que
pueden estar involucrados en la provisión del Sitio o el contenido, a
fin de proporcionarle un mejor servicio o simplemente, proveer respuesta
 a sus requisitos (por ej. cuando utiliza planillas de contacto).</li>
</ul> --}}
{{-- <a id="adonde-transferimos-sus-datos-personales"></a><h2>A dónde transferimos sus Datos Personales</h2>
<p class="p">Los datos ingresados a través de sitios de DonWeb.com,
EnvialoSimple.com, SitioSimple.com y MiCuenta de DonWeb.com, se
encuentran y son procesados en la República Argentina. Así mismo,
ciertos datos son también transferidos y procesados en EE.UU.</p>
<p class="p">Escudo de Privacidad U.E.-EE.UU. y Suiza-EE.UU. Web.com y alguna de
sus subsidiarias (a saber, Franchise WebSitio Solutions, LP, Monster
Commerce, LLC, NameSecure, LLC, Network Solutions, LLC, Register.com,
Inc., RPI, Inc., SnapNames Web.com, LLC, Web.com Holding Company, Inc.,
Yodle Web.com, Inc.) participan de y cumplen con el Marco de Protección
de Privacidad U.E.-EE.UU. y el Marco de Protección de Privacidad
Suiza-EE.UU., de acuerdo con lo requerido por el Departamento de
Comercio de EE.UU. respecto de la recolección, uso y almacenamiento de
información personal que es transferida desde países miembros de la
Unión Europea (U.E.) y Suiza hacia los Estados Unidos (en forma
conjunta, el “Marco”). Para aprender más respecto del programa Escudo de
 Privacidad, y ver nuestra certificación, por favor visite
https://www.privacyshield.gov/. A fin de proporcionar una protección
adecuada para ciertos datos personales de la U.E. y datos personales de
Suiza, sobre clientes corporativos, clientes, proveedores, socios
comerciales recibidos en EE.UU., Web.com ha decidido auto-certificarse
dentro del Marco administrado por el Departamento de Comercio de EE.UU.
(“Escudo de Protección”). Web.com adhiere a los principios de
Notificación, Elección, Contabilidad para Posterior Transferencia,
Seguridad, Integridad y limitación por los fines de los datos, Acceso, y
 Recurso, Aplicabilidad, y Responsabilidad del Escudo de Protección.
Web.com es responsable por el procesamiento de los datos que recibe,
bajo este Marco de Escudo de Protección, y por cualquier transferencia
subsiguiente a un tercero que actúe como agente en su interés. Web.com
cumple con estos Principios del Escudo de Protección para toda
transferencia subsiguiente de datos personales desde la U.E y Suiza,
incluyendo la posterior transferencia de previsiones de responsabilidad.
 A los fines de asegurar el cumplimento del Escudo de Protección,
Web.com está sujeta a los poderes de investigación y control de
cumplimiento de la Comisión de Comercio Federal de los Estados Unidos
respecto de nuestro cumplimiento con el escudo de protección. Web.com
podrá ser requerida a develar información personal a agencias a cargo
del cumplimiento de la ley, regulatorias u otros entes gubernamentales, o
 a terceros, en cada caso a cumplir con las obligaciones o pedidos,
legales, regulatorios, o de seguridad nacional. Web.com se compromete a
resolver las disputas respecto de su privacidad y nuestra recolección o
use de su información personal. Los individuos de la Unión Europea o de
Suiza que tengan consultas o quejas respecto de esta Política de
Privacidad deberían en primer lugar contactarse con Web.com por correo
electrónico a privacyshield@web.com. Investigaremos e intentaremos
resolver cualquier queja o disputa dentro de los 45 días desde la
recepción de su reclamo. El mecanismo independiente de Web.com para
reclamos por Escudo de Privacidad para el uso de individuos de la U.E y
Suiza es JAMS. Si Ud. no se encuentra satisfecho con la resolución del
reclamo, puede contactarse mediante JAMS en
https://www.jamsadr.com/eu-us-privacy-shield para mayor información y
asistencia. Ud. tendrá la opción de seleccionar un arbitraje vinculante
para la resolución de su reclamo bajo ciertas circunstancias, siempre
que haya cumplido los siguientes pasos: (1) haya comunicado su reclamo
directamente a Web.com y nos haya dado la oportunidad de resolver el
problema; (2) haya utilizado el mecanismo de resolución de disputas
independiente identificado arriba; y (3) haya formulado el reclamo a
través de la autoridad competente de protección de datos personales y
dado la oportunidad al Departamento de Comercio de los EE.UU. a resolver
 la disputa sin costo para Ud. Si Ud. tiene alguna consulta respecto a
la participación de Web.com en el Marco de Escudo de Privacidad por
favor contáctenos en la siguiente dirección: privacyshield@web.com o por
 correo dirigido a Web.com Group, Inc., At.: Privacy Officer, 12808 Gran
 Bay Parkway West, Jacksonville, Florida, Estados Unidos 32258.</p> --}}

<h2>Cómo protegemos sus Datos Personales</h2>
<p class="p">Trabajamos para proteger la seguridad de su información financiera
durante su transmisión al utilizar la tecnología de cifrado Secure
Sockets Layer ("SSL"), que encripta la información que Ud. ingresa al
Sitio. Sin embargo, ningún método de transmisión dentro de Internet o
método de almacenamiento electrónico es completamente seguro y no
podemos garantizar su seguridad absoluta. Ud. puede colaborar con su
seguridad adoptando unas pocas simples precauciones. Por ejemplo, es
importante protegerse contra un acceso no autorizado a su computadora o a
 cualquier usuario y contraseña de los servicios de Marktech.com, y
asegurarse de cerrar sesión cuando haya finalizado de usar una
computadora compartida.</p>

<h2>Sus Derechos en relación con sus Datos Personales</h2>
<p class="p">Ud. estará facultado a requerirnos a: </p>
<ul>
<li>Acceder a una copia de sus datos personales.</li>
<li>Corregir, eliminar o restringir nuestro procesamiento de sus datos personales.</li>
<li>Transferir la información que Ud. nos haya provisto bajo contrato o consentimiento con otra organización.</li>
<li>Dejar de procesar su información, mediante el retiro de su consentimiento u objetando nuestro interés legítimo.</li>
</ul>
<p class="p">Sus derechos relativos a sus datos personales se encuentran limitados
 en ciertas situaciones. Por ejemplo, si recibimos un requerimiento
legal o ante motivos legítimos imperiosos podremos continuar procesando
su información. Le proveeremos información respecto de si existen
límites o excepciones que resulten aplicables ante cualquier reclamo que
 formule.</p>
<p class="p">En primer lugar Ud. puede ver o modificar la información que nos haya provisto mediante una solicitud haciendo clic en la parte inferior del sitio en el bóton "contactanos",
 según sea aplicable. Por favor tenga en cuenta que no se le requerirá
ningún tipo de información financiera telefónicamente, y que en ningún
caso tal información debe ser suministrada sin verificación. Ud. también
 tiene el Derecho de formular quejas si Ud. siente que sus Datos
personales han sido tratados inadecuadamente. Le recomendamos que en
primer lugar se ponga en contacto con nosotros pero, en el caso de ser
aplicable, Ud. puede formular un reclamo directamente a la autoridad de
contralor competente. Esta puede ser la autoridad de contralor del lugar
 de su residencia o trabajo, o aquella ubicada donde Ud. crea que el
incumplimiento ha tenido lugar.</p>

<h2>Cambios a esta Política</h2>
<p class="p">Nosotros podremos modificar esta Política de Privacidad en cualquier
momento, y comunicaremos cualquier cambio a esta Política vía correo
electrónico a los usuarios que hayan provisto una dirección de correo
electrónico o postal en nuestro Sitio con anterioridad a que el cambio
entre en vigor.</p>
    </div>
    @endif
@endsection
