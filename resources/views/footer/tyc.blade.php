@extends('layouts.app')

@section('content')

@section('title', 'Marktech - Terminos y condiciones')
@if (App::getLocale() == 'en')
    {{-- test --}}
    <div style="text-align: center;">
        <br>
        <h1 class="titles"><a id="Terms-and-conditions-of-use" name="Terms-and-conditions-of-use"></a>Terms and conditions
            of
            use
    </div>
    <div>
        <p class="p">By using the www.Marktech.com site you signify your acceptance of these Terms of Use and our <a
                href="/privacy notice"><strong>Privacy Policy</strong></a>.
            If you do not accept these Terms and Conditions of Use, please do not
            use these sites. Marktech S.A. of C.V. (‘Marktech’)
            reserves the right, at our discretion, to change, add, remove or
            modify portions of these Terms and Conditions of Use at any
            moment. Please review these terms and conditions of use periodically to
            the changes. Your continued use of these sites after publication
            of the modifications of these conditions of use means that you
            accept those changes.</p>
        <div id="numbered-list">
            <ol>
                <li>
                    <a href="#Possession">Possession</a>
                </li>
                <li><a href="#License-and-use-of-the-site">License and Use of the Site</a></li>
                <li><a href="#Your-account">Your account</a></li>
                <li><a href="#Requests">Requests</a></li>
                <li><a href="#Content-linked-to-any-Marktech-website">Content linked to any Marktech website</a></li>
                <li><a href="#Disclaimer-Clause">Disclaimer Clause</a></li>
                <li><a href="#Credit-Cards">Credit Cards</a></li>
                <li><a href="#Indemnity">Indemnity</a></li>
                <li><a href="#Limitation-Of-Liability">Limitation of Liability</a></li>
                <li><a href="#Competence-issues">Competition issues “jurisdictional incidents”</a></li>
                <li><a href="#Termination">Termination</a></li>
                <li><a href="#Copyright-Claims">Notice and Procedures for Making Claims of Copyright Infringement</a>
                </li>
                <li><a href="#General-dispositions-and-provisions">General-dispositions and provisions</a></li>
                <li><a href="#Warranty-Disclaimer">Accuracy of Information and Warranty Disclaimer</a></li>
            </ol>
        </div>
        <h2><a id="Possession" name="Possession"></a></h2>
        <h2>&nbsp;</h2>
        <h2>Possession</h2>
        <p class="p">All content on these sites, including, without limitation, text,
            graphics, images, logos, audio or video clips, downloads
            digital data, compilations of data and software, are the property of
            Marktech or content licensor providers and is
            protected by the laws of the United Mexican States and other countries
            in addition to international treaties. The compilation of all
            The content of these sites is the property of Marktech and is protected
            by the laws of the United Mexican States and other countries in addition to
            international treaties. All other brands that are not
            property of Marktech that appear on these sites are the property of
            their respective owners, which may or may not be affiliated with or
            connected to Marktech. <a href="#Terms-and-conditions-of-use"></a></p>
        <h2><a name="License-and-use-of-the-site" id="License-and-use-of-the-site"></a></h2>
        <h2>&nbsp;</h2>
        <h2>License and Use of the Site</h2>
        <p class="p">Marktech grants you a limited license to access and use
            personal and non-commercial use of these sites. In accordance with
            these Terms of Use, you are not allowed to download any
            material (including, without limitation, software, text, graphics or other
            contents), except for the printing of simple copies of the
            pages, as necessary to access the sites (for use
            personal and non-commercial use, provided that all
            copyright and proprietary notices), links to any page or
            modify all or part of the sites without the express consent
            in writing from Marktech. You may not redistribute, sell,
            disseminate, reverse engineer, disassemble or reduce software
            legibly that you are permitted to download information from these
            sites, except where permitted by law. With the sole exception of
            expressly provided herein, these sites (or any work
            derived from the version thereof), its content and any member or
            account information may not, in any way or for any
            means now known or developed to reproduce, display,
            download, upload, published, reuse, move, distribute,
            transmit, resell or exploit for any commercial purpose without
            prior express written consent of Marktech. All the
            rights not expressly granted to you previously, including the
            ownership and title are reserved to the owner and are not transferred or
            licensed in your favor.
        <h2><a id="Your-account" name="Your-account"></a></h2>
        <h2>&nbsp;</h2>
        <h2>Your account</h2>
        <p class="p">If you use these sites, you are responsible for maintaining the
            confidentiality of membership and account information,
            credit card information, usernames, passwords and
            identifiers that may be required to use the site from time to time
            when ("Account Information") and to restrict access to your
            computer or other mobile devices, and you agree that it is
            responsible for all activity that occurs under your account or with the use
            of your account information (including, without limitation, the names
            username and password). Marktech reserves the right, at its sole
            discretion, deny access to the site or the services provided to
            through it, close accounts and rights of use, edit or delete
            content or submissions (as defined below) in addition to the
            cancellation of orders or materials that have been placed through the site
            required.
        <h2><a id="Requests" name="Requests"></a></h2>
        <h2>&nbsp;</h2>
        <h2>Requests</h2>
        <p class="p">Any information or material that you transmit, upload or
            deliver to any Marktech site (including, without limitation,
            comments, reviews, chat announcements, email messages or
            of materials directed to any forum, since the term is defined
            below) or any creative suggestions, ideas, notes,
            drawings, concepts or other information sent to Marktech through
            our website or other means of transmission or delivery, will be
            collectively referred to as "Submissions". If you want to transmit or
            submit presentations to Marktech you grant Marktech a
            non-exclusive, royalty-free, perpetual, irrevocable license (or the
            longest period permitted by law) of leave (with the right to
            sublicense and assign) to use, reproduce, modify, adapt,
            publish, translate, publicly perform and display, transmit,
            create, sell, create derivative works from, and distribute those writings or
            presentations as incorporated into other works in any form or
            medium and by any means or means of distribution or technology
            currently known or developed in the future. You agree and
            represents Marktech that owns or has obtained ownership
            intellectual and other necessary rights in the presentations
            (including, without limitation, a waiver of applicable rights in
            moral) and to grant such license to Marktech that are not such
            presentations, or will be, subject to any obligation of
            confidentiality by Marktech and that Marktech will not be
            responsible for any use or disclosure of any submission.
            Without limiting the foregoing, Marktech shall have the right to use without
            shipping restrictions, commercial or otherwise, without compensation
            to the shipping provider.
            {{-- <h2><a id="Foros-y-Comunicacion-Publica" name="Foros-y-Comunicacion-Publica"></a></h2>
<h2>&nbsp;</h2> --}}
            {{-- <h2>Foros y Comunicación Pública</h2>
<p class="p">"Foros", un área de chat, tablón de anuncios, la función de correo
electrónico u otra función que le permite transmitir o someter el
material al sitio de Marktech para su visualización, almacenamiento o
 distribución, se ofrecen como parte de cualquier sitio de Marktech o
 por una compañía afiliada / organización y / o proveedor de servicios
de Marktech donde el Foro está previsto en un sitio distinto de un
sitio de Marktech, usted estará obligado por los términos del
servicio y aviso de privacidad del sitio que han vinculado. Si usted
participa en cualquier Foro dentro de un sitio de Marktech, usted
debe y no acepta que no va a través del uso de las comunicaciones o de
otro modo:</p>
<ul id="list">
<li><span>Difamar, abusar, acosar o amenazar a otros;</span></li>
<li><span>Hacer cualquier burla de odio o racial que sean ofensivas las declaraciones;</span></li>
<li><span>Promover una actividad ilegal o discutir actividades ilegales con la intención de cometer los mismos;</span></li>
<li><span>Publicar o distribuir cualquier material que infrinja o viole cualquier derecho de un tercero o cualquier ley;</span></li>
<li><span>Publicar o distribuir cualquier material vulgar, obsceno, descortés, lenguaje indecente o imágenes ofensivas;</span></li>
<li><span>Anunciar, vender, o solicitar, a otros;</span></li>
<li><span>Utilizar el Foro con fines comerciales de ningún tipo;</span></li>
<li><span>Publicar o distribuir cualquier software u otros materiales que contengan un virus u otro componente dañino, o</span></li>
<li><span>Publicar material o hacer declaraciones que no corresponden en
 general con el tema designado o el tema de cualquier sala de chat o
tablón de anuncios.</span></li>
</ul> --}}
        <p class="p">In addition, you must be aware and agree that you will not use a
            false email address, impersonate someone else, or
            entity or otherwise mislead others as the source of
            origin of a presentation. Marktech reserves the right to remove
            or edit the content of any site that contains a forum
            Marktech at any time and for any reason.</p>
        <p class="p">All broadcast material, which is posted or delivered to a Forum
            shall constitute a request and is governed by the conditions applicable to
            shipments as described in this document.</p>
        <p class="p">When participating in a forum, never assume that people are who
            say they are, know what they say they know, or are affiliated with
            who say they are affiliated with any chat room, bulletin board
            messages or other user generated content area. Information
            obtained from a forum may not be reliable, and Marktech does not endorse
            responsible for the content or accuracy of the information.
        <h2><a id="Content-linked-to-any-Marktech-website" name="Content-linked-to-any-Marktech-website"></a></ h2>
            <h2>&nbsp;</h2>
            <h2>Content linked to any Marktech website</h2>
            <p class="p">Be discreet when browsing the Internet using any website
                Marktech. You should be aware that while you are in a
                Marktech site, you may be directed to other sites that are
                out of our control. There are links to other Marktech sites and
                sites that take you out of our service. This includes the links
                of the regional sections, sponsors and partners who can
                use our logo(s) as part of a co-branding or other
                agreement. These other sites may send their own cookies to
                users, obtain information, request personal information, or
                contain information that you consider inappropriate or offensive.
                Marktech reserves the right to disable links to sites of
                third parties to any Marktech site. Marktech does not offer any
                warranty regarding the content of sites linked to any
                Marktech site or listed in any of our directories. In
                Consequently, Marktech is not responsible for the accuracy,
                relevance, copyright compliance, legality or decency of the
                material contained on the sites listed in our search results
                search or otherwise linked to a Marktech site.
            <h2><a id="Disclaimer-Clause" name="Disclaimer-Clause"></a></h2>
            <h2>&nbsp;</h2>
            <h2>Disclaimer Clause</h2>
            <p class="p">The materials on these Marktech web sites are provided
                "as is" and without warranty of any kind, either express or implied. In
                To the fullest extent applicable by law, Marktech its
                contractors, agents, affiliates, partners, and intended beneficiaries
                of third parties (collectively with the "Marktech" parties), you waive any
                all warranties, express or implied, including, but not limited to,
                warranties of merchantability and fitness for a particular purpose.
                Marktech does not warrant that the functions contained in the materials
                of the Marktech website will be uninterrupted or error-free,
                that the defects will be corrected, or that any website of
                Marktech or the servers that make such materials available
                free of viruses or other harmful components. marktech not
                warranties and makes no representations about the use or
                results of the use of the materials on Marktech's web site in
                as to its correctness, accuracy, reliability or otherwise. you assume the
                full cost of any service, repair or correction. The law
                applicable law may not allow the exclusion of implied warranties, so
                that the above exclusion may not apply to you.</p>
            <p class="p">Marktech expressly disclaims any responsibility for the
                accuracy, completeness, content or availability of the
                information contained on sites that have links to or from
                any Marktech website. Marktech cannot guarantee that
                you will be satisfied with the products or services you buy
                on a third party site that you link to or from any third party website.
                Marktech or third party content on any Marktech website
                Marktech. Marktech is not responsible for the merchandise (in its
                case), except as expressly provided, nor has it taken any measures to
                confirm the accuracy or reliability of any of the information
                contained on such third party sites or their content. marktech not
                makes no representation or warranty as to the security of the
                information (including, without limitation, credit card and other
                personal information) that may be requested to give to third parties, and
                You hereby irrevocably waive any claim against
                Marktech. With respect to such third party sites and content.
                Marktech strongly encourages you to do your research
                consider necessary or appropriate before proceeding with any
                online or offline transaction with any of these
                third parties.
            <h2><a id="Credit-Cards" name="Credit-Cards"></a></h2>
            <h2>&nbsp;</h2>
            <h2>Credit Cards</h2>
            <p class="p">Marktech may provide your credit card number,
                billing and shipping information to financial institutions
                legally constituted and contracted by Marktech for the
                payment processing. Otherwise, Marktech does not share your
                credit card information with another person. to get more
                information, please read our Privacy Notice.</p>
            <p class="p">To protect the security of your credit card information,
                Marktech employs the industry standard of Secure Sockets Layer
                (SSL). Marktech also encrypts your credit card number
                when Marktech stores your request and as long as Marktech
                pass on information to participating merchants.
            <h2><a id="Indemnity" name="Indemnity"></a></h2>
            <h2>&nbsp;</h2>
            <h2>Compensation</h2>
            <p class="p">You are entirely responsible for maintaining the confidentiality
                and security of your account information and all activities that
                occur under your account. You agree to indemnify, defend and hold
                Marktech its subsidiaries and other affiliated companies/organizations,
                contractors, agents, partners and sponsors and their respective
                officers, directors, volunteers, employees and agents of and against
                any third party claims, lawsuits, actions, lawsuits,
                procedures, responsibilities, damages, losses, judgments and expenses
                (including, but not limited to, collection costs,
                attorneys and other reasonable costs of defending or enforcing your
                obligations hereunder) as a result of arising out of
                any violation of any of its representations or misuse of
                this or any other Marktech website or any site with
                links to this or any other Marktech website. You must
                use your best efforts to cooperate with us in defending
                of any demand.
            <h2><a id="Indemnity Clause" name="Indemnity Clause"></a></h2>
            <h2>&nbsp;</h2>
            <h2>Indemnity Clause</h2>
            <p class="p">Marktech assumes no responsibility for unauthorized access
                authorized by the client or the users to the information of which
                "hacking" networks participate, or any viruses or harmful programs,
                that may be presented by a client or user, or by the use of the
                information received through the service.</p>
            <p class="p">The Marktech Service below is subject to the
                interruption and delay due to causes beyond its control, such as
                cases of force majeure, acts of any government, war or
                hostility from others, civil disorder, the elements, fire, explosion,
                power failure, equipment failure, industrial error or labor disputes, the
                inability to obtain necessary supplies and the like.</p>
            <p class="p">LIMITATION OF LIABILITY - Neither Marktech, its officers,
                directors, employees or authorized agents, will incur in any
                liability to the customer or any other person for damages
                direct, indirect or consequential damages or damages (including but not limited to
                to lost or damaged benefits or corruption of information or
                data) arising out of or in connection with the use of the service or any
                delay, failure or interruption of the service or in the use or performance of the
                software.</p>
            <p class="p">Where Marktech has consented to the use of the
                service and the customer uses the service for purposes or purposes
                other than what was intended to be used, then the client does
                use of this service at your own risk.</p>
            <p class="p">I HAVE READ AND AGREE TO THE FOLLOWING TERMS AND CONDITIONS TO CONFIRM THE ORDER.
            <h2><a id="Limitation-Of-Liability" name="Limitation-Of-Liability"></a></h2>
            <h2>&nbsp;</h2>
            <h2>Limitation of Liability</h2>
            <p class="p">Under any circumstances, including, without limitation, negligence,
                Marktech (as defined in the previous section) will be responsible
                for direct, indirect, incidental, special or consequential damages
                resulting from the use of, or the inability to use, any website of
                Marktech or the materials or features on that site, even if
                Marktech has been advised of the possibility of such damages. The law
                applicable law may not allow the exclusion or limitation of liability
                or incidental or consequential damages, so the limitation or
                above exclusion may not apply to you. In no case our
                full liability to you for all damages, losses and causes
                of action whether in contract or tort (including, without limitation,
                negligence, or otherwise) exceed the amount paid by
                you, if any, login to the Marktech website.
            <h2><a id="Competition-issues" name="Competition-issues"></a></h2>
            <h2>&nbsp;</h2>
            <h2>Competition issues “jurisdictional incidents”</h2>
            <p class="p">Unless otherwise specified, materials, in any
                Marktech website are presented solely for the purpose of
                promote hardware, computer equipment, software, peripherals,
                networks, accessories and other products and services available in Mexico.
                Marktech does not warrant that the materials on any Marktech web site
                Marktech are appropriate or available for use in any
                particular situation. Those who decide to access a website of
                Marktech do so on their own initiative and are responsible for the
                compliance with local laws, when and to the extent local laws
                local are applicable.
            <h2><a id="Termination" name="Termination"></a></h2>
            <h2>&nbsp;</h2>
            <h2>Termination</h2>
            <p class="p">These Terms of Use are effective until terminated by
                Whatever of the parts. Your access to any and all websites
                Marktech may be terminated immediately and without notice
                us in our sole discretion, if you fail to comply with any
                term of these terms of use. Upon termination, you must stop
                use the Marktech website and destroy all material
                obtained from said site as well as all copies thereof, if made under
                the terms of these Terms of Use or otherwise. You can
                terminate at any time, suspend the use of all websites
                from Marktech. Upon termination, you must destroy all
                materials obtained from any and all sites in addition to all
                related documentation and all copies and installations of the
                same, if they were made under the terms of these Terms and
                Terms of Use or otherwise.
                {{-- <h2><a id="Reclamaciones-de-derechos-de-autor" name="Reclamaciones-de-derechos-de-autor"></a></h2>
<h2>&nbsp;</h2>
<h2>Aviso y procedimientos para realizar reclamaciones de infracción de derechos de autor</h2>
<p class="p">Las notificaciones de infracción de copyright demandada deben ser
enviadas al Agente Designado del proveedor de servicios. Deberá
presentarse una notificación a la siguiente agente designada:</p>
<p class="p">Marktech S.A. de C.V.<br>
<p class="p">Para ser eficaz, la notificación debe ser una comunicación escrita que incluya lo siguiente:</p>
<ul id="list">
<li><span>Una firma física o electrónica de la persona autorizada para
actuar en nombre del titular de un derecho exclusivo que presuntamente
se ha infringido;</span></li>
<li><span>Identificación de la obra con derechos de autor la cual se
alega, han sido violados o varias obras en un solo sitio en línea están
cubiertos por una sola notificación, y una lista representativa de tales
 obras en ese sitio;</span></li>
<li><span>La identificación del material que se demanda ha sido
infringida o para ser objeto de la actividad infractora y que debe ser
eliminado o cuyo acceso debe ser inhabilitado, e información razonable y
 suficiente para permitir al proveedor del servicio localizar el
material;</span></li>
<li><span>Información razonable y suficiente para permitir al proveedor
de servicios contactar al denunciante, como una dirección, un número de
teléfono y en su caso, dirección de correo electrónico en la cual la
parte demandante puede ser contactada;</span></li>
<li><span>Una declaración de que la parte demandante tiene una creencia
de buena fe que el uso del material descrito en la reclamación no está
autorizado por el propietario de los derechos de autor, su agente o la
ley;</span></li>
<li><span>Una declaración de que la información en la notificación es
exacta y bajo pena de perjurio, de que se autorice la parte reclamante
para actuar en nombre del titular de un derecho exclusivo que
presuntamente se ha infringido.</span></li>
</ul> --}}
            <p class="p"><a href="#Terms-and-conditions-of-use"></a></p>
            <h2><a id="General-provisions-and-provisions" name="General-provisions-and-provisions"></a></h2>
            <h2>&nbsp;</h2>
            <h2>General provisions and provisions</h2>
            <p class="p">By visiting these sites, you agree that the Terms and Conditions
                of Use shall be governed by and construed in accordance with the laws of the State of
                Nuevo León, without giving effect to any principle of conflict of laws, and
                that any legal or equitable action arising out of or in connection with
                These Terms and Conditions of Use and the Privacy Notice are
                will be brought only in state or federal court and hereby agree and submit to the venue and
                personal jurisdiction of said courts for the purposes of such
                action. If any provision of these Terms and Conditions of Use
                is illegal, void or unenforceable for any reason, such provision shall be
                shall be deemed severable from these Terms and Conditions of Use and shall not
                will affect the validity and enforceability of the remaining provisions.
                These Terms and Conditions of Use constitute the entire agreement
                between us in relation to the subject mentioned here and it will not be
                modified except in writing, signed by both parties. Marktech or
                Marktech contractor employee who developed and maintains these
                websites will be considered a third-party beneficiary object of all
                rights, but not obligations, provided to Marktech under these
                terms, obligations, which by their context are not exclusive to
                marktech
            <h2><a id="Disclaimer-of-warranty" name="Disclaimer-of-warranty"></a></h2>
            <h2>&nbsp;</h2>
            <h2>Information Accuracy and Warranty Disclaimer</h2>
            <p class="p">Marktech has made every effort to display the content of the
                online store accurately, but additions, deletions and changes
                can occur without warning. The content of the online store is
                provided "as is", neither Marktech nor its representatives nor
                subsidiaries makes no representation or warranty with respect to the
                contents. In the event that the customer wishes to return a
                purchased product, it will be applied according to the Federal Law of
                Current consumer protection.<br>
            </p>
            <p class="p">Marktech, its affiliates and representatives specifically disclaim, to
                to the fullest extent permitted by law, any and all&nbsp; the
                warranties, express or implied, relating to the online store or its
                content, including but not limited to: the implied warranties of
                merchantability, completeness, timeliness, accuracy, non-existence
                infringement or adequacy.
            <p class="p">&nbsp;</p>
            <hr>
    </div>
@else
    {{-- test --}}
    <div style="text-align: center;">
        <br>
        <h1 class="titles"><a id="Terminos-y-condiciones-de-uso" name="Terminos-y-condiciones-de-uso"></a>Términos y
            condiciones de uso</h1>
    </div>
    <div>
        <p class="p">Al usar el sitio www.Marktech.com usted manifiesta su aceptación de estos Términos de Uso y
            nuestra <a href="/avisodeprivacidad"><strong>Politica de Privacidad</strong></a>.
            Si usted no acepta estos Términos y condiciones de Uso, por favor no
            utilice estos sitios. Marktech S.A. de C.V. (‘Marktech’) se
            reserva el derecho, a nuestra discreción, de cambiar, añadir, eliminar o
            modificar partes de estos Términos y condiciones de Uso en cualquier
            momento. Revise estos términos y condiciones de uso periódicamente para
            los cambios. El uso continuado de estos sitios después de la publicación
            de las modificaciones de estas condiciones de uso significa que usted
            acepta esos cambios.</p>
        <div id="numbered-list">
            <ol>
                <li>
                    <a href="#Posesión">Posesión</a>
                </li>
                <li><a href="#Licencia-y-uso-del-sitio">Licencia y Uso del Sitio</a></li>
                <li><a href="#Su-cuenta">Su cuenta</a></li>
                <li><a href="#Peticiones">Peticiones</a></li>
                <li><a href="#Contenido-vinculado-a-cualquier-sitio-web-de-Marktech">Contenido vinculado a cualquier
                        sitio web de Marktech</a></li>
                <li><a href="#Clausula-de-Renuncia">Cláusula de Renuncia “disclaimer”</a></li>
                <li><a href="#Tarjetas-de-Credito">Tarjetas de Crédito</a></li>
                <li><a href="#Indemnizacion">Indemnización</a></li>
                <li><a href="#Limitacion-De-Responsabilidad">Limitación de Responsabilidad</a></li>
                <li><a href="#Cuestiones-de-competencia">Cuestiones de competencia “incidentes jurisdiccionales”</a>
                </li>
                <li><a href="#Terminacion">Terminación</a></li>
                <li><a href="#Reclamaciones-de-derechos-de-autor">Aviso y procedimientos para realizar reclamaciones de
                        infracción de derechos de autor</a></li>
                <li><a href="#Disposiciones-y-provisiones-generales">Disposiciones y provisiones generales</a></li>
                <li><a href="#Responsabilidad-de-garantia">Exactitud de Información y Descargo de responsabilidad de
                        garantía</a></li>
            </ol>
        </div>
        <h2><a id="Posesión" name="Posesión"></a></h2>
        <h2>&nbsp;</h2>
        <h2>Posesión</h2>
        <p class="p">Todo el contenido de estos sitios, incluyendo, sin limitación, texto,
            gráficos, imágenes, logotipos, clips de audio o de vídeo, descargas
            digitales, compilaciones de datos y software, son propiedad de
            Marktech o de los proveedores de licencias de contenido y está
            protegido por las leyes de los Estados Unidos Mexicanos y otros países
            además de los tratados internacionales. La compilación de todo el
            contenido de estos sitios es propiedad de Marktech y está protegido
            por las leyes de los Estados Unidos Mexicanos y otros países además de
            los tratados internacionales. Todas las demás marcas que no son
            propiedad de Marktech que aparecen en estos sitios son propiedad de
            sus respectivos propietarios, que pueden o no estar afiliados con o
            conectados a Marktech. <a href="#Terminos-y-condiciones-de-uso"></a></p>
        <h2><a name="Licencia-y-uso-del-sitio" id="Licencia-y-uso-del-sitio"></a></h2>
        <h2>&nbsp;</h2>
        <h2>Licencia y Uso del Sitio</h2>
        <p class="p">Marktech le concede una licencia limitada para acceder y hacer uso
            personal y el uso no comercial de estos sitios. De conformidad con
            estas Condiciones de Uso, no está permitida la descarga de cualquier
            material (incluyendo, sin limitación, software, textos, gráficos u otros
            contenidos), excepto para la impresión de copias simples de las
            páginas, según sea necesario para acceder a los sitios (para uso
            personal y el uso no comercial, siempre que se mantengan todos los
            avisos de copyright y de propiedad), enlaces a cualquier página o
            modificar en todo o parte de los sitios sin el consentimiento expreso
            por escrito de Marktech. Usted no puede redistribuir, vender,
            diseminar, realizar ingeniería inversa, desmontar o reducir un software
            de forma legible que se le permite descargar información de estos
            sitios, excepto cuando lo permita la ley. Con la única excepción de lo
            expresamente previsto en el presente, estos sitios (o cualquier trabajo
            derivado de la versión de la misma), su contenido y cualquier miembro o
            información de cuenta no pueden, en cualquier forma o por cualquier
            medio ahora conocido o desarrollado hacer la reproducción, muestra,
            descarga, carga, publicados, reutilizar, desplazar, distribuir,
            transmitir, revender o explotar para ningún propósito comercial sin
            previo y expreso consentimiento por escrito de Marktech. Todos los
            derechos no concedidos expresamente a usted anteriormente, incluida la
            propiedad y título, se reservan para el propietario y no se transfiere o
            se licencia a su favor.
        <h2><a id="Su-cuenta" name="Su-cuenta"></a></h2>
        <h2>&nbsp;</h2>
        <h2>Su cuenta</h2>
        <p class="p">Si utiliza estos sitios, usted es responsable de mantener la
            confidencialidad de la afiliación y la información de la cuenta,
            información de tarjeta de crédito, nombres de usuario, contraseñas e
            identificadores que pueden ser obligados a utilizar el sitio de vez en
            cuando ( "Información de cuenta") y para restringir el acceso a su
            computador o a otros dispositivos móviles, y usted acepta que es
            responsable de toda la actividad que ocurre bajo su cuenta o con el uso
            de la información de su cuenta (incluyendo, sin limitación, los nombres
            de usuario y contraseñas). Marktech se reserva el derecho, a su sola
            discreción, denegar el acceso al sitio o los servicios prestados a
            través del mismo, cerrar cuentas y derechos de uso, editar o eliminar
            contenido o presentaciones (como se define más adelante) además de la
            cancelación de pedidos o de materiales que a través del sitio se hayan
            solicitado.
        <h2><a id="Peticiones" name="Peticiones"></a></h2>
        <h2>&nbsp;</h2>
        <h2>Peticiones</h2>
        <p class="p">Cualquier información o material que usted transmita, cargue o
            entregue a cualquier sitio de Marktech (incluyendo, sin limitación,
            comentarios, reseñas, anuncios de chat, mensajes de correo electrónico o
            de materiales dirigidos a cualquier foro, ya que el término se define
            más abajo) o cualquier sugerencia en forma creativa, ideas, notas,
            dibujos, conceptos u otra información enviada a Marktech a través de
            nuestro sitio Web u otros medios de transmisión o entrega, serán
            denominados colectivamente "Envíos". Si usted desea transmitir o
            entregar presentaciones a Marktech usted concede a Marktech una
            licencia no exclusiva, libre de regalías, perpetua, irrevocable (o el
            período más largo permitido por la ley) de licencia (con derecho a
            sub-licenciar y asignar) para utilizar, reproducir, modificar, adaptar,
            publicar, traducir, representar y exhibir públicamente, transmitir,
            crear, vender, crear obras derivadas y distribuir esos escritos o
            presentaciones como incorporar a otros trabajos en cualquier forma o
            medio y por cualquier medio o medios de distribución o tecnología
            actualmente conocida o desarrollada en el futuro. Usted acepta y
            representa a Marktech que posee o ha obtenido la propiedad
            intelectual y otros derechos necesarios en las presentaciones
            (incluyendo, sin limitación, una renuncia de los derechos aplicables en
            moral) y a conceder tal licencia a Marktech que no son tales
            presentaciones, o será, sujeto a cualquier obligación de
            confidencialidad por parte de Marktech y que Marktech no será
            responsable de cualquier uso o divulgación de cualquier presentación.
            Sin limitación de lo anterior, Marktech tendrá derecho al uso sin
            restricciones de los envíos, comercial o de otro tipo, sin compensación
            al proveedor de los envíos.
            {{-- <h2><a id="Foros-y-Comunicacion-Publica" name="Foros-y-Comunicacion-Publica"></a></h2>
<h2>&nbsp;</h2> --}}
            {{-- <h2>Foros y Comunicación Pública</h2>
<p class="p">"Foros", un área de chat, tablón de anuncios, la función de correo
electrónico u otra función que le permite transmitir o someter el
material al sitio de Marktech para su visualización, almacenamiento o
distribución, se ofrecen como parte de cualquier sitio de Marktech o
por una compañía afiliada / organización y / o proveedor de servicios
de Marktech donde el Foro está previsto en un sitio distinto de un
sitio de Marktech, usted estará obligado por los términos del
servicio y aviso de privacidad del sitio que han vinculado. Si usted
participa en cualquier Foro dentro de un sitio de Marktech, usted
debe y no acepta que no va a través del uso de las comunicaciones o de
otro modo:</p>
<ul id="list">
<li><span>Difamar, abusar, acosar o amenazar a otros;</span></li>
<li><span>Hacer cualquier burla de odio o racial que sean ofensivas las declaraciones;</span></li>
<li><span>Promover una actividad ilegal o discutir actividades ilegales con la intención de cometer los mismos;</span></li>
<li><span>Publicar o distribuir cualquier material que infrinja o viole cualquier derecho de un tercero o cualquier ley;</span></li>
<li><span>Publicar o distribuir cualquier material vulgar, obsceno, descortés, lenguaje indecente o imágenes ofensivas;</span></li>
<li><span>Anunciar, vender, o solicitar, a otros;</span></li>
<li><span>Utilizar el Foro con fines comerciales de ningún tipo;</span></li>
<li><span>Publicar o distribuir cualquier software u otros materiales que contengan un virus u otro componente dañino, o</span></li>
<li><span>Publicar material o hacer declaraciones que no corresponden en
general con el tema designado o el tema de cualquier sala de chat o
tablón de anuncios.</span></li>
</ul> --}}
        <p class="p">Además, usted debe tener en cuenta y aceptar que no usará una
            dirección de correo electrónico falsa, hacerse pasar por otra persona o
            entidad o de otra manera inducir a error a los demás como a la fuente de
            origen de una presentación. Marktech se reserva el derecho de quitar
            o editar el contenido de cualquier sitio que contenga un foro de
            Marktech en cualquier momento y por cualquier razón.</p>
        <p class="p">Todo el material de transmisión, que se envía o entrega a un Foro
            constituirá una petición y se rige por las condiciones aplicables a los
            envíos como se describe en este documento.</p>
        <p class="p">Al participar en un foro, nunca asuma que las personas son quienes
            dicen que son, saben lo que dicen que saben, o están afiliados con los
            que dicen que están afiliados a cualquier sala de chat, tablón de
            mensajes u otro usuario generado en el área de contenido. La información
            obtenida en un foro puede no ser fiable, y Marktech no se hace
            responsable por el contenido o la exactitud de la información.
        <h2><a id="Contenido-vinculado-a-cualquier-sitio-web-de-Marktech"
                name="Contenido-vinculado-a-cualquier-sitio-web-de-Marktech"></a></h2>
        <h2>&nbsp;</h2>
        <h2>Contenido vinculado a cualquier sitio web de Marktech</h2>
        <p class="p">Sea discreto al navegar por Internet utilizando cualquier sitio de
            Marktech. Usted debe ser consciente de que mientras usted está en un
            sitio de Marktech, usted puede dirigirse a otros sitios que están
            fuera de nuestro control. Hay enlaces a otros sitios de Marktech y
            sitios que lo llevan fuera de nuestro servicio. Esto incluye los enlaces
            de las secciones regionales, patrocinadores y socios que pueden
            utilizar nuestro logotipo(s) como parte de un co-branding o de otro
            acuerdo. Estos otros sitios pueden enviar sus propias cookies a los
            usuarios, obtener información, solicitar información personal, o
            contener información que usted considere inapropiada u ofensiva.
            Marktech se reserva el derecho a invalidar los enlaces de sitios de
            terceros a cualquier sitio de Marktech. Marktech no ofrece ninguna
            garantía respecto al contenido de los sitios vinculados a cualquier
            sitio de Marktech o figura en ninguno de nuestros directorios. En
            consecuencia, Marktech no se hace responsable de la exactitud,
            relevancia, cumplimiento de derechos de autor, legalidad o decencia del
            material contenido en los sitios enumerados en nuestros resultados de
            búsqueda o de otro tipo vinculado a un sitio de Marktech.
        <h2><a id="Clausula-de-Renuncia" name="Clausula-de-Renuncia"></a></h2>
        <h2>&nbsp;</h2>
        <h2>Cláusula de Renuncia “Disclaimer”</h2>
        <p class="p">Los materiales en estos sitios web de Marktech se proporcionan
            "tal cual" y sin garantía de ningún tipo, ya sea expresa o implícita. En
            la máxima medida en que la ley así lo aplique, Marktech sus
            contratistas, agentes, afiliados, socios, y destinados de beneficiarios
            de terceros (conjuntamente con las partes de "Marktech"), renuncia a
            toda garantía, expresa o implícita, incluyendo, pero no limitado a,
            garantías de comercialización y aptitud para un propósito particular.
            Marktech no garantiza que las funciones contenidas en los materiales
            del sitio web de Marktech serán ininterrumpidas o libres de errores,
            que los defectos serán corregidos, o que cualquier sitio web de
            Marktech o los servidores que hacen que dichos materiales estén
            libres de virus u otros componentes perjudiciales. Marktech no
            garantiza ni realiza ninguna representación sobre el uso o los
            resultados del uso de los materiales del sitio web de Marktech en
            cuanto a su corrección, precisión, fiabilidad u otros. Usted asume el
            costo total de cualquier servicio, reparación o corrección. La ley
            aplicable puede no permitir la exclusión de garantías implícitas, por lo
            que la exclusión anterior puede no aplicarse a usted.</p>
        <p class="p">Marktech declina expresamente cualquier responsabilidad por la
            exactitud, integridad, el contenido o la disponibilidad de la
            información contenida en los sitios que tienen enlaces hacia o desde
            cualquier sitio web de Marktech. Marktech no puede garantizar que
            usted estará satisfecho con los productos o servicios que usted compra
            en un sitio de terceros que enlace o que de cualquier sitio web de
            Marktech o el contenido de terceros en cualquier sitio web de
            Marktech. Marktech no se hace responsable de la mercancía (en su
            caso), salvo lo expresamente previsto, ni ha tomado ninguna medida para
            confirmar la exactitud o confiabilidad de ninguna de la información
            contenida en dichos sitios de terceros o de su contenido. Marktech no
            hace ninguna representación o garantía en cuanto a la seguridad de la
            información (incluyendo, sin limitación, la tarjeta de crédito y otra
            información personal) que puede ser solicitada para dar a terceros, y
            por el presente renuncia irrevocablemente a cualquier reclamación contra
            Marktech. Con respecto a tales sitios y contenidos de terceros.
            Marktech le recomienda encarecidamente que haga las investigaciones
            que considere necesarias o apropiadas antes de proceder con cualquier
            transacción en línea o sin conexión con cualquiera de estos
            terceros.
        <h2><a id="Tarjetas-de-Credito" name="Tarjetas-de-Credito"></a></h2>
        <h2>&nbsp;</h2>
        <h2>Tarjetas de Crédito</h2>
        <p class="p">Marktech puede proporcionar su número de tarjeta de crédito,
            información de facturación y envío a las instituciones financieras
            legalmente constituidas y contratados por Marktech para el
            procesamiento de pagos. De lo contrario, Marktech no comparte su
            información de tarjeta de crédito con otra persona. Para obtener más
            información, por favor lea nuestro Aviso de Privacidad.</p>
        <p class="p">Para proteger la seguridad de su información de tarjeta de crédito,
            Marktech emplea el estándar de la industria de Secure Sockets Layer
            (SSL). Marktech también encripta su número de tarjeta de crédito
            cuando Marktech almacena su solicitud y siempre que Marktech
            transmita información a los comerciantes participantes.
        <h2><a id="Indemnizacion" name="Indemnizacion"></a></h2>
        <h2>&nbsp;</h2>
        <h2>Indemnización</h2>
        <p class="p">Usted se hace enteramente responsable de mantener la confidencialidad
            y seguridad de su información de cuenta y de todas las actividades que
            ocurran bajo su cuenta. Usted acuerda indemnizar, defender y mantener a
            Marktech sus filiales y otras empresas afiliadas / organizaciones,
            contratistas, agentes, socios y patrocinadores y sus respectivos
            funcionarios, directores, voluntarios, empleados y agentes de y contra
            cualquier reclamación de terceros, demandas, acciones, pleitos,
            procedimientos, responsabilidades, daños, pérdidas, juicios y gastos
            (incluyendo pero no limitado a, los costos de cobranza, cargos de
            abogados y otros costos razonables de la defensa o la aplicación de sus
            obligaciones bajo el presente) como resultado de lo que surjan de
            cualquier violación de cualquiera de sus representaciones o mal uso de
            este o cualquier otro sitio web de Marktech o de cualquier sitio con
            enlaces a este o cualquier otro sitio web de Marktech. Usted deberá
            utilizar sus mejores esfuerzos para cooperar con nosotros en la defensa
            de cualquier demanda.
        <h2><a id="Cláusula De Indemnización" name="Cláusula De Indemnización"></a></h2>
        <h2>&nbsp;</h2>
        <h2>Cláusula de Indemnización</h2>
        <p class="p">Marktech no asume ninguna responsabilidad por el acceso no
            autorizado por el cliente o los usuarios a la información de la que
            participan redes de "hacking", o cualquier virus o programas dañinos,
            que puede ser presentado por un cliente o usuario, o por el uso de la
            información recibida a través del servicio.</p>
        <p class="p">Los Servicio de Marktech a continuación se somete a la
            interrupción y retraso debido a causas fuera de su control, tales como
            casos de fuerza mayor, actos de cualquier gobierno, la guerra o la
            hostilidad de otros, desorden civil, los elementos, fuego, explosión,
            apagón, fallo del equipo, error industrial o conflictos laborales, la
            imposibilidad de obtener los suministros necesarios y similares.</p>
        <p class="p">LIMITACIÓN DE RESPONSABILIDAD - Ni Marktech, sus oficiales,
            directores, empleados o agentes autorizados, incurrirá en ninguna
            responsabilidad para el cliente o ninguna otra persona por daños
            directos, indirectos o consecuentes o daños (incluyendo pero no limitado
            a los beneficios perdidos o dañados o corrupción de información o
            datos) que surjan de o en conexión con el uso del servicio o cualquier
            retraso, falla o interrupción del servicio o en el uso o rendimiento del
            software.</p>
        <p class="p">Donde de Marktech ha dado su consentimiento para el uso del
            servicio y el cliente utiliza el servicio con fines o propósitos
            distintos de lo que se ha pretendía utilizar, entonces el cliente hace
            uso de este servicio bajo su propio riesgo.</p>
        <p class="p">HE LEÍDO Y ESTOY DE ACUERDO CON LOS SIGUIENTES TÉRMINOS Y CONDICIONES PARA CONFIRMAR LA
            ORDEN.
        <h2><a id="Limitacion-De-Responsabilidad" name="Limitacion-De-Responsabilidad"></a></h2>
        <h2>&nbsp;</h2>
        <h2>Limitación de Responsabilidad</h2>
        <p class="p">Bajo ninguna circunstancia, incluyendo, sin limitación, negligencia,
            Marktech (como se define en la sección anterior) serán responsables
            por daños directos, indirectos, incidentales, especiales o consecuentes
            que resulten del uso de, o la incapacidad de usar cualquier sitio web de
            Marktech o los materiales o funciones en dicho sitio, incluso si
            Marktech ha sido advertido de la posibilidad de tales daños. La ley
            aplicable puede no permitir la exclusión o limitación de responsabilidad
            o de daños incidentales o consecuentes, por lo que la limitación o
            exclusión anterior puede no aplicarse a usted. En ningún caso nuestra
            responsabilidad total hacia usted por todos los daños, pérdidas y causas
            de acción ya sea en contrato o agravio (incluyendo, sin limitación,
            negligencia, o cualquier otra forma) exceder la cantidad pagada por
            usted, en su caso, de ingreso en el sitio web de Marktech.
        <h2><a id="Cuestiones-de-competencia" name="Cuestiones-de-competencia"></a></h2>
        <h2>&nbsp;</h2>
        <h2>Cuestiones de competencia “incidentes jurisdiccionales”</h2>
        <p class="p">A menos que se especifique lo contrario, los materiales, en cualquier
            sitio web de Marktech se presentan únicamente con el propósito de
            promover el hardware, equipos informáticos, software, periféricos,
            redes, accesorios y otros productos y servicios disponibles en México.
            Marktech no garantiza que los materiales en cualquier sitio web de
            Marktech sean apropiados o estén disponibles para su uso en cualquier
            situación particular. Aquellos que decidan acceder a un sitio web de
            Marktech lo hacen por su propia iniciativa y son responsables del
            cumplimiento de las leyes locales, cuando y en la medida que las leyes
            locales sean aplicables.
        <h2><a id="Terminacion" name="Terminacion"></a></h2>
        <h2>&nbsp;</h2>
        <h2>Terminación</h2>
        <p class="p">Estos Términos de Uso son efectivos hasta que sea terminado por
            cualquiera de las partes. Su acceso a todos y cada uno de los sitios web
            de Marktech podrá prescindirse de inmediato y sin previo aviso de
            nosotros a nuestra única discreción, si usted no cumple con cualquier
            término de estas condiciones de uso. Tras la terminación, debe dejar de
            utilizar el sitio web de Marktech y destruir todo el material
            obtenido de dicho sitio así como todas sus copias, si se hicieron bajo
            los términos de estas Condiciones de Uso o de otra manera. Usted puede
            terminar en cualquier momento, suspender el uso de todos los sitios web
            de Marktech. Tras la terminación, usted deberá destruir todos los
            materiales obtenidos de cualquier y todos los sitios además de toda la
            documentación relacionada y todas las copias e instalaciones de la
            misma, si se hicieron bajo los términos de las presentes Términos y
            Condiciones de Uso o de otra manera.
            {{-- <h2><a id="Reclamaciones-de-derechos-de-autor" name="Reclamaciones-de-derechos-de-autor"></a></h2>
<h2>&nbsp;</h2>
<h2>Aviso y procedimientos para realizar reclamaciones de infracción de derechos de autor</h2>
<p class="p">Las notificaciones de infracción de copyright demandada deben ser
enviadas al Agente Designado del proveedor de servicios. Deberá
presentarse una notificación a la siguiente agente designada:</p>
<p class="p">Marktech S.A. de C.V.<br>
<p class="p">Para ser eficaz, la notificación debe ser una comunicación escrita que incluya lo siguiente:</p>
<ul id="list">
<li><span>Una firma física o electrónica de la persona autorizada para
actuar en nombre del titular de un derecho exclusivo que presuntamente
se ha infringido;</span></li>
<li><span>Identificación de la obra con derechos de autor la cual se
alega, han sido violados o varias obras en un solo sitio en línea están
cubiertos por una sola notificación, y una lista representativa de tales
obras en ese sitio;</span></li>
<li><span>La identificación del material que se demanda ha sido
infringida o para ser objeto de la actividad infractora y que debe ser
eliminado o cuyo acceso debe ser inhabilitado, e información razonable y
suficiente para permitir al proveedor del servicio localizar el
material;</span></li>
<li><span>Información razonable y suficiente para permitir al proveedor
de servicios contactar al denunciante, como una dirección, un número de
teléfono y en su caso, dirección de correo electrónico en la cual la
parte demandante puede ser contactada;</span></li>
<li><span>Una declaración de que la parte demandante tiene una creencia
de buena fe que el uso del material descrito en la reclamación no está
autorizado por el propietario de los derechos de autor, su agente o la
ley;</span></li>
<li><span>Una declaración de que la información en la notificación es
exacta y bajo pena de perjurio, de que se autorice la parte reclamante
para actuar en nombre del titular de un derecho exclusivo que
presuntamente se ha infringido.</span></li>
</ul> --}}
        <p class="p"><a href="#Terminos-y-condiciones-de-uso"></a></p>
        <h2><a id="Disposiciones-y-provisiones-generales" name="Disposiciones-y-provisiones-generales"></a></h2>
        <h2>&nbsp;</h2>
        <h2>Disposiciones y provisiones generales</h2>
        <p class="p">Al visitar estos sitios, usted acepta que los Términos y condiciones
            de Uso se regirán e interpretarán de acuerdo con las leyes del Estado de
            Nuevo León, sin dar efecto a cualquier principio de conflicto de leyes, y
            que cualquier acción legal o de equidad que surja de o en relación a
            estos Términos y condiciones de Uso y el Aviso de Privacidad se
            presentará sólo en los tribunales estatales o federales y por el presente acepta y se somete a la sede y la
            jurisdicción personal de dichos tribunales a los efectos de tales
            acción. Si cualquier disposición de estas Términos y Condiciones de Uso
            es ilegal, nula o inaplicable por cualquier razón, dicha disposición se
            considerará separable de estos Términos y condiciones de Uso y no
            afectará a la validez y aplicabilidad de las disposiciones restantes.
            Estos Términos y condiciones de Uso constituyen el acuerdo completo
            entre nosotros en relación con el tema aquí mencionado y no será
            modificado excepto por escrito, firmado por ambas partes. Marktech o
            contratista de Marktech empleado que desarrolló y mantiene estos
            sitios web se considerará un objeto tercero beneficiario de todos los
            derechos, pero no de obligaciones, siempre a Marktech bajo estos
            términos, obligaciones, que por su contexto son no es exclusivo de
            Marktech
        <h2><a id="Responsabilidad-de-garantia" name="Responsabilidad-de-garantia"></a></h2>
        <h2>&nbsp;</h2>
        <h2>Exactitud de Información y Descargo de responsabilidad de garantía</h2>
        <p class="p">Marktech ha hecho todo lo posible para mostrar el contenido de la
            tienda en línea con exactitud, pero las adiciones, supresiones y cambios
            pueden ocurrir sin previo aviso. El contenido de la tienda en línea se
            proporciona “tal cual”, ni Marktech ni sus representantes ni
            subsidiarias hace ninguna representación o garantía con respecto a los
            contenidos. En caso de que el cliente desee realizar la devolución de un
            producto adquirido, la misma se aplicará de acuerdo a la Ley Federal de
            Protección al consumidor vigente.<br>
        </p>
        <p class="p">Marktech, sus filiales y representantes niegan específicamente, a
            la mayor medida permitida por ley, cualquier y todas&nbsp; las
            garantías, expresas o implícitas, relativas a la tienda en línea o su
            contenido, incluyendo pero no limitado a: las garantías implícitas de
            comerciabilidad, exhaustividad, actualidad, exactitud, la inexistencia
            de infracción o adecuación.
        <p class="p">&nbsp;</p>
        <hr>
    </div>
@endif
@endsection
