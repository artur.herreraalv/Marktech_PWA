@extends('layouts.app')

@section('content')

@section('title', 'Marktech - Sugerencias')

<h1 class="text-center titles"><strong>{{ __('Suggestions') }}</strong></h1>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-body">

                    <form action={{ route('contact') }} method="POST">
                        {{ csrf_field() }}



                        <div class="form-group">
                            <label for="name"><strong>*{{ __('Name:') }}</strong></label>
                            <input type="text" name="name" type="text" class="form-control"
                                placeholder="" required>
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="name"><strong>*{{ __('Email:') }}</strong></label>
                            <input type="email" name="mail" type="text" class="form-control"
                                placeholder="" required>
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="name"><strong>*{{ __('Message:') }}</strong></label>
                            <textarea name="msg" type="text" class="form-control" placeholder="" required></textarea>
                        </div>
                        <br>
                        <div class="form-group text-center">
                            <button type="submit" id='btn-contact' class="btn btn-black text-white">{{ __('Send') }}</button>
                        </div>


                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
<br>

@endsection
