@extends('layouts.navbar')
@section('content')



    {{-- submit search with checkbox --}}
    <script>
        $(document).ready(function() {
            $("#search-form").on("change", "input:checkbox", function() {
                $("#search-form").submit();
            });
        });
    </script>

    {{-- submit search with select --}}
    <script>
        $(document).ready(function() {
            $("#search-form").on("change", "select", function() {
                $("#search-form").submit();
            });
        });
    </script>

    <section id="filters">
        <div class="container position-relative">
            <form id="search-form" method="GET">
                <input type="text" class="form-control mx-auto" name="barra" placeholder="{{ __('Search products...') }}"
                    style="width: 300px; height: 50px"><br>

                {{-- get the value of search from the current url after barra= --}}
                <script>
                    $(document).ready(function() {
                        var url = window.location.href;
                        // get the value of search from the current url after barra= and before &
                        var barra = url.split('barra=')[1] ? url.split('barra=')[1].split('&')[0] : '';
                        if (barra) {
                            $('input[name="barra"]').val(barra);
                        }
                    });
                </script>

                <div class="position-absolute top-0 start-0">
                    {{-- sort select --}}
                    <form method="GET">
                        <h5><strong>{{ __('Sort by') }}</strong></h5>
                        <select class="form-control form-control-sm" name="sort">
                            <option name="sort" value="name">{{ __('Name') }}</option>
                            <option name="sort" value="price_asc">{{ __('Price Ascending') }}</option>
                            <option name="sort" value="price_desc">{{ __('Price Descending') }}</option>
                            <option name="sort" value="trademark_asc">{{ __('Trademark Ascending') }}</option>
                            <option name="sort" value="trademark_desc">{{ __('Trademark Descending') }}</option>
                            {{-- <option name="sort" value="stock">Descending name</option> --}}
                        </select>
                    </form>
                    <br>
                    {{-- checkboxs --}}
                    <div class="accordion accordion-flush" id="accordionExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingOne">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <strong>{{ __('Trademark') }}</strong>
                                </button>
                            </h2>
                            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne"
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    {{-- <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="trademark"
                                            value="all" id="flexCheckChecked" checked>
                                        <label class="form-check-label" for="flexCheckChecked">
                                            All
                                        </label>
                                    </div><br> --}}
                                    @foreach ($viewData['trademarks'] as $menus)
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="trademark" value="toshiba"
                                                id="flexCheckDefault">
                                            <label class="form-check-label" for="flexCheckChecked">
                                                <input class="form-check-input" type="checkbox" name="trademark"
                                                    value="{{ $menus->trademarks }}" id="flexCheckChecked">
                                                {{ $menus->trademarks }}
                                            </label>
                                        </div>
                                    @endforeach

                                    {{-- remember the checkboxs selected --}}
                                    <script>
                                        $(document).ready(function() {
                                            var url = new URL(window.location.href);
                                            var params = url.searchParams;
                                            var trademark = params.get("trademark");
                                            if (trademark != null) {
                                                var trademark = trademark.split(",");
                                                for (var i = 0; i < trademark.length; i++) {
                                                    $("input[name='trademark'][value='" + trademark[i] + "']").prop(
                                                        "checked", true);
                                                }
                                            }
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    {{-- checkboxs --}}
                    <div class="accordion accordion-flush" id="accordionExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingOne">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                    <strong>{{ __('Price') }}</strong>
                                </button>
                            </h2>
                            <div id="collapseTwo" class="accordion-collapse collapse show" aria-labelledby="headingOne"
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    {{-- <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="price"
                                            value="all" id="flexCheckChecked" checked>
                                        <label class="form-check-label" for="flexCheckChecked">
                                            All
                                        </label>
                                    </div>
                                    <br> --}}
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="price" value="1000-2000"
                                            id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckChecked">
                                            1000 - 2000
                                        </label>
                                        <br>
                                        <input class="form-check-input" type="checkbox" name="price" value="2000-3000"
                                            id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckChecked">
                                            2000 - 3000
                                        </label>
                                        <br>
                                        <input class="form-check-input" type="checkbox" name="price" value="3000-4000"
                                            id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckChecked">
                                            3000 - 4000
                                        </label>
                                        <br>
                                        <input class="form-check-input" type="checkbox" name="price" value="4000-5000"
                                            id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckChecked">
                                            4000 - 5000
                                        </label>
                                        <br>
                                        <input class="form-check-input" type="checkbox" name="price" value="5000-..."
                                            id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckChecked">
                                            5000 - ...
                                        </label>

                                    </div>

                                    {{-- remember the checkboxs selected --}}
                                    <script>
                                        $(document).ready(function() {
                                            var url = new URL(window.location.href);
                                            var params = url.searchParams;
                                            var price = params.get("price");
                                            if (price != null) {
                                                var price = price.split(",");
                                                for (var i = 0; i < price.length; i++) {
                                                    $("input[name='price'][value='" + price[i] + "']").prop("checked",
                                                        true);
                                                }
                                            }
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
            </form>
        </div>
    </section>

    <script>
        $(document).ready(function() {
            $("#search-form-mobile").on("change", "input:checkbox", function() {
                $("#search-form-mobile").submit();
            });
        });
    </script>

    {{-- submit search with select --}}
    <script>
        $(document).ready(function() {
            $("#search-form-mobile").on("change", "select", function() {
                $("#search-form-mobile").submit();
            });
        });
    </script>

    <section id="filters-mobile">
        <div class="container position-relative">
            <form id="search-form-mobile" method="GET">
                <input type="text" class="form-control mx-auto" name="barra" placeholder="{{ __('Search products...') }}"
                    style="width: 300px; height: 50px"><br>


                <div class="top-0 start-0">
                    {{-- sort select --}}
                    <form method="GET">
                        <h5><strong>{{ __('Sort by') }}</strong></h5>
                        <select class="form-control form-control-sm" name="sort">
                            <option name="sort" value="name">{{ __('Name') }}</option>
                            <option name="sort" value="price_asc">{{ __('Price Ascending') }}</option>
                            <option name="sort" value="price_desc">{{ __('Price Descending') }}</option>
                            <option name="sort" value="trademark_asc">{{ __('Trademark Ascending') }}</option>
                            <option name="sort" value="trademark_desc">{{ __('Trademark Descending') }}</option>
                            {{-- <option name="sort" value="stock">Descending name</option> --}}
                        </select>
                    </form>
                    <br>
                    {{-- checkboxs --}}
                    <div class="accordion accordion-flush" id="accordionExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingOne">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                    <strong>{{ __('Trademark') }}</strong>
                                </button>
                            </h2>
                            <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne"
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    {{-- <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="trademark"
                                            value="all" id="flexCheckChecked" checked>
                                        <label class="form-check-label" for="flexCheckChecked">
                                            All
                                        </label>
                                    </div><br> --}}
                                    @foreach ($viewData['trademarks'] as $menus)
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="trademark"
                                                value="toshiba" id="flexCheckDefault">
                                            <label class="form-check-label" for="flexCheckChecked">
                                                <input class="form-check-input" type="checkbox" name="trademark"
                                                    value="{{ $menus->trademarks }}" id="flexCheckChecked">
                                                {{ $menus->trademarks }}
                                            </label>
                                        </div>
                                    @endforeach

                                    {{-- remember the checkboxs selected --}}
                                    <script>
                                        $(document).ready(function() {
                                            var url = new URL(window.location.href);
                                            var params = url.searchParams;
                                            var trademark = params.get("trademark");
                                            if (trademark != null) {
                                                var trademark = trademark.split(",");
                                                for (var i = 0; i < trademark.length; i++) {
                                                    $("input[name='trademark'][value='" + trademark[i] + "']").prop(
                                                        "checked", true);
                                                }
                                            }
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    {{-- checkboxs --}}
                    <div class="accordion accordion-flush" id="accordionExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingTwo">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <strong>{{ __('Price') }}</strong>
                                </button>
                            </h2>
                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingOne"
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    {{-- <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="price"
                                            value="all" id="flexCheckChecked" checked>
                                        <label class="form-check-label" for="flexCheckChecked">
                                            All
                                        </label>
                                    </div>
                                    <br> --}}
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="price" value="1000-2000"
                                            id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckChecked">
                                            1000 - 2000
                                        </label>
                                        <br>
                                        <input class="form-check-input" type="checkbox" name="price" value="2000-3000"
                                            id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckChecked">
                                            2000 - 3000
                                        </label>
                                        <br>
                                        <input class="form-check-input" type="checkbox" name="price" value="3000-4000"
                                            id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckChecked">
                                            3000 - 4000
                                        </label>
                                        <br>
                                        <input class="form-check-input" type="checkbox" name="price" value="4000-5000"
                                            id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckChecked">
                                            4000 - 5000
                                        </label>
                                        <br>
                                        <input class="form-check-input" type="checkbox" name="price" value="5000-..."
                                            id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckChecked">
                                            5000 - ...
                                        </label>

                                    </div>

                                    {{-- remember the checkboxs selected --}}
                                    <script>
                                        $(document).ready(function() {
                                            var url = new URL(window.location.href);
                                            var params = url.searchParams;
                                            var price = params.get("price");
                                            if (price != null) {
                                                var price = price.split(",");
                                                for (var i = 0; i < price.length; i++) {
                                                    $("input[name='price'][value='" + price[i] + "']").prop("checked",
                                                        true);
                                                }
                                            }
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
            </form>
        </div>
        <br>
    </section>

    @if (isset($details))
        @foreach ($details as $product)
            <div class="container ">
                <div class="card mb-3 mx-auto" style="max-width: 840px;">
                    <div class="row no-gutters">

                        <div class="col-md-4">

                            <a href="{{ route('product.show', ['id' => $product->id]) }}">
                                <div class="img-card">
                                    <img src="{{ asset('/img/products/' . $product->image) }}" width="200px"
                                        height="200px" alt="imagen"
                                        class="card-img-top img-card d-inline mx-auto d-block">
                                </div>
                            </a>
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <a href="{{ route('product.show', ['id' => $product->id]) }}">
                                    <h5 class="card-title"><b>{{ $product->name }}</b></h5>
                                </a>
                                @if ($product->getPrice() == 0)
                                    <span><strong class="text-primary fs-5">{{ __('Free') }}</strong></span>
                                @elseif ($product->getDiscountedprice() > 0)
                                    <strong class="text-secondary text-decoration-line-through fs-5">
                                        <x-money class="text-decoration-line-through" amount="{{ $product->getPrice() }}"
                                            currency="MXN" convert />
                                    </strong>
                                    <strong class="text-primary fs-5">
                                        <x-money class="text-decoration-line-through"
                                            amount="{{ $product->getPrice() - $product->getDiscountedprice() }}"
                                            currency="MXN" convert />
                                    </strong>
                                @else
                                    <strong class="text-primary fs-5">
                                        <x-money class="text-decoration-line-through" amount="{{ $product->getPrice() }}"
                                            currency="MXN" convert />
                                    </strong>
                                @endif
                                <br>
                                <br>
                                @if ($product->getStock() > 0)
                                    <span class="badge bg-primary text-white fs-6"><span class="iconify"
                                            data-icon="akar-icons:check"></span>
                                        {{ __('In stock') }}</span>
                                    {{-- // add to cart --}}
                                    <form action="{{ route('cart.add', ['id' => $product->getId()]) }}" method="POST">
                                        @csrf
                                        <br>
                                        <input type="hidden" name="quantity" value="1">
                                        <button type="submit" class="hov btn btn-primary btn-sm fs-6">
                                            <span class="iconify" data-icon="mi:shopping-cart-add"></span>
                                            <strong class="titledark">{{ __('Add to cart') }}</strong>
                                        </button>
                                    </form>
                                @else
                                    <span class="badge bg-secondary text-white fs-6"><span class="iconify"
                                            data-icon="bi:x-lg"></span>

                                        {{ __('Out of stock') }}</span>
                                @endif

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        @endforeach
    @endif

    {{-- message no products found --}}
    <div class="container">
        <div class="alert alert-info mx-auto" role="alert" style="width:40%;">
            {{ __('No products found') }}
        </div>

    @endsection

    {{-- <div class="container-sm">
        <section>
            <!--footer-->
            <footer class="mt-auto bg-black text-center text-white">

                <div class="container p-4">

                    <!--Links-->
                    <section class="">

                        <div class="row">

                            <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                                <h5 class="text-uppercase">Contacto</h5>

                                <ul class="list-unstyled mb-0">
                                    <li>
                                        <a href="mailto:herrera.alvaradoartu@gmail.com"
                                            class="text-white">Contactanos</a>
                                    </li>
                                    <li>
                                        <a href="/Sugerencias" class="text-white">Sugerencias</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                                <h5 class="text-uppercase">Nuestras Redes</h5>

                                <ul class="list-unstyled mb-0">
                                    <li>
                                        <a href="https://www.facebook.com/Mark-Tech-100458546063140"><i
                                                class="icon iconify text-white" data-icon="ion-social-facebook"
                                                data-width="24"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="icon iconify text-white"
                                                data-icon="ion-social-twitter" data-width="24"></i></a>
                                    </li>
                                    <li>
                                        <a href="https://instagram.com/marktech2022"><i
                                                class="icon iconify text-white" data-icon="ion-social-instagram"
                                                data-width="24"></i></a>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                                <h5 class="text-uppercase">Marktech</h5>

                                <ul class="list-unstyled mb-0">
                                    <li>
                                        <a href="/avisodeprivacidad" class="text-white">Politica de Privacidad</a>
                                    </li>
                                    <li>
                                        <a href="/terminosycondiciones" class="text-white">Terminos y
                                            Condiciones</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                                <img src="{{ asset('img/paypalcheckout.png') }}" alt="logo" class="img-fluid">
                            </div>



                        </div>

                    </section>
                </div>

                <!--Derechos-->
                <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
                    Todos los derechos reservados 2022 ©:
                    <a class="text-white" href="/">https://Marktech.com/</a>
                </div>
            </footer>
        </section>
        <!--footer-->
    </div>
</body>

</html> --}}
