@extends('layouts.app')
@section('content')


    <div class="card">
        <div class="card-body">
            <table class="table table-borderless table-striped text-center">
                <thead>
                    <tr>
                        <th scope="col">{{ __('Address') }}</th>
                        <th scope="col">{{ __('Edit') }}</th>
                        <th scope="col">{{ __('Delete') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($viewData['products'] as $product)
                        <tr>
                            <td><strong>{{ __('Name') }}:</strong> {{ $product->nombre }}, <strong>{{ __('Street') }}:</strong>
                                {{ $product->calle }} Nº
                                {{ $product->exterior }},
                                <strong>{{ __('Colony') }}:</strong> {{ $product->colonia }}, <strong>{{ __('State') }}:</strong>
                                {{ $product->estado }}
                            </td>
                            <td>
                                <a class="btn btn-black"
                                    href="{{ route('myaccount.myaddress.edit', ['id' => $product->hashid()]) }}">
                                    <span class="iconify" data-icon="clarity:note-edit-solid"></span></i>
                                </a>
                            </td>
                            <td>
                                <form action="{{ route('myaccount.myaddress.delete', $product->getId()) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger">
                                        <span class="iconify" data-icon="fluent:delete-24-filled"></span>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <a class="btn btn-black" href="/direcciones">{{ __('Add Address') }}</a>
    </div>
@endsection
