{{-- stripe logic --}}
<?php
require_once __DIR__ . '/../../../vendor/autoload.php';

$url = 'https://' . $_SERVER['SERVER_NAME'];
// This is your test secret API key.
\Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
$session = \Stripe\Checkout\Session::create([
    'line_items' => [
        [
            'price_data' => [
                'currency' => 'mxn',
                'product_data' => [
                    'name' => 'Marktech: Pedido ' . $viewData['orders']->hashid(),
                ],
                'unit_amount' => $viewData['orders']->getTotal() * 100,
            ],
            'quantity' => 1,
        ],
    ],
    'mode' => 'payment',
    // send success url with order id of current url
    'success_url' => $url . '/stripe/success/' . $viewData['orders']->getId(),
    'cancel_url' => $url . '/stripe/cancel/',
]);
?>

@extends('layouts.app')
@section('title', $viewData['title'])
@section('subtitle', $viewData['subtitle'])
@section('content')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <div class="card mb-3">
        <div class="row g-0">
            <div class="col-md-0 mx-auto">
                <div class="card-body">
                    <b class="fs-5">{{ __('Date of Order') }}:</b>
                    <a
                        class="fs-5">{{ Carbon\Carbon::parse($viewData['orders']->getCreatedat())->subHours(3)->subMinutes(2) }}</a><br>
                    <b class="fs-5">{{ __('Order') }}:</b> <a class="fs-5">{{ $viewData['orders']->hashid() }}</a><br>
                    <b class="fs-5">{{ __('Total') }}: </b>
                    <a class="fs-5">
                        <x-money amount="{{ $viewData['orders']->getTotal() }}" currency="MXN" convert /><br />

                        @if ($viewData['orders']->getEstado() == 'No pagado')
                            <b class="fs-5">{{ __('Status') }}:</b> <a class="fs-5"><span
                                    class="text-warning">{{ $viewData['orders']->getState() }}</span></a><br />
                        @elseif ($viewData['orders']->getEstado() == 'Pagado')
                            <b class="fs-5">{{ __('Status') }}:</b> <a class="fs-5"><span
                                    class="text-success">{{ $viewData['orders']->getState() }}</span></a><br />
                        @elseif ($viewData['orders']->getEstado() == 'Cancelado')
                            <b class="fs-5">{{ __('Status') }}:</b> <a class="fs-5"><span
                                    class="text-danger">{{ $viewData['orders']->getState() }}</span></a><br />
                        @endif
                        {{-- @if ($viewData['orders']->getEstado() == 'Preparing Order' && $viewData['orders']->getState() == 'Pagado')
                    <b>Estado de envio:</b> <span class="text-warning">{{ $viewData['orders']->getEstado() }}</span><br />
                @elseif ($viewData['orders']->getEstado() == 'Sent to')
                    <b>Estado de envio:</b> <span class="text-success">{{ $viewData['orders']->getEstado() }}</span><br />
                @elseif ($viewData['orders']->getEstado() == 'Sent to')
                    <b>Estado de envio:</b> <span class="text-success">{{ $viewData['orders']->getEstado() }}</span><br />
                @endif --}}
                        <br>
                        @if ($viewData['orders']->getTrackingNumber() != null &&
                            $viewData['orders']->getState() == 'Pagado' &&
                            $viewData['orders']->getPaqueteria() == 'DHL')
                            <b class="fs-5">{{ __('Tracking Number') }}:</b>
                            <a class="text-success fs-5"
                                href="https://www.dhl.com/mx-es/home/tracking/tracking-express.html?submit=1&tracking-id={{ $viewData['orders']->getTrackingNumber() }}">{{ $viewData['orders']->getTrackingNumber() }}</a><br />
                            <b class="fs-5">{{ __('Shipping Company') }}:</b>
                            <a class="text-success fs-5">{{ $viewData['orders']->getPaqueteria() }}</a><br />
                            <b class="fs-5">{{ __('Estimated Delivery Date') }}:</b> <a
                                class="text-success fs-5">{{ __('Between the') }}
                                {{ Carbon\Carbon::parse($viewData['orders']->getPaidAt())->subHours(3)->subMinutes(2)->addDays(2) }}
                                {{ __('and') }}
                                {{ Carbon\Carbon::parse($viewData['orders']->getPaidAt())->subHours(3)->subMinutes(2)->addDays(7) }}</a><br />
                        @elseif ($viewData['orders']->getTrackingNumber() != null &&
                            $viewData['orders']->getState() == 'Pagado' &&
                            $viewData['orders']->getPaqueteria() == 'Estafeta')
                            <b class="fs-5">{{ __('Tracking Number') }}:</b>
                            <a class="text-success fs-5"
                                href="https://www.estafeta.com/track/track.aspx?tracking={{ $viewData['orders']->getTrackingNumber() }}">{{ $viewData['orders']->getTrackingNumber() }}</a><br />
                            <b class="fs-5">{{ __('Shipping Company') }}:</b>
                            <a class="text-success fs-5">{{ $viewData['orders']->getPaqueteria() }}</a><br />
                            <b class="fs-5">{{ __('Estimated Delivery Date') }}:</b> <a
                                class="text-success fs-5">{{ __('Between the') }}
                                {{ Carbon\Carbon::parse($viewData['orders']->getPaidAt())->subHours(3)->subMinutes(2)->addDays(2) }}
                                {{ __('and') }}
                                {{ Carbon\Carbon::parse($viewData['orders']->getPaidAt())->subHours(3)->subMinutes(2)->addDays(7) }}</a><br />
                        @elseif ($viewData['orders']->getTrackingNumber() != null &&
                            $viewData['orders']->getState() == 'Pagado' &&
                            $viewData['orders']->getPaqueteria() == 'Fedex')
                            <b class="fs-5">{{ __('Tracking Number') }}:</b> <a class="text-success fs-5"
                                href="https://www.fedex.com/apps/fedextrack/?action=track&trackingnumber={{ $viewData['orders']->getTrackingNumber() }}">{{ $viewData['orders']->getTrackingNumber() }}</a><br />
                            <b class="fs-5">{{ __('Shipping Company') }}:</b> <a
                                class="text-success fs-5">{{ $viewData['orders']->getPaqueteria() }}</a><br />
                            <b class="fs-5">{{ __('Estimated Delivery Date') }}:</b> <a
                                class="text-success fs-5">{{ __('Between the') }}
                                {{ Carbon\Carbon::parse($viewData['orders']->getPaidAt())->subHours(3)->subMinutes(2)->addDays(2) }}
                                {{ __('and') }}
                                {{ Carbon\Carbon::parse($viewData['orders']->getPaidAt())->subHours(3)->subMinutes(2)->addDays(7) }}</a><br />
                        @elseif ($viewData['orders']->getTrackingNumber() != null &&
                            $viewData['orders']->getState() == 'Pagado' &&
                            $viewData['orders']->getPaqueteria() == 'IMILE')
                            <b class="fs-5">{{ __('Tracking Number') }}:</b> <a class="text-success fs-5"
                                href="https://www.imile.com.mx/track?tracking={{ $viewData['orders']->getTrackingNumber() }}">{{ $viewData['orders']->getTrackingNumber() }}</a><br />
                            <b class="fs-5">{{ __('Shipping Company') }}:</b> <a
                                class="text-success fs-5">{{ $viewData['orders']->getPaqueteria() }}</a><br />
                            <b class="fs-5">{{ __('Estimated Delivery Date') }}:</b> <a
                                class="text-success fs-5">{{ __('Between the') }}
                                {{ Carbon\Carbon::parse($viewData['orders']->getPaidAt())->subHours(3)->subMinutes(2)->addDays(2) }}
                                {{ __('and') }}
                                {{ Carbon\Carbon::parse($viewData['orders']->getPaidAt())->subHours(3)->subMinutes(2)->addDays(7) }}</a><br />
                        @elseif ($viewData['orders']->getTrackingNumber() == null && $viewData['orders']->getState() == 'Pagado')
                            <b class="fs-5">{{ __('Tracking Number') }}:</b> <span class="text-warning fs-5">
                                {{ __('Not available yet') }}
                            </span><br />
                        @endif
                        <br>
                        <b class="fs-5">{{ __('Shipping address:') }}</b>
                        <a class="fs-5">{{ $viewData['orders']->getAddress() }}</a><br>

                        <div class="hide-mobile">
                            <table class="table table-borderless table-striped text-center mt-3">
                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">{{ __('Name') }}</th>
                                        <th scope="col">{{ __('Unique ID') }}</th>
                                        <th scope="col">{{ __('Price') }}</th>
                                        <th scope="col">{{ __('Discount') }}</th>
                                        <th scope="col">{{ __('Quantity') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($viewData['orders']->getItems() as $item)
                                        <tr>
                                            <td>
                                                <img src="{{ asset('/img/products/' . $item->getProduct()->getImage()) }}"
                                                    alt="{{ $item->getProduct()->getName() }}" class="img-fluid"
                                                    width="100">
                                            </td>
                                            <td>
                                                <a class="link-success"
                                                    href="{{ route('product.show', ['id' => $item->getProduct()->getId()]) }}">
                                                    {{ $item->getProduct()->getName() }}
                                                </a>
                                            </td>
                                            <td>{{ $item->getId() }}</td>
                                            <td>
                                                <x-money amount="{{ $item->getPrice() }}" currency="MXN" convert />
                                            </td>
                                            @if ([$item->getDiscountedprice()] > 0)
                                                <td class="text-decoration-line-through">-
                                                    <x-money amount="{{ $item->getDiscountedprice() }}" currency="MXN"
                                                        convert />
                                                </td>
                                            @else
                                                <td></td>
                                            @endif
                                            <td>{{ $item->getQuantity() }}</td>

                                            @if ($viewData['orders']->status == 'Entregado')
                                                <td colspan="2"><a type="button" class="btn btn-dark"
                                                        href="{{ route('myaccount.orders.comment', ['id' => $item->getProduct()->getId()]) }}">
                                                        {{ __('Review Product') }}
                                                    </a></td>
                                            @endif
                                    @endforeach

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="hide-desktop">
                            <div class="hide-desktop">
                                <table class="table text-center">
                                    <thead>
                                    </thead>
                                    <tbody>
                                        @foreach ($viewData['orders']->getItems() as $item)
                                            {{-- @foreach ($viewData['comments'] as $comments) --}}
                                            <tr>
                                                <th scope="row"></th>
                                                <td>
                                                    <img src="{{ asset('/img/products/' . $item->image) }}"
                                                        alt="{{ $item->getProduct()->getName() }}" class="img-fluid"
                                                        width="100">
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">{{ __('Name') }}</th>
                                                <td>{{ $item->getProduct()->getName() }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">{{ __('Unique ID') }}</th>
                                                <td>{{ $item->getId() }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">{{ __('Price') }}</th>
                                                <td>
                                                    <x-money amount="{{ $item->getPrice() - $item->getDiscountedprice() }}"
                                                        currency="MXN" convert />
                                                </td>
                                            </tr>
                                            @if ($item->getDiscountedprice() > 0)
                                                <tr>
                                                    <th scope="row">{{ __('Discount') }}</th>

                                                    <td class="text-decoration-line-through">-
                                                        <x-money amount="{{ $item->getDiscountedprice() }}" currency="MXN"
                                                            convert />
                                                    </td>

                                                </tr>
                                            @else
                                            @endif
                                            <tr>
                                                <th scope="row">{{ __('Quantity') }}</th>
                                                <td>{{ $item->getQuantity() }}</td>
                                            </tr>
                                            @if ($viewData['orders']->status == 'Enviado')
                                                <td colspan="2"><a type="button" class="btn btn-dark"
                                                        href="{{ route('myaccount.orders.comment', ['id' => $item->getProduct()->getId()]) }}">
                                                        {{ __('Review Product') }}
                                                    </a></td>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <br>
                        @if ($viewData['orders']->getState() == 'No Pagado')
                            <p class="text-center fs-3"><strong>{{ __('Choose a payment method') }}:<strong></p>
                            <div class="accordion" id="accordionExample">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingOne">
                                        <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#collapseOne" aria-expanded="true"
                                            aria-controls="collapseOne">
                                            <span class="iconify" data-icon="logos:stripe" data-width="48"
                                                style="margin-right:18px"></span>
                                            <strong>{{ __('Credit or debit card (Stripe)') }}</strong>
                                        </button>
                                    </h2>
                                    <div id="collapseOne" class="accordion-collapse collapse show"
                                        aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            @csrf
                                            <input type="hidden" name="payment_method" value="stripe" />
                                            <button class="btn btn-black" id="checkout-button"
                                                type="submit">{{ __('Go to Stripe Checkout') }}</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingTwo">
                                        <button class="accordion-button collapsed" type="button"
                                            data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false"
                                            aria-controls="collapseTwo">
                                            <span class="iconify" data-icon="logos:paypal" data-width="32"
                                                style="margin-right:18px"></span>
                                            <strong>{{ __('PayPal') }}</strong>
                                        </button>
                                    </h2>
                                    <div id="collapseTwo" class="accordion-collapse collapse"
                                        aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <form
                                                action="{{ route('complete.order', ['id' => $viewData['orders']->getId()]) }}"
                                                method="POST">
                                                @csrf
                                                <input type="hidden" name="payment_method" value="paypal" />
                                                <input type="hidden" name="id"
                                                    value="{{ $viewData['orders']->getId() }}" />
                                                <input type="hidden" name="total"
                                                    value="{{ $viewData['orders']->getTotal() }}" />
                                                <button type="submit"
                                                    class="btn btn-black">{{ __('Go to Paypal
                                                                                                        Checkout') }}</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <br>
                        @if ($viewData['orders']->getState() == 'No Pagado')
                            <a href="{{ route('myaccount.orders.show.cancel', ['id' => $viewData['orders']->getId()]) }}"
                                class="btn btn-danger">{{ __('Cancel Order') }}</a>
                        @endif
                </div>
            </div>
        </div>
    </div>
    </div>

    <script src="https://js.stripe.com/v3/"></script>
    <script>
        const stripe = Stripe('{{ env('STRIPE_KEY') }}') //Your Publishable key.

        const btn = document.getElementById('checkout-button');
        btn.addEventListener("click", function() {
            stripe.redirectToCheckout({
                sessionId: "<?php echo $session->id; ?> "
            })
        });
    </script>

@endsection
