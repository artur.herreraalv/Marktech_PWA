@extends('layouts.app')
@section('content')
    @if ($viewData['comments'] == 'Si')
        <tr>
            <td colspan="2">
                <div class="alert alert-info" role="alert">
                    {{ __('You have already rated this product') }}
                </div>
            </td>
        </tr>
    @else
        {{-- comentar --}}
        <form method="POST">
            <div class="row">
                <div class="col-md-8 mb-4 mx-auto">
                    <div class="card mb-4">
                        <div class="card-header py-3 mx-auto">
                            <h5 class="mb-0"><strong>{{ __('Add a review') }}</strong></h5>
                        </div>
                        <div class="card-body">
                            @csrf
                            <form>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="form-outline">
                                            <label class="form-label" for="names"> {{ __('Comment') }} </label>
                                            <input type="text" name="comment" placeholder="" class="form-control"
                                                required>
                                        </div>
                                        <br>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="stars"
                                                id="flexRadioDefault1" value="1">
                                            <label class="form-check-label" for="flexRadioDefault1">
                                                <span class="iconify" data-icon="emojione:star"
                                                    style="font-size: 24px;"></span>
                                                <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                    style="font-size: 24px;"></span>
                                                <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                    style="font-size: 24px;"></span>
                                                <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                    style="font-size: 24px;"></span>
                                                <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                    style="font-size: 24px;"></span>
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="stars"
                                                id="flexRadioDefault2" value="2">
                                            <label class="form-check-label" for="flexRadioDefault2">
                                                <span class="iconify" data-icon="emojione:star"
                                                    style="font-size: 24px;"></span>
                                                <span class="iconify" data-icon="emojione:star"
                                                    style="font-size: 24px;"></span>
                                                <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                    style="font-size: 24px;"></span>
                                                <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                    style="font-size: 24px;"></span>
                                                <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                    style="font-size: 24px;"></span>
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="stars"
                                                id="flexRadioDefault3" value="3">
                                            <label class="form-check-label" for="flexRadioDefault3">
                                                <span class="iconify" data-icon="emojione:star"
                                                    style="font-size: 24px;"></span>
                                                <span class="iconify" data-icon="emojione:star"
                                                    style="font-size: 24px;"></span>
                                                <span class="iconify" data-icon="emojione:star"
                                                    style="font-size: 24px;"></span>
                                                <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                    style="font-size: 24px;"></span>
                                                <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                    style="font-size: 24px;"></span>
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="stars"
                                                id="flexRadioDefault4" value="4" checked>
                                            <label class="form-check-label" for="flexRadioDefault4">
                                                <span class="iconify" data-icon="emojione:star"
                                                    style="font-size: 24px;"></span>
                                                <span class="iconify" data-icon="emojione:star"
                                                    style="font-size: 24px;"></span>
                                                <span class="iconify" data-icon="emojione:star"
                                                    style="font-size: 24px;"></span>
                                                <span class="iconify" data-icon="emojione:star"
                                                    style="font-size: 24px;"></span>
                                                <span class="iconify" data-icon="fluent-mdl2:favorite-star"
                                                    style="font-size: 24px;"></span>
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="stars"
                                                id="flexRadioDefault5" value="5">
                                            <label class="form-check-label" for="flexRadioDefault5">
                                                <span class="iconify" data-icon="emojione:star"
                                                    style="font-size: 24px;"></span>
                                                <span class="iconify" data-icon="emojione:star"
                                                    style="font-size: 24px;"></span>
                                                <span class="iconify" data-icon="emojione:star"
                                                    style="font-size: 24px;"></span>
                                                <span class="iconify" data-icon="emojione:star"
                                                    style="font-size: 24px;"></span>
                                                <span class="iconify" data-icon="emojione:star"
                                                    style="font-size: 24px;"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <button type="submit" button class="btn btn-black">{{ __('Save') }}</button>
                </div>

            </div>

        </form>
    @endif
@endsection
