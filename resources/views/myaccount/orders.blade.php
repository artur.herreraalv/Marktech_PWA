@extends('layouts.app')
@section('title', $viewData['title'])
@section('subtitle', $viewData['subtitle'])
@section('content')

    <div class="hide-mobile">
        {!! $viewData['orders']->withQueryString()->links('layouts.pagination') !!}
    </div>

    <div class="hide-desktop mobile-pagination">
        {!! $viewData['orders']->withQueryString()->links('layouts.paginationm') !!}
    </div>

    @forelse ($viewData["orders"] as $order)
        <div class="card mb-4">
            <div class="card-header">
                <strong
                    class="text-primary">{{ Carbon\Carbon::parse($order->getCreatedat())->subHours(3)->subMinutes(2) }}</strong>
            </div>
            <div class="card-body">
                <b>{{ __('Order') }}:
                </b> {{ $order->hashid() }}<br>
                <b>{{ __('Total') }}:
                </b>
                <x-money amount="{{ $order->getTotal() }}" currency="MXN" convert /><br />

                @if ($order->getState() == 'No Pagado')
                    <b>{{ __('Status') }}:</b> <span class="text-warning">{{ __('Waiting for payment') }}</span><br />
                @elseif ($order->getState() == 'Pagado')
                    <b>{{ __('Status') }}:</b> <span class="text-success">{{ __('Paid out') }}</span><br />
                @elseif ($order->getState() == 'Cancelado')
                    <b>{{ __('Status') }}:</b> <span class="text-danger">{{ __('Canceled') }}</span><br />
                @endif
                {{-- @if ($order->getEstado() == 'Preparando Pedido' && $order->getState() == 'Pagado')
                    <b>Estado de envio:</b> <span class="text-warning">{{ $order->getEstado() }}</span><br />
                @elseif ($order->getEstado() == 'Enviado')
                    <b>Estado de envio:</b> <span class="text-success">{{ $order->getEstado() }}</span><br />
                @elseif ($order->getEstado() == 'Entregado')
                    <b>Estado de envio:</b> <span class="text-success">{{ $order->getEstado() }}</span><br />
                @endif --}}
                @if ($order->getTrackingNumber() != null && $order->getState() == 'Pagado' && $order->getPaqueteria() == 'DHL')
                    <b>{{ __('Tracking Number') }}:</b> <a class="text-success"
                        href="https://www.dhl.com/mx-es/home/tracking/tracking-express.html?submit=1&tracking-id={{ $order->getTrackingNumber() }}">{{ $order->getTrackingNumber() }}</a><br />
                @elseif ($order->getTrackingNumber() != null &&
                    $order->getState() == 'Pagado' &&
                    $order->getPaqueteria() == 'Estafeta')
                    <b>{{ __('Tracking Number') }}:</b> <a class="text-success"
                        href="https://www.estafeta.com/track/track.aspx?tracking={{ $order->getTrackingNumber() }}">{{ $order->getTrackingNumber() }}</a><br />
                @elseif ($order->getTrackingNumber() != null && $order->getState() == 'Pagado' && $order->getPaqueteria() == 'Fedex')
                    <b>{{ __('Tracking Number') }}:</b> <a class="text-success"
                        href="https://www.fedex.com/apps/fedextrack/?action=track&trackingnumber={{ $order->getTrackingNumber() }}">{{ $order->getTrackingNumber() }}</a><br />
                @elseif ($order->getTrackingNumber() != null && $order->getState() == 'Pagado' && $order->getPaqueteria() == 'IMILE')
                    <b>{{ __('Tracking Number') }}:</b> <a class="text-success"
                        href="https://www.imile.com.mx/track?tracking={{ $order->getTrackingNumber() }}">{{ $order->getTrackingNumber() }}</a><br />
                @elseif ($order->getTrackingNumber() == null && $order->getState() == 'Pagado')
                    <b>{{ __('Tracking Number') }}:</b> <span class="text-warning">{{ __('Not available') }}</span><br />
                @endif

                {{-- @if ($order->getState() == 'No Pagado' && $order->getId() == $viewData['last_order']) --}}
                <br>
                @if ($order->getState() == 'No Pagado')
                    <b>{{ __('View order details to complete your purchase.') }}</b> <br>
                @endif
                <a href="{{ route('myaccount.orders.show', $order->hashid()) }}"
                    class="btn btn-black">{{ __('See details') }}</a>

                <div class="hide-mobile">
                    <table class="table table-borderless table-striped text-center
                    mt-3">
                        <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">{{ __('Name') }}</th>
                                <th scope="col">{{ __('Unique ID') }}</th>
                                <th scope="col">{{ __('Price') }}</th>
                                <th scope="col">{{ __('Discount') }}</th>
                                <th scope="col">{{ __('Amount') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($order->getItems() as $item)
                                <tr>
                                    <td>
                                        <img src="{{ asset('/img/products/' . $item->getProduct()->getImage()) }}"
                                            alt="{{ $item->getProduct()->getName() }}" class="img-fluid" width="100">
                                    </td>
                                    <td>
                                        <a class="link-primary"
                                            href="{{ route('product.show', ['id' => $item->getProduct()->getId()]) }}">
                                            {{ $item->getProduct()->getName() }}
                                        </a>
                                    </td>
                                    <td>{{ $item->getId() }}</td>
                                    <td>
                                        <x-money amount="{{ $item->getPrice() }}" currency="MXN" convert />
                                    </td>
                                    @if ([$item->getDiscountedprice()] > 0)
                                        <td class="text-decoration-line-through">-
                                            <x-money amount="{{ $item->getDiscountedprice() }}" currency="MXN" convert />
                                        </td>
                                    @else
                                        <td></td>
                                    @endif
                                    <td>{{ $item->getQuantity() }}</td>

                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="hide-desktop">
                    <table class="table table-borderless table-striped text-center
                    mt-3">
                        <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">{{ __('Name') }}</th>
                                <th scope="col">{{ __('Price') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($order->getItems() as $item)
                                <tr>
                                    <td>
                                        <img src="{{ asset('/img/products/' . $item->getProduct()->getImage()) }}"
                                            alt="{{ $item->getProduct()->getName() }}" class="img-fluid" width="100">
                                    </td>
                                    <td>
                                        <a class="link-info"
                                            href="{{ route('product.show', ['id' => $item->getProduct()->getId()]) }}">
                                            {{ $item->getProduct()->getName() }}
                                        </a>
                                    </td>
                                    <td>
                                        <x-money amount="{{ $item->getPrice() }}" currency="MXN" convert />
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @empty
        <div class="alert alert-danger" role="alert">
            {{ __('You have no orders yet.') }}
        </div>
    @endforelse

    <div class="hide-mobile">
        <div class="hide-mobile">
            {!! $viewData['orders']->withQueryString()->links('layouts.pagination') !!}
        </div>

        <div class="hide-desktop mobile-pagination">
            {!! $viewData['orders']->withQueryString()->links('layouts.paginationm') !!}
        </div>
    </div>

    <div class="hide-desktop mobile-pagination">
        {!! $viewData['orders']->withQueryString()->links('layouts.paginationm') !!}
    </div>
@endsection
