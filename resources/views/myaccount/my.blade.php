@extends('layouts.app')
@section('content')
    {{-- script autorun button click --}}
    <script>
        $(document).ready(function() {
            $('#allow-notification').click();
        });
    </script>

    {{-- // show loading-animation when loading products from database --}}
    {{-- <script>
        $(document).ready(function() {
            if ($(window).width() < 992) {
                // hide all hide-desktop elements
                $('.products-loaded').hide();
                $('.loading-animation').ajaxStart(function() {
                    $(this).show();
                })
                // check if all ajax requests are finished
                var checkAjax = setInterval(function() {
                    if ($.active == 0) {
                        $('.loading-animation').hide();
                        $('.products-loaded').show();
                        clearInterval(checkAjax);
                    } else { // if not finished, check again in 100ms
                        $('.loading-animation').show();
                        $('.products-loaded').hide();
                    }
                }, 1000);
            }
        });
    </script> --}}

    <div class="container products-loaded">
        <div class="col">
            <div class="p-3 border-top border-bottom bg-light">
                <div class="dark-hide"><img src="{{ asset('img/userblack.png') }}" alt="Logo"
                        class="rounded mx-auto d-block" /></div>
                <div class="light-hide"><img src="{{ asset('img/userwhite.png') }}" alt="Logo"
                        class="rounded mx-auto d-block" /></div>
                <h1 class="h3 text-center titles">{{ $viewData['user'] }}</h1>
            </div>
        </div>
        <br>
        <div class="hide-mobile">
            <div class="container">
                <div class="col test">
                    <div class="p-3 border-top border-bottom bg-light"><strong>{{ __('My account') }}</strong></div>
                    <div class="p-3 border-top border-bottom bg-light">{{ __('Modify your user data.') }}
                        <a
                            class="btn btn-link float-end" href="/miusuario" role="button">></a></div>
                </div>
            </div>
            <br>
            <div class="container">
                <div class="col">
                    <div class="p-3 border-top border-bottom bg-light"><strong>{{ __('Address') }}</strong></div>
                    <div class="p-3 border-top border-bottom bg-light">{{ __('Modify or add your address.') }}
                        <a
                            class="btn btn-link float-end" href="/misdatos" role="button">></a></div>
                </div>
            </div>
            <br>
            <div class="container">
                <div class="col">
                    <div class="p-3 border-top border-bottom bg-light"><strong>{{ __('Orders') }}</strong></div>
                    <div class="p-3 border-top border-bottom bg-light">{{ __('See your orders.') }}
                        <a
                            class="btn btn-link float-end" href="/pedidos" role="button">></a></div>
                </div>
            </div>
            <br>
            {{-- Notifications --}}
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-0">
                        <button onclick="startFCM()" class="btn btn-outline-primary" id="allow-notification">
                            {{ __('Allow notifications') }}</button>
                    </div>
                </div>
            </div>
            {{-- Notifications/ --}}
        </div>
        <div class="hide-desktop">
            <div class="list-group">
                <a href="/miusuario" class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1"><span class="iconify" data-icon="carbon:edit" style="font-size: 32px;"></span>
                        </h5>
                        <small class="text-muted"><span class="iconify" data-icon="bi:arrow-right-circle"
                                style="font-size: 32px;"></span></small>
                    </div>
                    <p class="mb-1">{{ __('My account') }}</p>
                    <small class="text-muted">{{ __('Modify your user data.') }}</small>
                </a>
                <a href="/misdatos" class="list-group-item list-group-item-action" aria-current="true">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1"><span class="iconify" data-icon="carbon:delivery-truck"
                                style="font-size: 32px;"></span></h5>
                        <small class="text-muted"><span class="iconify" data-icon="bi:arrow-right-circle"
                                style="font-size: 32px;"></span></small>
                    </div>
                    <p class="mb-1">{{ __('Address') }}</p>
                    <small class="text-muted">{{ __('Modify or add your address.') }}</small>
                </a>
                <a href="/pedidos" class="list-group-item list-group-item-action" aria-current="true">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1"><span class="iconify" data-icon="bx:package" style="font-size: 32px;"></span>
                        </h5>
                        <small class="text-muted"><span class="iconify" data-icon="bi:arrow-right-circle"
                                style="font-size: 32px;"></span></small>
                    </div>
                    <p class="mb-1">{{ __('Orders') }}</p>
                    <small class="text-muted">{{ __('See your orders.') }}</small>
                </a>
            </div>
            <br>
            <form id="logout" action="{{ route('logout') }}" method="POST">
                <a role="button" class="btn btn-primary mx-auto d-block"
                    onclick="document.getElementById('logout').submit();">{{ __('Logout') }}</a>
                @csrf
            </form>
            <br>
            {{-- Notifications --}}
            {{-- <button onclick="startFCM()" class="btn btn-outline-primary mx-auto d-block">Allow Notifications
            </button> --}}
            {{-- Notifications/ --}}
            @if (App::getLocale() == 'en')
            @php
                //Device specific headers
                $iPod = stripos($_SERVER['HTTP_USER_AGENT'], 'iPod');
                $iPhone = stripos($_SERVER['HTTP_USER_AGENT'], 'iPhone');
                $iPad = stripos($_SERVER['HTTP_USER_AGENT'], 'iPad');
                $Android = stripos($_SERVER['HTTP_USER_AGENT'], 'Android');
                $webOS = stripos($_SERVER['HTTP_USER_AGENT'], 'webOS');

                if ($iPhone || $iPod || $iPad) {
                    // hide id allow-notification
                    echo '<br>';
                } else {
                    echo ' <button onclick="startFCM()" class="btn btn-outline-primary mx-auto d-block">Allow Notifications
            </button>';
                }
            @endphp
            @else
            @php
                //Device specific headers
                $iPod = stripos($_SERVER['HTTP_USER_AGENT'], 'iPod');
                $iPhone = stripos($_SERVER['HTTP_USER_AGENT'], 'iPhone');
                $iPad = stripos($_SERVER['HTTP_USER_AGENT'], 'iPad');
                $Android = stripos($_SERVER['HTTP_USER_AGENT'], 'Android');
                $webOS = stripos($_SERVER['HTTP_USER_AGENT'], 'webOS');

                if ($iPhone || $iPod || $iPad) {
                    // hide id allow-notification
                    echo '<br>';
                } else {
                    echo ' <button onclick="startFCM()" class="btn btn-outline-primary mx-auto d-block">Permitir notificaciones
            </button>';
                }
            @endphp
            @endif
        </div>
        {{-- Notifications/ --}}
        <!--space for bottom navbar-->
        {{-- <div class="container-sm">
                <div class="p-3 border bg-light text-center fs-4"><strong><span class="iconify" data-icon="codicon:blank" style="font-size: 32px;"></span></strong></div>
            </div> --}}
        <!--space for bottom navbar-->
    </div>
    <br>

    {{-- loading animation --}}

    {{-- <div class="container loading-animation hide-desktop">
        <div class="col">
            <div class="p-3 border-top border-bottom bg-light">
                <div class="dark-hide"><img src="{{ asset('img/userblack.png') }}" alt="Logo"
                        class="rounded mx-auto d-block" /></div>
                <div class="light-hide"><img src="{{ asset('img/userwhite.png') }}" alt="Logo"
                        class="rounded mx-auto d-block" /></div>
                <h1 class="h3 text-center titles skeleton skeleton-title"></h1>
            </div>
        </div>
        <br>
        <div class="hide-mobile">
            <div class="container">
                <div class="col test">
                    <div class="p-3 border-top border-bottom bg-light skeleton skeleton-footer"><strong></strong></div>
                    <div class="p-3 border-top border-bottom bg-light skeleton skeleton-text"><a
                            class="btn btn-link float-end" href="#" role="button"></a></div>
                </div>
            </div>
            <br>
            <div class="container">
                <div class="col">
                    <div class="p-3 border-top border-bottom bg-light skeleton skeleton-footer"><strong></strong></div>
                    <div class="p-3 border-top border-bottom bg-light skeleton skeleton-text"><a
                            class="btn btn-link float-end" href="#" role="button">></a></div>
                </div>
            </div>
            <br>
            <div class="container">
                <div class="col">
                    <div class="p-3 border-top border-bottom bg-light skeleton skeleton-footer"><strong></strong></div>
                    <div class="p-3 border-top border-bottom bg-light skeleton skeleton-text"><a
                            class="btn btn-link float-end" href="#" role="button">></a></div>
                </div>
            </div>
            <br>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-0">
                        <button onclick="startFCM()" class="btn btn-outline-primary" id="allow-notification">Allow Notifications
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="hide-desktop">
            <div class="list-group">
                <a href="#" class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1"><small class="text-muted"><img
                                    src="{{ asset('/img/products/transparent.png') }}"
                                    class="skeleton skeleton-img d-inline mx-auto d-block"></small></span>
                        </h5>
                        <small class="text-muted"><img src="{{ asset('/img/products/transparent.png') }}"
                                class="skeleton skeleton-imgc d-inline mx-auto d-block"></small>
                    </div>
                    <p class="mb-1 skeleton skeleton-footer">&nbsp;&nbsp;&nbsp;</p>
                    <small
                        class="text-muted skeleton skeleton-text">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small>
                </a>
                <a href="#" class="list-group-item list-group-item-action" aria-current="true">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1"><small class="text-muted"><img
                                    src="{{ asset('/img/products/transparent.png') }}"
                                    class="skeleton skeleton-img d-inline mx-auto d-block"></small></h5>
                        <small class="text-muted"><img src="{{ asset('/img/products/transparent.png') }}"
                                class="skeleton skeleton-imgc d-inline mx-auto d-block"></small>
                    </div>
                    <p class="mb-1 skeleton skeleton-footer">&nbsp;&nbsp;&nbsp;</p>
                    <small
                        class="text-muted skeleton skeleton-text">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small>
                </a>
                <a href="#" class="list-group-item list-group-item-action" aria-current="true">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1"><small class="text-muted"><img
                                    src="{{ asset('/img/products/transparent.png') }}"
                                    class="skeleton skeleton-img d-inline mx-auto d-block"></small>
                        </h5>
                        <small class="text-muted"><img src="{{ asset('/img/products/transparent.png') }}"
                                class="skeleton skeleton-imgc d-inline mx-auto d-block"></small>
                    </div>
                    <p class="mb-1 skeleton skeleton-footer">&nbsp;&nbsp;&nbsp;</p>
                    <small
                        class="text-muted skeleton skeleton-text">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small>
                </a>
            </div>
            <br>
            <form id="logout" action="{{ route('logout') }}" method="POST">
                <a role="button"
                    class="btn btn-primary mx-auto d-block">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                @csrf
            </form>
            <br>
            <button
                class="btn btn-outline-primary mx-auto d-block">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </button>
        </div>
    </div> --}}

    <!-- The core Firebase JS SDK is always required and must be listed first -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-messaging.js"></script>
    <script>
        var firebaseConfig = {
            apiKey: "AIzaSyCpcQr0TEakh-laC1zZX5tHE2AcWfidU2M",
            authDomain: "marktech-33398.firebaseapp.com",
            projectId: "marktech-33398",
            storageBucket: "marktech-33398.appspot.com",
            messagingSenderId: "506959388755",
            appId: "1:506959388755:web:24a9243bd13c60a4eda6a6",
            measurementId: "G-Q9VZ817S2D"
        };
        firebase.initializeApp(firebaseConfig);
        const messaging = firebase.messaging();

        function startFCM() {
            messaging
                .requestPermission()
                .then(function() {
                    return messaging.getToken()
                })
                .then(function(response) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: '{{ route('store.token') }}',
                        type: 'POST',
                        data: {
                            token: response
                        },
                        dataType: 'JSON',
                        success: function(response) {
                            // alert('Token stored.');
                        },
                        error: function(error) {
                            alert('Error storing token.');
                        },
                    });
                }).catch(function(error) {
                    // alert(error);
                });
        }
        messaging.onMessage(function(payload) {
            const title = payload.notification.title;
            const options = {
                body: payload.notification.body,
                icon: payload.notification.icon,
            };
            new Notification(title, options);
        });
    </script>
@endsection
