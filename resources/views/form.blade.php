@extends('layouts.app')

@section('content')

@section('title', 'Form')

<h1>{{ __('Suggestions') }}</h1>
<form action={{ route('contact') }} method="POST">
    {{ csrf_field() }}

    <div class="form-group">
        <label for="name">*{{ __('Email') }}</label>
        <input type="email" name="name" type="text" class="form-control" required>
    </div>
<br>
    <div class="form-group">
        <label for="name">*{{ __('Message') }}</label>
        <input name="msg" type="text" class="form-control" required>
    </div>
<br>
    <div class="form-group">
        <button type="submit" id='btn-contact' class="btn bg-black text-white">{{ __('Send') }}</button>
    </div>
</form>

@endsection
