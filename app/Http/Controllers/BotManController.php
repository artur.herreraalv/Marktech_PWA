<?php

namespace App\Http\Controllers;

use BotMan\BotMan\BotMan;
use BotMan\BotMan\Messages\Incoming\Answer;

class BotManController extends Controller
{
    /**
     * Place your BotMan logic here.
     */
    public function handle()
    {
        $botman = app('botman');

        $botman->hears('{message}', function ($botman, $message) {
            switch ($message) {
                case $message == 'hola':
                case $message == 'Hola':
                case $message == 'ola':
                    $this->askSaludo($botman);
                    break;
                case $message == '¿como puedo reembolsar?':
                case $message == '¿como puedo cancelar?':
                case $message == '¿como puedo cancelar un pedido?':
                case $message == 'quiero cancelar':
                case $message == 'como puedo reembolsar':
                    $this->askrembolso($botman);
                    break;
                case $message == 'que metodos de pagos aceptan?':
                case $message == '¿Que metodos de pagos aceptan?':
                case $message == 'aceptan tarjetas de credito?':
                case $message == '¿Aceptan tarjetas de credito?':
                case $message == 'aceptan paypal?':
                case $message == '¿Aceptan paypal?':
                    $this->askpagos($botman);
                    break;
                case $message == 'puedo rastrear mi pedido?':
                case $message == '¿Puedo rastrear mi pedido?':
                case $message == 'puedo ver como va mi pedido?':
                case $message == '¿Puedo ver como va mi pedido?':
                case $message == 'quiero rastrear mi pedido':
                case $message == ' Quiero ver como va mi pedido':
                    $this->askpedidos($botman);
                    break;
                case $message == 'Gracias':
                case $message == 'gracias':
                    $this->askGracias($botman);
                    break;
                // english
                case $message == 'hello':
                case $message == 'Hello':
                case $message == 'hi':
                case $message == 'Hi':
                    $this->askSaludoEN($botman);
                    break;
                case $message == 'how can i refund?':
                case $message == 'how can i cancel?':
                case $message == 'how can i cancel a order?':
                case $message == 'i want to cancel':
                case $message == 'how can i refund':
                case $message == 'cancel':
                    $this->askrembolsoEN($botman);
                    break;
                case $message == 'what payment methods do you accept?':
                case $message == 'do you accept credit cards?':
                case $message == 'do you accept paypal?':
                    $this->askpagosEN($botman);
                    break;
                case $message == 'can i track my order?':
                case $message == 'can i see how my order is going?':
                case $message == 'i want to track my order':
                case $message == 'i want to see how my order is going':
                    $this->askpedidosEN($botman);
                    break;
                case $message == 'Thanks':
                case $message == 'thanks':
                    $this->askGraciasEN($botman);
                    break;
                default:
                // if the language is english
                if (app()->getLocale() == 'en') {
                    $botman->reply('I am sorry I did not understand your question, you can try with another');
                } else {
                    $botman->reply('Lo siento no entendi tu pregunta, puedes intentar con otra');
                }
                    break;
            }
        });

        $botman->listen();
    }

    /**
     * Place your BotMan logic here.
     */
    public function askSaludo($botman)
    {
        // if the bot no have stored the user name, ask for it
        if (!$botman->userStorage()->get('name')) {
            $botman->ask('¿Cual es tu nombre?', function (Answer $answer) use ($botman) {
                $name = $answer->getText();
                $botman->userStorage()->save([
                    'name' => $name,
                ]);
                $this->say('Hola ' . $name);
                $this->say('¿Cual es su pregunta?');
            }

            );
        } else {
            $botman->say('Hola ' . $botman->userStorage()->get('name') . ' ¿Cual es su pregunta?', function (Answer $answer) {
            });
        }
    }
    // Preguntas:

    public function askrembolso($botman)
    {
        $botman->ask('Para reembolsos se tendra que comunicar por medio de nuestras 3 redes sociales ¿Quiere que le brindemos enlace directo?Si/No', function (Answer $answer) {
            $name = $answer->getText();

            if ($name == 'Si') {
                $this->say('Facebook: <a href="https://www.facebook.com/MarktechMX" target="_blank"style="color:white">https://www.facebook.com/MarktechMX</a>');
                $this->say('Instagram: <a href="https://www.instagram.com/marktech2022/" target="_blank" style="color:white">https://www.instagram.com/marktech2022/</a>');
                $this->say('Twitter: <a href="https://twitter.com/MarktechOficial" target="_blank" style="color:white">https://twitter.com/MarktechOficial</a>');
            } else {
                $this->say('Gracias por utilizar el bot');
                $this->say('¿Dudas o Sugerencias?, Envialas en el siguiente enlace: <a href="https://marktech.ml/Sugerencias" target="_blank" style="color:white">https://marktech.ml/Sugerencias</a>');
                $this->say('¿Quieres Contactanos?, Visita el siguiente enlace: mailto:marktechof@gmail.com');
            }
        });
    }

    public function askpagos($botman)
    {
        $botman->say('En marktech contamos con 2 metodos de pagos oficiales que vendrian a ser Stripe(Tarjetas de credito/debito) y Paypal', function (Answer $answer) {
        });
    }

    public function askpedidos($botman)
    {
        $botman->ask('Al momento de realizar la compra en la seccion de <a href="pedidos" style="color:white" target="_blank" text-decoration="none">Mis Compras</a> podras encontrar el numero de rastreo que tendras que colocarlo en la pagina de la compañia y podras ver el Estado del paquete', function (Answer $answer) {
        });
    }

    public function askGracias($botman)
    {
        $botman->say('Gracias por utlizar el bot ¿Dudas o Sugerencias?, Envialas en el siguiente enlace: <a href="https://marktech.ml/Sugerencias" target="_blank" style="color:white">Sugerencias</a>
        ¿Quieres Contactarnos?, Mandanos un correo ah: marktechof@gmail.com', function (Answer $answer) {
        });
    }

    // English

    public function askSaludoEN($botman)
    {
        // if the bot no have stored the user name, ask for it
        if (!$botman->userStorage()->get('name')) {
            $botman->ask('What is your name?', function (Answer $answer) use ($botman) {
                $name = $answer->getText();
                $botman->userStorage()->save([
                    'name' => $name,
                ]);
                $this->say('Hello ' . $name);
                $this->say('What is your question?');
            }

            );
        } else {
            $botman->say('Hello
            ' . $botman->userStorage()->get('name') . ' What is your question?', function (Answer $answer) {
            });

        }
    }

    public function askrembolsoEN($botman)
    {
        $botman->ask('For refunds you will have to communicate through our 3 social networks Do you want us to provide you with a direct link? Yes/No', function (Answer $answer) {
            $name = $answer->getText();

            if ($name == 'Yes') {
                $this->say('Facebook: <a href="https://www.facebook.com/MarktechMX" target="_blank"style="color:white">https://www.facebook.com/MarktechMX</a>');
                $this->say('Instagram: <a href="https://www.instagram.com/marktech2022/" target="_blank" style="color:white">https://www.instagram.com/marktech2022/</a>');
                $this->say('Twitter: <a href="https://twitter.com/MarktechOficial" target="_blank" style="color:white">https://twitter.com/MarktechOficial</a>');
            } else {
                $this->say('Thank you for using the bot');
                $this->say('Questions or Suggestions?, Send them at the following link: <a href="https://marktech.ml/Sugerencias" target="_blank" style="color:white">https://marktech.ml/Sugerencias</a>');
                $this->say('Do you want to Contact us?, Visit the following link: mailto:marktechof@gmail.com');
            }
        });
    }

    public function askpagosEN($botman)
    {
        $botman->say('At marktech we have 2 official payment methods that would be Stripe (Credit / Debit Cards) and Paypal', function (Answer $answer) {
        });
    }

    public function askpedidosEN($botman)
    {
        $botman->ask('When you make a purchase in the <a href="pedidos" style="color:white" target="_blank" text-decoration="none">My Purchases</a> section you will find the tracking number that you will have to put it on the company page and you will be able to see the Status of the package', function (Answer $answer) {
        });
    }

    public function askGraciasEN($botman)
    {
        $botman->say('Thank you for using the bot Questions or Suggestions?, Send them at the following link: <a href="https://marktech.ml/Sugerencias" target="_blank" style="color:white">Suggestions</a>
        Do you want to Contact us?, Send us an email to: marktechof@gmail.com', function (Answer $answer) {
        });
    }

}
