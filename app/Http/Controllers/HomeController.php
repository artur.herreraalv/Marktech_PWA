<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $viewData = [];
        $viewData['title'] = 'Marktech';

        return view('home.index')->with('viewData', $viewData);
    }

    public function test()
    {
        return view('push');
    }

    public function about()
    {
        $viewData = [];
        $viewData['title'] = 'Marktech';
        $viewData['subtitle'] = '';
        $viewData['description'] = '';
        $viewData['author'] = '';

        return view('home.about')->with('viewData', $viewData);
    }

// push

    public function saveToken(Request $request)
    {
        auth()->user()->update(['device_key' => $request->token]);

        return response()->json(['Token successfully stored.']);
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function sendNotification(Request $request)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $FcmToken = User::whereNotNull('device_key')->pluck('device_key')->all();

        $serverKey = 'AAAAdgkiYFM:APA91bH6qH5Tz7f4lUI-_oawOsvOEV960M2EXWSfQF_6cnlQBsWoEBPmVFtjBJgK9ty5nwfpARUbjI2NUIrfUEA8TE_7WpstRpU7y-WDWkRH_l8GzJfDrsRM4V5hhIJuFgCej7es9IPi';

        $data = [
            'registration_ids' => $FcmToken,
            'notification' => [
                'title' => $request->title,
                'body' => $request->body,
            ],
        ];
        $encodedData = json_encode($data);

        $headers = [
            'Authorization:key='.$serverKey,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);
        // Execute post
        $result = curl_exec($ch);
        if ($result === false) {
            exit('Curl failed: '.curl_error($ch));
        }
        // Close connection
        curl_close($ch);
        // FCM response
        dd($result);
    }
}
