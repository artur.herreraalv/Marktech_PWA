<?php

namespace App\Http\Controllers;

use App\Models\User;
use Auth;
use Illuminate\Http\Request;

class WebNotificationController extends Controller
{
    public function updateDeviceToken(Request $request)
    {
        Auth::user()->device_token = $request->token;

        Auth::user()->save();

        return response()->json(['Token successfully stored.']);
    }

    public function sendNotification(Request $request)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';

        $FcmToken = User::whereNotNull('device_token')->pluck('device_token')->all();

        $serverKey = 'AAAAdgkiYFM:APA91bH6qH5Tz7f4lUI-_oawOsvOEV960M2EXWSfQF_6cnlQBsWoEBPmVFtjBJgK9ty5nwfpARUbjI2NUIrfUEA8TE_7WpstRpU7y-WDWkRH_l8GzJfDrsRM4V5hhIJuFgCej7es9IPi'; // ADD SERVER KEY HERE PROVIDED BY FCM

        $data = [
            'registration_ids' => $FcmToken,
            'notification' => [
                'title' => $request->title,
                'body' => $request->body,
                // open the page when click on notification
                'click_action' => 'https://marktech.ml/',
                'icon' => 'https://marktech.ml/img/logo.jpg',
            ],
        ];
        $encodedData = json_encode($data);

        $headers = [
            'Authorization:key='.$serverKey,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);
        // Execute post
        $result = curl_exec($ch);
        if ($result === false) {
            exit('Curl failed: '.curl_error($ch));
        }
        // Close connection
        curl_close($ch);
        // FCM response
        dd($result);
    }
}
