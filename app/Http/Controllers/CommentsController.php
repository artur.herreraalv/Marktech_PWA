<?php

namespace App\Http\Controllers;

use App\Models\Comments;
use App\Models\Product;
use Request;

class CommentsController extends Controller
{
    // make sure the user is logged in
    public function __construct()
    {
        $this->middleware('auth');
    }

    // store a new comment
    public function store(Request $request)
    {
        $this->validate($request, [
            'body' => 'required|min:2',
        ]);

        $comment = new Comment;
        $comment->body = $request->body;
        $comment->user_id = Auth::id();
        $comment->post_id = $request->post_id;
        $comment->save();

        return back();
    }

    // delete a comment
    public function destroy($id)
    {
        $comment = Comment::find($id);
        $comment->delete();

        return back();
    }

    // show comments of product
    public function show($id)
    {
        $comments = Comment::where('post_id', $id)->get();

        return view('comments.show', compact('comments'));
    }
}
