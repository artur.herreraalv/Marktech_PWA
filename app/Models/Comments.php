<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    use HasFactory;

    public function getId()
    {
        return $this->attributes['id'];
    }

    public function setId($id)
    {
        $this->attributes['id'] = $id;
    }

    public function getProductId()
    {
        return $this->attributes['product_id'];
    }

    public function setProductId($product_id)
    {
        $this->attributes['product_id'] = $product_id;
    }

    public function getProduct()
    {
        return $this->attributes['product'];
    }

    public function setProduct($product)
    {
        $this->attributes['product'] = $product;
    }

    public function getUserId()
    {
        return $this->attributes['user_id'];
    }

    public function setUserId($user_id)
    {
        $this->attributes['user_id'] = $user_id;
    }

    public function getUserName()
    {
        return $this->attributes['user_name'];
    }

    public function setUserName($user_name)
    {
        $this->attributes['user_name'] = $user_name;
    }

    public function getComment()
    {
        return $this->attributes['comment'];
    }

    public function setComment($comment)
    {
        $this->attributes['comment'] = $comment;
    }

    public function getStars()
    {
        return $this->attributes['stars'];
    }

    public function setStars($stars)
    {
        $this->attributes['stars'] = $stars;
    }

    // public function getItemId()
    // {
    //     return $this->attributes['item_id'];
    // }

    // public function setItemId($item_id)
    // {
    //     $this->attributes['item_id'] = $item_id;
    // }
}
