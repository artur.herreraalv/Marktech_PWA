<!doctype html>
<html lang="en">

<head>
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#FFFFFF">
    <!-- Safari -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <!--  -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- PWA  -->
    <link rel="apple-touch-icon" href="/logo.jpg">
    <link rel="manifest" href="/manifest.json">
    <!-- Botstrap CSS -->
    <script src="../js/iconify.min.js"></script>
    <script src="../js/app.js" defer></script>
           <!-- check if the dark mode is enabled -->
           <script>
        if (localStorage.getItem('darkSwitch') !== null) {
            if (localStorage.getItem('darkSwitch') === 'dark') {
                document.documentElement.setAttribute('data-theme', 'dark')
            } else {
                document.documentElement.setAttribute('data-theme', 'light')
            }
        }
        // check if the user has a preference
        var darkQuery = window.matchMedia('(prefers-color-scheme: dark)');
        darkQuery.addListener(function(e) {
            var newTheme = e.matches ? 'dark' : 'light';
            document.documentElement.setAttribute('data-theme', newTheme)
        })
    </script>

<!-- check if the dark mode is enabled on mobile -->
<script>
    if (localStorage.getItem('darkSwitchm') !== null) {
        if (localStorage.getItem('darkSwitchm') === 'dark') {
            document.documentElement.setAttribute('data-theme', 'dark')
        } else {
            document.documentElement.setAttribute('data-theme', 'light')
        }
    }
    // check if the user has a preference
    var darkQuery = window.matchMedia('(prefers-color-scheme: dark)');
    darkQuery.addListener(function(e) {
        var newTheme = e.matches ? 'dark' : 'light';
        document.documentElement.setAttribute('data-theme', newTheme)
    })
</script>


    <link href="../css/app.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/dark-mode.css" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <title>Marktech</title>
    <!-- icons ios -->
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3) and (orientation: landscape)"
        href="img/img/splash_screens/iPhone_13_mini__iPhone_12_mini__iPhone_11_Pro__iPhone_XS__iPhone_X_landscape.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 834px) and (device-height: 1194px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)"
        href="img/splash_screens/11__iPad_Pro__10.5__iPad_Pro_landscape.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 1024px) and (device-height: 1366px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)"
        href="img/splash_screens/12.9__iPad_Pro_landscape.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)"
        href="img/splash_screens/iPhone_8__iPhone_7__iPhone_6s__iPhone_6__4.7__iPhone_SE_portrait.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)"
        href="img/splash_screens/iPhone_11__iPhone_XR_portrait.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)"
        href="img/splash_screens/iPhone_11__iPhone_XR_landscape.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 810px) and (device-height: 1080px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)"
        href="img/splash_screens/10.2__iPad_portrait.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3) and (orientation: landscape)"
        href="img/splash_screens/iPhone_11_Pro_Max__iPhone_XS_Max_landscape.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 393px) and (device-height: 852px) and (-webkit-device-pixel-ratio: 3) and (orientation: landscape)"
        href="img/splash_screens/iPhone_14_Pro_landscape.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 430px) and (device-height: 932px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)"
        href="img/splash_screens/iPhone_14_Pro_Max_portrait.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 834px) and (device-height: 1112px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)"
        href="img/splash_screens/10.5__iPad_Air_portrait.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 428px) and (device-height: 926px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)"
        href="img/splash_screens/iPhone_14_Plus__iPhone_13_Pro_Max__iPhone_12_Pro_Max_portrait.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 820px) and (device-height: 1180px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)"
        href="img/splash_screens/10.9__iPad_Air_landscape.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)"
        href="img/splash_screens/iPhone_13_mini__iPhone_12_mini__iPhone_11_Pro__iPhone_XS__iPhone_X_portrait.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 820px) and (device-height: 1180px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)"
        href="img/splash_screens/10.9__iPad_Air_portrait.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 834px) and (device-height: 1194px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)"
        href="img/splash_screens/11__iPad_Pro__10.5__iPad_Pro_portrait.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)"
        href="img/splash_screens/iPhone_8__iPhone_7__iPhone_6s__iPhone_6__4.7__iPhone_SE_landscape.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 430px) and (device-height: 932px) and (-webkit-device-pixel-ratio: 3) and (orientation: landscape)"
        href="img/splash_screens/iPhone_14_Pro_Max_landscape.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 393px) and (device-height: 852px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)"
        href="img/splash_screens/iPhone_14_Pro_portrait.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)"
        href="img/splash_screens/iPhone_11_Pro_Max__iPhone_XS_Max_portrait.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 428px) and (device-height: 926px) and (-webkit-device-pixel-ratio: 3) and (orientation: landscape)"
        href="img/splash_screens/iPhone_14_Plus__iPhone_13_Pro_Max__iPhone_12_Pro_Max_landscape.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)"
        href="img/splash_screens/9.7__iPad_Pro__7.9__iPad_mini__9.7__iPad_Air__9.7__iPad_portrait.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)"
        href="img/splash_screens/4__iPhone_SE__iPod_touch_5th_generation_and_later_landscape.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 390px) and (device-height: 844px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)"
        href="img/splash_screens/iPhone_14__iPhone_13_Pro__iPhone_13__iPhone_12_Pro__iPhone_12_portrait.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 810px) and (device-height: 1080px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)"
        href="img/splash_screens/10.2__iPad_landscape.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)"
        href="img/splash_screens/iPhone_8_Plus__iPhone_7_Plus__iPhone_6s_Plus__iPhone_6_Plus_portrait.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)"
        href="img/splash_screens/9.7__iPad_Pro__7.9__iPad_mini__9.7__iPad_Air__9.7__iPad_landscape.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)"
        href="img/splash_screens/4__iPhone_SE__iPod_touch_5th_generation_and_later_portrait.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 834px) and (device-height: 1112px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)"
        href="img/splash_screens/10.5__iPad_Air_landscape.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 1024px) and (device-height: 1366px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)"
        href="img/splash_screens/12.9__iPad_Pro_portrait.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3) and (orientation: landscape)"
        href="img/splash_screens/iPhone_8_Plus__iPhone_7_Plus__iPhone_6s_Plus__iPhone_6_Plus_landscape.png">
    <link rel="apple-touch-startup-image"
        media="screen and (device-width: 390px) and (device-height: 844px) and (-webkit-device-pixel-ratio: 3) and (orientation: landscape)"
        href="img/splash_screens/iPhone_14__iPhone_13_Pro__iPhone_13__iPhone_12_Pro__iPhone_12_landscape.png">
    <!-- icons ios -->
</head>

<body>
      <!-- header -->
      <div class="navbar1">
            <nav class="navbar navbar-expand-lg navbar-light bg-white"><a class="mx-auto" href="/offline.php"> <div class="dark-hide"><img src="../img/mk2otln.png" class="logo-mk"></div>
                        <div class="light-hide"><img src="../img/mk2otlnwhite.png" class="logo-mk"></div>
                    <div class="container">
                        <a class="navbar-brand" href="home.index"></a>
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="mx-auto">
                            <div class="input-group">
                                <form action="/busqueda" method="POST" role="search">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="barra"
                                            placeholder="Search products..." style="width: 300px; height: 50px">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-default">
                                            </button>
                                        </span>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                            <div class="navbar-nav ms-auto">
                                    <a class="nav-link" href="cart.index"><span class="iconify"
                                            data-icon="eva:shopping-cart-outline" data-width="24"></span> Carrito</a>
                                    <a class="nav-link" href="/IniciarSesion">Log In</a>
                                    <a class="nav-link" href="/Registro">Sign Up</a>
                            </div>
                        </div>


                    </div>

            </nav>

             <!--Navbar-->
             </nav>
                <div class="hide-mobile">
                    <br>
                </div>
                <div class="navbar-black">
                    <nav class="navbar navbar-expand-lg navbar-dark bg-black rounded">

                        <!--Dropdown-->
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0 ">
                            <div class="collapse navbar-collapse " id="navbarSupportedContent">
                                <li class="nav-item dropdown s">
                                    <a class="btn btn-link btn-lg" href="/hardware" id="navbarDropdown"
                                        role="button" aria-haspopup="true" aria-expanded="false"
                                        id="hardware">Hardware <span class="iconify"
                                            data-icon="bx:down-arrow"></span></a>
                                    {{-- <a class="btn btn-link btn-lg dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    </a> --}}
                                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <ul>
                                            <li><a class="dropdown-item" href="/armatucomputadora"
                                                    id="buildyourpc">Build your PC</a></li>
                                            <li><a class="dropdown-item" href="/hardware/procesadores"
                                                    id="cpus">CPUs</a></li>
                                            </li>
                                            <li><a class="dropdown-item" href="/hardware\motherboards"
                                                    id="motherboards">Motherboards</a></li>
                                            </li>
                                            <li><a class="dropdown-item" href="/hardware\gabinetes"
                                                    id="case">Case</a></li>
                                            <li><a class="dropdown-item" href="/hardware\graficas"
                                                    id="gpu">GPUs</a></li>
                                            </li>
                                            <li><a class="dropdown-item" href="/hardware\ram" id="ram">RAM</a></li>
                                            <li><a class="dropdown-item" href="/hardware\disipadores"
                                                    id="coolers">Coolers</a></li>
                                            </li>
                                            <li><a class="dropdown-item" href="/hardware\fuentes"
                                                    id="psu">PSUs</a></li>
                                            </li>
                                        </ul>
                                        <ul>
                                            <li><a class="dropdown-item" href="/almacenamiento"
                                                    id="storage">Storage</a></li>
                                            </li>
                                            <li><a class="dropdown-item" href="/hardware\ssd" id="ssd">SSD</a></li>
                                            </li>
                                            <li><a class="dropdown-item" href="/hardware\hdd" id="hdd">HDD</a></li>
                                            <li><a class="dropdown-item" href="/hardware\ram" id="ram2">RAM</a></li>
                                            <li><a class="dropdown-item" href="/hardware\usb"
                                                    id="usbsd">USB/SD</a></li>
                                        </ul>
                                    </ul>
                                </li>
                        </ul>
                        <!--Dropdown-->
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item dropdown">
                                <a class="btn btn-link btn-lg" href="/accesorios" id="navbarDropdown" role="button"
                                    aria-haspopup="true" aria-expanded="false" id="accessories">Accessories <span
                                        class="iconify" data-icon="bx:down-arrow"></span></a>
                                {{-- <a class="btn btn-link btn-lg dropdown-toggle" href="/todo" id="navbarDropdown" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">

                    </a> --}}
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <ul>
                                        <li><a class="dropdown-item" href="/accesrios"
                                                id="accessories2">Accessories</a></li>
                                        <li><a class="dropdown-item" href="/accesorios\audifonos"
                                                id="headphones">Headphones</a></li>
                                        <li><a class="dropdown-item" href="/accesorios\alfombrillas"
                                                id="mousepads">Mousepads</a></li>
                                        </li>
                                        <li><a class="dropdown-item" href="/accesorios\mouse"
                                                id="mouse">Mouse</a></li>
                                        <li><a class="dropdown-item" href="/accesorios\teclados"
                                                id="keyboards">Keyboards</a></li>
                                    </ul>
                                </ul>
                        </ul>
                        </li>
                        </ul>
                        <!--Dropdown-->
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item dropdown">
                                <a class="btn btn-link btn-lg" href="/computadoras" id="navbarDropdown"
                                    role="button" aria-haspopup="true" aria-expanded="false"
                                    id="pcs">PCs <span class="iconify" data-icon="bx:down-arrow"></span></a>
                                {{-- <a class="btn btn-link btn-lg dropdown-toggle" href="/todo" id="navbarDropdown" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">

                    </a> --}}
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <ul>
                                        <li><a class="dropdown-item" href="/computadras"
                                                id="pcs2">PCs</a></li>
                                        <li><a class="dropdown-item" href="/computadoras\laptop"
                                                id="laptop">Laptops</a></li>
                                        <li><a class="dropdown-item" href="/computadoras\escritorio"
                                                id="desktop">Desktops</a></li>
                                        </li>
                                    </ul>
                                </ul>
                        </ul>
                        </li>
                        </ul>
                        </ul>
                        </ul>
                        </li>
                        </ul>

                        <!--Dropdown-->
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item dropdown">
                                <a class="btn btn-link btn-lg" href="/electronica" id="navbarDropdown"
                                    role="button" aria-haspopup="true" aria-expanded="false"
                                    id="electronics">Electronics <span class="iconify"
                                        data-icon="bx:down-arrow"></span></a>
                                {{-- <a class="btn btn-link btn-lg dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">

                    </a> --}}
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <ul>
                                        <li><a class="dropdown-item" href="/electrnica"
                                                id="electronics2">Electronics</a></li>
                                        <li><a class="dropdown-item" href="/electronica\consolas"
                                                id="consoles">Consoles</a></li>
                                        <li><a class="dropdown-item" href="/electronica\tv"
                                                id="tv">TV</a></li>
                                        <li><a class="dropdown-item" href="/electronica\monitores"
                                                id="monitors">Monitors</a></li>
                                        <li><a class="dropdown-item" href="/electronica\bocinas"
                                                id="speakers">Speakers</a></li>
                                        <li><a class="dropdown-item" href="/electronica\camaras"
                                                id="cameras">Cameras</a></li>
                                        <li><a class="dropdown-item" href="/electronica\telefonos"
                                                id="smartphones">Smartphones</a></li>
                                    </ul>
                                </ul>
                        </ul>
                        </li>
                        </ul>
                </div>
                </nav>

        <!-- Bottom fixed navbar bootstrap -->
        <div class="hide-desktop">
            <div class="mobile-nav">
                <nav class="navbar navbar-light bg-light fixed-bottom">
                    <div class="container-fluid">
                        <div class="navbar-nav flex-row mx-auto">
                                <a class="nav-link offlinenav" href="../offline.php"><img src="../img/UI/home-button.png" width="36"
                                    height="36">Home</a>
                                <a class="nav-link offlinenav" href="../offline/categoriesm.php"><img src="../img/UI/menu.png" width="36"
                                    height="36">Categories</a>

                            <!-- account or login -->
                            <a class="nav-link offlinenav" href="/micuenta"><img src="../img/UI/user.png" width="36"
                                    height="36">Account</a>
                        <!--account or login/-->

                        <a class="nav-link offlinenav" href="/tuCarrito"><img src="../img/UI/shopping-cart.png" width="36"
                                    height="36">Cart</a>
                                <a class="nav-link offlinenav" href="../offline/settingsm.php"><img src="../img/UI/settings.png" width="36"
                                    height="36">Settings</a>

                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <!-- Bottom fixed navbar bootstrap -->
<br>
    <figure class="text-center">
        <blockquote class="blockquote">
            <p class="h1"><strong>Settings</strong></p>
        </blockquote>
    </figure>
    <div class="list-group">
        <a href="./settings/theme.php" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1"><span class="iconify" data-icon="arcticons:galaxy-themes" style="font-size: 32px;"></span>
                </h5>
                <small class="text-muted"><span class="iconify" data-icon="bi:arrow-right-circle"
                        style="font-size: 32px;"></span></small>
            </div>
            <p class="mb-1">Theme</p>
            <small class="text-muted">Change the appearance of the application.</small>
        </a>
        <a href="./settings/languages" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1"><span class="iconify" data-icon="arcticons:galaxy-themes" style="font-size: 32px;"></span>
                </h5>
                <small class="text-muted"><span class="iconify" data-icon="bi:arrow-right-circle"
                        style="font-size: 32px;"></span></small>
            </div>
            <p class="mb-1">Language</p>
        </a>
        <a href="./settings/about.php" class="list-group-item list-group-item-action" aria-current="true">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1"><span class="iconify" data-icon="fluent:info-20-regular" style="font-size: 32px;"></span>
                </h5>
                <small class="text-muted"><span class="iconify" data-icon="bi:arrow-right-circle"
                        style="font-size: 32px;"></span></small>
            </div>
            <p class="mb-1">About Marktech</p>
            <small class="text-muted"></small>
        </a>
        <a href="./settings/help.php" class="list-group-item list-group-item-action" aria-current="true">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1"><span class="iconify" data-icon="ei:question" style="font-size: 32px;"></span></h5>
                <small class="text-muted"><span class="iconify" data-icon="bi:arrow-right-circle"
                        style="font-size: 32px;"></span></small>
            </div>
            <p class="mb-1">Help</p>
            <small class="text-muted"></small>
        </a>
    </div>
