const preLoad = function () {
    return caches.open("offline").then(function (cache) {
        // caching index and important routes
        return cache.addAll(filesToCache);
    });
};

self.addEventListener("install", function (event) {
    event.waitUntil(preLoad());
});

const filesToCache = [
    '/',
    '/offline.php',
    '/offline/categoriesm.php',
    '/offline/settingsm.php',
    '/offline/settings/about.php',
    '/offline/settings/help.php',
    '/offline/settings/language.php',
    '/offline/settings/theme.php',
    '/offline/settings/footer/av.php',
    '/offline/settings/footer/tyc.php',
    '/js/iconify.min.js',
    '/js/app.js',
    '/css/app.css',
    '/css/style.css',
    '/css/dark-mode.css',
    '/img/mk2otln.png',
    '/img/mk2otlnwhite.png',
    '/img/oflnconblue.png',
    '/img/UI/home-button.png',
    '/img/UI/left.png',
    '/img/UI/user.png',
    '/img/UI/shopping-cart.png',
    '/img/UI/up-arrow-circular-button.png',
    '/img/UI/settings.png',
    '/img/UI/menu.png',
    '/js/offline.min.js',
    '/css/offline-language-spanish.css',
    '/css/offline-theme-slide.css',
];

const checkResponse = function (request) {
    return new Promise(function (fulfill, reject) {
        fetch(request).then(function (response) {
            if (response.status !== 404) {
                fulfill(response);
            } else {
                reject();
            }
        }, reject);
    });
};

const addToCache = function (request) {
    return caches.open("offline").then(function (cache) {
        return fetch(request).then(function (response) {
            return cache.put(request, response);
        });
    });
};

const returnFromCache = function (request) {
    return caches.open("offline").then(function (cache) {
        return cache.match(request).then(function (matching) {
            if (!matching || matching.status === 404) {
                return cache.match('offline.php');
            } else {
                return matching;
            }
        });
    });
};

self.addEventListener("fetch", function (event) {
    event.respondWith(checkResponse(event.request).catch(function () {
        return returnFromCache(event.request);
    }));
    if (!event.request.url.startsWith('http')) {
        event.waitUntil(addToCache(event.request));
    }
});

// self.addEventListener('notificationclick', function (event) {
//     //For root applications: just change "'./'" to "'/'"
//     //Very important having the last forward slash on "new URL('./', location)..."
//     const rootUrl = new URL('/index.php', location).href;
//     event.notification.close();
//     event.waitUntil(
//         clients.matchAll().then(matchedClients => {
//             for (let client of matchedClients) {
//                 if (client.url.indexOf(rootUrl) >= 0) {
//                     return client.focus();
//                 }
//             }

//             return clients.openWindow(rootUrl).then(function (client) { client.focus(); });
//         })
//     );
// });

self.addEventListener('notificationclick', function (event) {
    console.log('On notification click: ', event.notification.tag);
    // Android doesn't close the notification when you click on it
    // See: http://crbug.com/463146
    event.notification.close();

    // This looks to see if the current is already open and
    // focuses if it is
    event.waitUntil(
        clients.matchAll({
            type: "window"
        })
            .then(function (clientList) {
                for (var i = 0; i < clientList.length; i++) {
                    var client = clientList[i];
                    if (client.url == '/' && 'focus' in client)
                        return client.focus();
                }
                if (clients.openWindow) {
                    return clients.openWindow('/');
                }
            })
    );
});
